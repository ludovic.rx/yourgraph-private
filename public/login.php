<?php

/** Login
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 Your Graph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Page to login to the account
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Email of the user */
$email = "";
/** Password of the user */
$password = "";
/** Contains true if login is valid */
$validLogin = null;

if (filter_input(INPUT_POST, "submit", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterEmail($email) &&  LToolsFilter::filterPassword($password)) {
        $userDB = new LUserDB();
        if ($userDB->verifyPasswordByEmail($email, $password)) {
            LTools::connectUser($userDB->getUserByEmail($email));
        } else {
            $validLogin = false;
        }
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php LToolsHTML::writeHead("Login !", "You can login to your account.") ?>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    <form class="user" method="POST" action="#">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user <?= LToolsHTMLForm::writeValidityInput($validLogin) ?>" id="email" placeholder="Enter Email Address..." name="email" value="<?= $email ?>" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user <?= LToolsHTMLForm::writeValidityInput($validLogin) ?>" id="password" placeholder="Password" name="password" require />
                                        </div>
                                        <input class="btn btn-primary btn-user btn-block" value="Login" name="submit" type="submit" />
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="register.php">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <?php

    LToolsHTML::writeScripts();

    ?>

</body>

</html>