<?php

/** Graphs
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 Your Graph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Page that shows graphs of the user
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Define if we success to delete */
$deleteSuccess = null;

// Redirect user if not connected
if (LSession::getInstance()->getUserSession() == null) {
    LTools::redirect("index.php");
}

LToolsFilter::filterDeleteModal($deleteSuccess);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php LToolsHTML::writeHead("Your Graphs !", "You can look at your graphs.") ?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php LToolsHTML::writeSideBar() ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php LToolsHTML::writeTopbar() ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-4 text-gray-800">Your Graphs</h1>
                    </div>

                    <!-- Graphs -->
                    <div class="row">
                        <?= LToolsHTML::writeChartsUser(); ?>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php LToolsHTML::writeFooter() ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

   
    <?php LToolsHTML::writeScrollTop(); ?>
    <?= LToolsHTMLModal::writeLogoutModal(); ?>
    <?= LToolsHTMLModal::writeCreateGraphModal(); ?>
    <?= LToolsHTMLModal::writeFailDeleteModal(); ?>
    <?= LToolsHTMLModal::writeSuccessDeleteModal(); ?>
    <?php LToolsHTML::writeScripts();  ?>
   

    <script>
        <?= LToolsHTMLModal::showSuccessDeleteModal($deleteSuccess) ?>
        <?= LToolsHTMLModal::showFailDeleteModal($deleteSuccess) ?>
    </script>
</body>

</html>