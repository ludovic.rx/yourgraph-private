<?php

/** Index
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 Your Graph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Page that allow to create graphs
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <?php LToolsHTML::writeHead("Error !", "You encoutered an error.") ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php LToolsHTML::writeSideBar() ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php LToolsHTML::writeTopbar() ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- 404 Error Text -->
                    <div class="text-center">
                        <div class="error mx-auto" data-text="404">404</div>
                        <p class="lead text-gray-800 mb-5">Page Not Found</p>
                        <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
                        <a href="index.php">&larr; Back to graph page</a>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php LToolsHTML::writeFooter() ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <?php
    LToolsHTML::writeScrollTop();
    LToolsHTML::writeScripts();
    echo LToolsHTMLModal::writeCreateGraphModal();
    ?>

</body>

</html>