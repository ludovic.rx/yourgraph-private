<?php

/** export
 *  -------
 *  @file 
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief export data of chart to a csv file
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Chart */
$chart = LSession::getInstance()->getChartSession();

if ($chart != null) {
    if (LTools::exportCSV($chart)) {
        exit;
    }
}

LTools::redirect(".." . DIRECTORY_SEPARATOR . "index.php");