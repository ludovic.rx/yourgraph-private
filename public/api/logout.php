<?php

/** Page that logs out the user
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 Your Graph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Page to log out the user
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR .".." . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

LTools::disconnectUser();