<?php

/** import
 *  -------
 *  @file 
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief import data in the chart
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Url where we redirect */
$redirectUrl = ".." . DIRECTORY_SEPARATOR . "index.php";
/** Chart */
$chart = clone(LSession::getInstance()->getChartSession());

// First verify that we have clicked the button to import
if (filter_input(INPUT_POST, "import", FILTER_SANITIZE_STRING)) {
    $redirectUrl .= "?importSuccess=";
    if ($chart != null) {
        if ($chart->getXAxis() != null && $chart->getYAxis() !== null) {
            $filename = LTools::upload();
            if ($filename !== "") {
                if (LTools::import($chart, $filename)) {
                    $redirectUrl .= "1";
                } else {
                    $redirectUrl .= "0";
                }
                // Delete file at the end of the import
                unlink($filename);
            }
        }
    }
}

LTools::redirect($redirectUrl);
