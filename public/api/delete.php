<?php

/** delete
 *  -------
 *  @file 
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief delete a chart in the database
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Url where we redirect */
$redirectUrl = ".." . DIRECTORY_SEPARATOR . "graphs.php";
/** User */
$user = LSession::getInstance()->getUserSession();

if ($user != null) {
    if (LToolsFilter::filterIdChart($idChart)) {
        $redirectUrl .= "?deleteSuccess=";
        if (LTools::deleteChart($idChart, $user->getId())) {
            $redirectUrl .= "1";
        } else {
            $redirectUrl .= "0";
        }
    }
}

LTools::redirect($redirectUrl);
