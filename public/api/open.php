<?php

/** Page to open chart
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 Your Graph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Page to open chart
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Url where we redirect the user */
$redirectUrl = ".." . DIRECTORY_SEPARATOR . "index.php";

/** user in the session */
$user = LSession::getInstance()->getUserSession();

// if user exists and we have the id of the chart to open
if ($user !== null && LToolsFilter::filterIdChart($idChart)) {
    if ($chart = (new LChartDB)->getChartById($idChart, $user->getId())) {
        LSession::getInstance()->setChartSession($chart);
    } else {
        $redirectUrl = ".." . DIRECTORY_SEPARATOR . "graphs.php";
    }
}

LTools::redirect($redirectUrl);
