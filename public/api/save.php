<?php

/** save
 *  -------
 *  @file 
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief save a chart in the database
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Url where we redirect */
$redirectUrl = ".." . DIRECTORY_SEPARATOR . "index.php";

if (LSession::getInstance()->getUserSession() != null) {
    $redirectUrl .= "?saveSuccess=";
    if (LTools::saveChart()) {
        $redirectUrl .= "1";
    } else {
        $redirectUrl .= "0";
    }
}

LTools::redirect($redirectUrl);