<?php

/** Register
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 Your Graph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Page to register
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** First Name of the user */
$firstName = "";
/** Contains tru if first name is valid */
$validFirstName = null;
/** Last name of the user */
$lastName = "";
/** Contains tru if last name is valid */
$validLastName = null;
/** Email of the user */
$email = "";
/** Contains tru if email is valid */
$validEmail = null;
/** Password of the user */
$password = "";
/** Password repeat of the user */
$repeatPassword = "";
/** Contains true if passwords the same, else false */
$samePassword = null;

if (filter_input(INPUT_POST, "submit", FILTER_SANITIZE_STRING)) {
    $validEmail = LToolsFilter::filterEmail($email);
    $validFirstName = LToolsFilter::filterFirstName($firstName);
    $validLastName = LToolsFilter::filterLastName($lastName);
    // Just verify email first so we can check if email exists
    if ($validEmail) {
        // True if email doesn't exists so we invert
        $userDB = new LUserDB();
        $validEmail = !$userDB->emailExists($email);
        if (
            $validFirstName && $validFirstName && $validEmail &&
            LToolsFilter::filterPassword($password) &&
            LToolsFilter::filterRepeatPassword($repeatPassword)
        ) {
            $samePassword = $repeatPassword == $password;
            if ($samePassword) {
                if ($userDB->insertUser($firstName, $lastName, $email, $password)) {
                    LTools::redirect("login.php");
                }
            }
        }
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php LToolsHTML::writeHead("Register !", "You can create your account.") ?>
</head>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <form class="user" method="POST" action="#">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user <?= LToolsHTMLForm::writeValidityInput($validFirstName) ?>" id="firstName" name="firstName" placeholder="First Name" value="<?= $firstName ?>" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user <?= LToolsHTMLForm::writeValidityInput($validLastName) ?>" id="lastName" name="lastName" placeholder="Last Name" value="<?= $lastName ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user <?= LToolsHTMLForm::writeValidityInput($validEmail) ?>" id="email" name="email" placeholder="Email Address" value="<?= $email ?>" required>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user <?= LToolsHTMLForm::writeValidityInput($samePassword) ?>" id="password" name="password" placeholder="Password" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user <?= LToolsHTMLForm::writeValidityInput($samePassword) ?>" id="repeatPassword" name="repeatPassword" placeholder="Repeat Password" required>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary btn-user btn-block" name="submit" value="Register Account">
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="login.php">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php
    LToolsHTML::writeScripts();
    ?>

</body>

</html>