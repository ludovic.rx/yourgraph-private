<?php

/** Index
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 Your Graph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Page that allow to create graphs
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR . "all.inc.php");

/** Name of the x Axis */
$xAxisName = "";
/** Id of the type of the x axis */
$idXAxisType = -1;
/** Name of th y axis */
$yAxisName = "";
/** Id of the type of the y axis */
$idYAxisType = -1;

/** data on the x axis */
$dataXAxis = "";
/** data on the y axis */
$dataYAxis = "";

/** Defines if the save succeed */
$saveSuccess = null;

/** Defines if the create success modal need to be shown */
$createSuccess = false;

/** Defines if the import modal need to be shown */
$importSuccess = false;

LToolsFilter::filterSaveModal($saveSuccess);
LToolsFilter::filterImportModal($importSuccess);

if (filter_input(INPUT_POST, "createChart", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterChartName($chartName) && LToolsFilter::filterChartType($chartType)) {
        $chartTypeDB = new LChartTypeDB();

        LSession::getInstance()->setChartSession(new LChart(null, $chartName, null, null, $chartTypeDB->getChartTypeById($chartType), array(), false));

        $createSuccess = true;
    }
} else if (filter_input(INPUT_POST, "modifyChart", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterChartName($chartName) && LToolsFilter::filterChartType($chartType)) {
        $chartTypeDB = new LChartTypeDB();
        $chart = LSession::getInstance()->getChartSession();
        $chart->setName($chartName);
        $chart->setTypeChart($chartTypeDB->getChartTypeById($chartType));
        $chart->setIsSaved(false);
        LSession::getInstance()->setChartSession($chart);
    }
} else if (filter_input(INPUT_POST, "createAxes", FILTER_SANITIZE_STRING)) {
    if (
        LToolsFilter::filterXAxisName($xAxisName) &&
        LToolsFilter::filterYAxisName($yAxisName) &&
        LToolsFilter::filterIdXAxisType($idXAxisType)
    ) {
        // If axes haven't been craeted
        if (LSession::getInstance()->getChartSession()->getXAxis() == null && LSession::getInstance()->getChartSession()->getYAxis() == null) {
            $idYAxisType = (new LAxisTypeDB)->getIdNumber();
            // Verify because id could be 0
            if ($idYAxisType !== false) {
                LTools::createAxes(new LAxis(null, $xAxisName, new LAxisType($idXAxisType)), new LAxis(null, $yAxisName, new LAxisType($idYAxisType)));
            }
        } else {
            // Change name
            LSession::getInstance()->getChartSession()->getXAxis()->setName($xAxisName);
            LSession::getInstance()->getChartSession()->getYAxis()->setName($yAxisName);
        }
        LSession::getInstance()->getChartSession()->setIsSaved(false);
    }
} else if (filter_input(INPUT_POST, "addDataContainer", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterNameDataContainer($nameDataContainer)) {
        LSession::getInstance()->getChartSession()->addDataContainer(new LDataContainer(null, $nameDataContainer, LSession::getInstance()->getChartSession()->getXData()));
        LSession::getInstance()->getChartSession()->setIsSaved(false);
    }
} else if (filter_input(INPUT_POST, "modifyDataContainer", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterNameDataContainer($nameDataContainer) && LToolsFilter::filterIdDataContainer($idDataContainer) !== false) {
        if (LSession::getInstance()->getChartSession()->modifyNameDataContainer($idDataContainer, $nameDataContainer)) {
            LSession::getInstance()->getChartSession()->setIsSaved(false);
        }
    }
} else if (filter_input(INPUT_POST, "deleteDataContainer", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterIdDataContainer($idDataContainer) !== false) {
        if (LSession::getInstance()->getChartSession()->deleteDataContainer($idDataContainer)) {
            LSession::getInstance()->getChartSession()->setIsSaved(false);
        }
    }
} else  if (filter_input(INPUT_POST, "addPoint", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterCoordX($coordX, LSession::getInstance()->getChartSession()->getXAxis()->getAxisType()->getName()) && LToolsFilter::filterCoordY($coordY, LSession::getInstance()->getChartSession())) {
        if (LSession::getInstance()->getChartSession()->addDataToDataContainer($coordX, $coordY)) {
            LSession::getInstance()->getChartSession()->setIsSaved(false);
        }
    }
} else  if (filter_input(INPUT_POST, "modifyPoint", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterCoordX($coordX, LSession::getInstance()->getChartSession()->getXAxis()->getAxisType()->getName()) && LToolsFilter::filterCoordY($coordY, LSession::getInstance()->getChartSession()) && LToolsFilter::filterIdCoord($idCoord) !== false) {
        if (LSession::getInstance()->getChartSession()->modifyDataToDataContainer($coordX, $coordY, $idCoord)) {
            LSession::getInstance()->getChartSession()->setIsSaved(false);
        }
    }
} else if (filter_input(INPUT_POST, "deletePoint", FILTER_SANITIZE_STRING)) {
    if (LToolsFilter::filterIdCoord($idCoord) !== false) {
        LSession::getInstance()->getChartSession()->deleteDataToDataContainer($idCoord);
        LSession::getInstance()->getChartSession()->setIsSaved(false);
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php LToolsHTML::writeHead("Create your graph !", "You can create your graph on this page.") ?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php LToolsHTML::writeSideBar() ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php LToolsHTML::writeTopbar() ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between">
                        <h1 class="h3 mb-4 text-gray-800">Create your Graph Now !</h1>
                    </div>

                    <div class="col">
                        <div class="card shadow mb-4 chart">
                            <!-- Card Body -->
                            <div class="card-body">
                                <div id="chart-area" class="h-100">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <?= LToolsHTMLForm::writeOptionsForm(LSession::getInstance()->getChartSession()) ?>
                    </div>

                    <div class="col">
                        <?= LToolsHTMLForm::writeAxesForm() ?>
                    </div>

                    <div class="col">
                        <?= LToolsHTMLForm::writeDataForm() ?>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php LToolsHTML::writeFooter() ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <?php LToolsHTML::writeScrollTop(); ?>
    <?= LToolsHTMLModal::writeLogoutModal() ?>
    <?= LToolsHTMLModal::writeCreateGraphModal() ?>
    <?= LToolsHTMLModal::writeFailSaveModal() ?>
    <?= LToolsHTMLModal::writeSuccessSaveModal() ?>
    <?= LToolsHTMLModal::writeSuccessCreateModal() ?>
    <?= LToolsHTMLModal::writeImportModal() ?>
    <?= LToolsHTMLModal::writeSuccessImportModal() ?>
    <?= LToolsHTMLModal::writeFailImportModal() ?>
    <?php LToolsHTML::writeScripts(); ?>

    <!-- A ajouter pour pouvoir utiliser google chart -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="res/js/LChartJS.js"></script>
    <script>
        <?= LToolsHTML::createChartJS(LSession::getInstance()->getChartSession()) ?>
        <?= LToolsHTMLModal::showSuccessSaveModal($saveSuccess) ?>
        <?= LToolsHTMLModal::showFailSaveModal($saveSuccess) ?>
        <?= LToolsHTMLModal::showSuccessCreateModal($createSuccess) ?>
        <?= LToolsHTMLModal::showSuccessImportModal($importSuccess) ?>
        <?= LToolsHTMLModal::showFailImportModal($importSuccess) ?>
    </script>
</body>

</html>