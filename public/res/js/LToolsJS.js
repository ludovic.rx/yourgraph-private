/** LToolsJS
 *  -------
 *  @file 
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Tools in js for the project
 *  @author ludovic.rx@eduge.ch
 */


/**
 * @brief Tools in js for the projec
 */
class LToolsJS {
    /**
     * Show a modal
     * @param {string} idModal id of the modal
     */
    static showModal(idModal) {
        $('#' + idModal).modal('show');
    }

    static toggleToolTip() {
        // https://getbootstrap.com/docs/4.6/components/tooltips/
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    }
}

LToolsJS.toggleToolTip();