/** LChartJS
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class that handles charts in js
 *  @author ludovic.rx@eduge.ch
 */


/**
 * @brief Chart in js using GoogleChart API
 */
class LChartJS {

    /**
 * Different type of charts
 */
    TYPE_CHARTS = Object.freeze({
        AREA_CHART: "AreaChart",
        LINE_CHART: "LineChart",
        BAR_CHART: "BarChart",
        SCATTER_CHART: "ScatterChart",
        PIE_CHART: "PieChart",
        COLUMN_CHART: "ColumnChart",
        HISTOGRAM: "Histogram",
        COMBO_CHART: "ComboChart",
        DONUT_CHART: "DonutChart",
        STEPPED_AREA_CHART: "SteppedAreaChart"
    });


    /**
     * Create an instance of LChartJS
     * @param {string} InChart json chart from object php
     */
    constructor(InChart) {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        this.chart = InChart;
        this.checkDataType();
    }

    /**
     * Cgheck the type of the data and transform if need to be changed
     */
    checkDataType() {
        for (let i = 0; i < this.chart.dataContainers.length; i++) {
            // à améliorer
            switch (this.chart.xAxis.axisType.name) {
                case "number":
                    this.chart.dataContainers[i].dataXAxis = parseInt(this.chart.dataContainers[i].dataXAxis);
                    break;
                case "date":

                    break;
            }
            switch (this.chart.yAxis.axisType.name) {
                case "number":
                    this.chart.dataContainers[i].dataYAxis = parseInt(this.chart.dataContainers[i].dataYAxis);
                    break;
                case "date":
                    break;
            }

        }
    }

    /**
     * Calback that draws the chart
     * @param {LChartJS} self informations to draw the chart
     */
    drawChart(self) {
        if (self.chart.dataContainers.length != 0) {

            // Create the data table.
            var data = self.getData();

            if (data.getNumberOfRows() > 0) {
                // Set chart options
                var options = self.getOptions();

                // Instantiate and draw our chart, passing in some options.
                var func = self.selectType();
                var chart = new func(document.getElementById('chart-area'));
                chart.draw(data, options);
                self.createSaveButton('chart-area', 'exportSvg');
            }
        }
    }

    /**
     * Get the data and format for google api
     * @returns datas of chart formatted for google api
     */
    getData() {
        var arrayData = [[this.chart.xAxis.name]];
        this.chart.dataContainers.forEach(line => {
            arrayData[0].push(line.name);
        });
        for (let i = 0; i < this.chart.dataContainers[0].points.length; i++) {
            let values = [];
            if (this.chart.typeChart.name == "PieChart" || this.chart.typeChart.name == "DonutChart") {
                values.push(this.chart.dataContainers[0].points[i].xValue.toString());
            } else {
                values.push(this.chart.dataContainers[0].points[i].xValue);
            }
            for (let j = 0; j < this.chart.dataContainers.length; j++) {
                values.push(this.chart.dataContainers[j].points[i].yValue);
            }
            arrayData.push(values);
        }
        return google.visualization.arrayToDataTable(arrayData);
    }

    /**
     * Get options for a chart
     */
    getOptions() {
        // Set chart options
        var options = {
            'title': this.chart.name,
        };
        var hAxis = { title: this.chart.xAxis.name };
        var vAxis = { title: this.chart.yAxis.name };
        switch (this.chart.typeChart.name) {
            case this.TYPE_CHARTS.DONUT_CHART:
                options.pieHole = 0.4;
                break;
            case this.TYPE_CHARTS.BAR_CHART:
                // Invert when a bar chart
                hAxis = { title: this.chart.yAxis.name };
                vAxis = { title: this.chart.xAxis.name };
                break;

        }
        options.vAxis = vAxis;
        options.hAxis = hAxis;
        return options;
    }

    /**
    * Return the function that correspond to the graph
    * @returns google.visualization type
    */
    selectType() {
        switch (this.chart.typeChart.name) {
            case this.TYPE_CHARTS.AREA_CHART:
                return google.visualization.AreaChart;
            case this.TYPE_CHARTS.LINE_CHART:
                return google.visualization.LineChart;
            case this.TYPE_CHARTS.BAR_CHART:
                return google.visualization.BarChart;
            case this.TYPE_CHARTS.SCATTER_CHART:
                return google.visualization.ScatterChart;
            case this.TYPE_CHARTS.PIE_CHART:
            case this.TYPE_CHARTS.DONUT_CHART:
                return google.visualization.PieChart;
            case this.TYPE_CHARTS.COLUMN_CHART:
                return google.visualization.ColumnChart;
            case this.TYPE_CHARTS.HISTOGRAM:
                return google.visualization.Histogram;
            case this.TYPE_CHARTS.COMBO_CHART:
                return google.visualization.ComboChart;
            case this.TYPE_CHARTS.STEPPED_AREA_CHART:
                return google.visualization.SteppedAreaChart;
        }
    }
    /**
     * Create a save button
     * @param {string} divChart id of the div where the chart is
     * @param {string} targetLink id of the div where we can download
     */
    createSaveButton(divChart, targetLink) {
        var a = document.getElementById(targetLink);
        a.href = this.getSvg(document.getElementById(divChart));
        a.download = "graph.svg";
    }

    /**
    * Get the svg href
    * @param {HTMLElement} divChart div of the chart
    * @returns the svg href
    */
    getSvg(divChart) {
        // https://stackoverflow.com/questions/12628968/how-can-i-save-svg-code-as-a-svg-image
        // https://stackoverflow.com/questions/38477972/javascript-save-svg-element-to-file-on-disk
        // https://code-boxx.com/create-save-files-javascript/
        // https://stackoverflow.com/questions/23218174/how-do-i-save-export-an-svg-file-after-creating-an-svg-with-d3-js-ie-safari-an
        // https://css-tricks.com/lodge/svg/09-svg-data-uris/

        var response = "";
        var svg = divChart.getElementsByTagName("svg")[0];
        if (svg != null) {
            svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
            response = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(svg.outerHTML);
        }
        return response;
    }

}

