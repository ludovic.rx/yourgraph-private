<?php

/** All Inc
 *  -------
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief File that contains all the path to specific pages/scripts
 *  @author ludovic.rx@eduge.ch
 */

/** Path to the folder content */
define("CONTENT_PATH",  __DIR__ . DIRECTORY_SEPARATOR . "content" . DIRECTORY_SEPARATOR);
/** Path to the folder of the database scripts */
define("DB_PATH", __DIR__ . DIRECTORY_SEPARATOR . "database" . DIRECTORY_SEPARATOR);
/** Path to the folder that contains class container */
define("CONTAINER_PATH", __DIR__ . DIRECTORY_SEPARATOR . "container" . DIRECTORY_SEPARATOR);
/** Path to folder where we save import */
define("IMPORT_PATH", __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "import". DIRECTORY_SEPARATOR);

/** Path to the sidebar */
define("SIDEBAR_PATH", CONTENT_PATH . "sidebar.inc.php");
/** Path to side bar content when on index */
define("SIDEBAR_INDEX_PATH", CONTENT_PATH . "sidebarIndex.inc.php");
/** Path to the topbar */
define("TOPBAR_PATH", CONTENT_PATH . "topbar.inc.php");
/** Path to the topbar */
define("FOOTER_PATH", CONTENT_PATH . "footer.inc.php");
/**>Path to the scripts */
define("SCRIPT_PATH", CONTENT_PATH . "script.inc.php");
/** Path to the scroll top button */
define("SCROLL_TOP_PATH", CONTENT_PATH . "scrollTop.inc.php");
/** Path to the heads */
define("HEAD_PATH", CONTENT_PATH . "head.inc.php");
/** Path to the heads */
define("USER_CONECTED_PATH", CONTENT_PATH . "userConnected.inc.php");
/** Path to the heads */
define("USER_DISCONNECTED_PATH", CONTENT_PATH . "userDisconnected.inc.php");
