<?php

/** LToolsHTML
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class for all the general functions of the project about HTML
 *  @author ludovic.rx@eduge.ch
 */

/**
 * Class that has only static functions
 * These are the functions for the project
 * These functions are all about HTML
 */
class LToolsHTML
{
    /*--------PUBLIC--------*/

    /**
     * Includes the head for a page
     *
     * @param string $title title of the page. Optional, default "YourGraph"
     * @param string $description description of the page. Optional, default ""
     * @return HTML head
     */
    public static function writeHead($title = "YourGraph", $description = "")
    {
        // Les variables de la fonction sont utilisées dans le fichier inclu
        include_once(HEAD_PATH);
    }

    /**
     * Write the sidebar by include
     *
     * @return HTML sidebar
     */
    public static function writeSideBar()
    {
        include_once(SIDEBAR_PATH);
    }

    /**
     * Write more options on the sidebar when we are on index
     *
     * @return HTML html content when on index
     */
    public static function writeSidebarContent()
    {
        if (basename($_SERVER["SCRIPT_NAME"], ".php") == "index") {
            include_once(SIDEBAR_INDEX_PATH);
        }
    }

    /**
     * Write the top bar by include
     *
     * @return HTML topbar
     */
    public static function writeTopbar()
    {
        include_once(TOPBAR_PATH);
    }

    /**
     * Writes the footer
     *
     * @return HTML footer
     */
    public static function writeFooter()
    {
        include_once(FOOTER_PATH);
    }

    /**
     * Writes the button to scroll to the top   
     *
     * @return HTML scroll button
     */
    public static function writeScrollTop()
    {
        include_once(SCROLL_TOP_PATH);
    }

    /**
     * Write all the script needed to all the pages
     *
     * @return HTML link to scripts 
     */
    public static function writeScripts()
    {
        include_once(SCRIPT_PATH);
    }

    /**
     * Write either the nav for user or the button to log in
     * it depends on if the user is logged in
     *
     * @return HTML 
     */
    public static function writeConnexion()
    {
        if (LSession::getInstance()->getUserSession() !== null) {
            self::writeUserConnected();
        } else {
            self::writeUserDisconnected();
        }
    }

    /**
     * Gets the Bar Chart Icon
     * 
     * @param int $size size of the icon default 2
     * @return string HTML icon
     */
    public static function getBarChartIcon(int $size = 2): string
    {
        return  "<i class=\"fas fa-chart-bar fa-" . $size . "x text-gray-300\"></i>";
    }

    /**
     * Gets the Line chart icon
     *
     * @param int $size size of the icon default 2
     * @return string HTML icon
     */
    public static function getLineChartIcon(int $size = 2): string
    {
        return "<i class=\"fas fa-chart-line fa-" . $size . "x text-gray-300\"></i>";
    }

    /**
     * Gets the area chart icon
     *
     * @param int $size size of the icon default 2
     * @return string HMTL icon
     */
    public static function getAreaChartIcon(int $size = 2): string
    {
        return "<i class=\"fas fa-chart-area fa-" . $size . "x text-gray-300\"></i>";
    }

    /**
     * Gets the pie chart icon
     *
     * @param int $size size of the icon default 2
     * @return string HMTL icon
     */
    public static function getPieChartIcon(int $size = 2): string
    {
        return "<i class=\"fas fa-chart-pie fa-" . $size . "x text-gray-300\"></i>";
    }

    /**
     * Write charts from a user
     *
     * @return string result as HTML
     */
    public static function writeChartsUser()
    {
        $response = "";
        $user = LSession::getInstance()->getUserSession();
        // If user isn't connected
        if ($user !== null) {
            // If programm could get the chart
            if ($charts = (new LChartDB)->getChartsFromUser(LSession::getInstance()->getUserSession()->getId())) {
                foreach ($charts as $chart) {
                    $response .= '<div class="col-xl-3 col-md-6 mb-4">';
                    $response .= '<div class="card border-left-' . self::getColor($chart->getTypeChart()->getName()) . ' shadow h-100 py-2">';
                    $response .= '<div class="card-body">';
                    $response .= '<div class="row no-gutters align-items-center">';
                    $response .= '<div class="col mr-2">';
                    $response .= '<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">';
                    $response .= $chart->getName();
                    $response .= '</div>';
                    $response .= '<div class="h5 mb-0 font-weight-bold text-gray-800">';
                    $response .= '<a href="api/open.php?idChart=' . $chart->getId() . '" class="btn btn-info btn-circle btn-sm">';
                    $response .= '<i class="fas fa-pencil-alt"></i>';
                    $response .= '</a>';
                    $response .= '<a href="api/delete.php?idChart=' . $chart->getId() . '" class="btn btn-danger btn-circle btn-sm mx-2">';
                    $response .= '<i class="fas fa-trash"></i>';
                    $response .= '</a>';
                    $response .= '</div>';
                    $response .= '</div>';
                    $response .= '<div class="col-auto">';
                    $response .= self::getIcon($chart->getTypeChart()->getName());
                    $response .= '</div>';
                    $response .= '</div>';
                    $response .= '</div>';
                    $response .= '</div>';
                    $response .= '</div>';
                }
            }
        }
        return $response;
    }

    /**
     * Write script to create a chart
     *
     * @param LChart|null $chart chart that is in session
     * @return string js function to create chart 
     */
    public static function createChartJS(?LChart $chart): string
    {
        $response = "";
        if ($chart != null) {
            if ($chart->getXAxis() != null) {
                $response .= "chart = new LChartJS(" . LTools::encodeChart() . ");";
                $response .= " google.charts.setOnLoadCallback(function() {";
                $response .= "  chart.drawChart(chart)";
                $response .= "  });";
            }
        }
        return $response;
    }

    /**
     * Get an icon depending the name that we give
     *
     * @param string $name name of the icon
     * @param integer $size size of the icon
     * @return string HTML icon
     */
    public static function getIcon(string $name, int $size = 2): string
    {
        $icon = "";
        switch ($name) {
            case "AreaChart":
                $icon = self::getAreaChartIcon($size);
                break;
            case "LineChart";
                $icon = self::getLineChartIcon($size);
                break;
            case "BarChart":
                $icon =  self::getBarChartIcon($size);
                break;
            case "PieChart":
                $icon = self::getPieChartIcon($size);
                break;
            default:
            $icon = $name;
            break;
        }
        return $icon;
    }

    /*--------END PUBLIC--------*/

    /*--------PRIVATE--------*/

    /**
     * Writes the navbar for a connected user
     *
     * @return HTML nav for user
     */
    private static function writeUserConnected()
    {
        include_once(USER_CONECTED_PATH);
    }

    /**
     * Write the button to log in for a user
     *
     * @return HTML button to log in
     */
    private static function writeUserDisconnected()
    {
        include_once(USER_DISCONNECTED_PATH);
    }

    /**
     * Write the class to the border depending the chart type
     *
     * @param string $chartTypeName name of the chart type
     * @return string class HTML
     */
    private static function getColor($chartTypeName): string
    {
        $response = "";
        switch ($chartTypeName) {
            case 'AreaChart':
                $response .= "info";
                break;
            case 'LineChart':
                $response .= "success";
                break;
            case 'BarChart':
            default:
                $response .= "primary";
                break;
        }
        return $response;
    }

    /*--------END PRIVATE--------*/
}
