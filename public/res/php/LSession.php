<?php

/** LSession
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class for session
 *  @author ludovic.rx@eduge.ch
 */

/**
 * Class that handles the session 
 */
class LSession
{
    /** Instance of the object */
    private static $objInstance = null;

    /** Index of the user */
    private const INDEX_USER = "user";

    /** Index of the chart that is stored */
    private const INDEX_CHART = "chart";

    /**
     * Create an instance of LSession
     */
    private function __construct()
    {
        if (session_id() === "") {
            session_start();
        }

        // Sets default values
        if ($this->getUserSession() == null) {
            $this->setUserSession(null);
        }
        if ($this->getChartSession() == null) {
            $this->setChartSession(null);
        }
    }

    /**
     * Gets the instance of LSession
     * If the instance is not created, instantiate, else return the instance
     * @return LSession
     */
    public static function getInstance(): LSession
    {
        if (self::$objInstance === null) {
            self::$objInstance = new LSession();
        }
        return self::$objInstance;
    }

    /**
     * Set a user in the session
     *
     * @param LUser|null $user user to set
     * @return LUser user that has been just addedd
     */
    public function setUserSession(?LUser $user)
    {
        $this->setValueSession(self::INDEX_USER, $user);
        return $user;
    }

    /**
     * Get user in the session
     *
     * @return null|LUser null if failed, else LUser
     */
    public function getUserSession()
    {
        return $this->getValueSession(self::INDEX_USER);
    }

    /**
     * Set a chart in the session
     *
     * @param LChart|null $chart chart to set
     * @return LChart chart that has been just addedd
     */
    public function setChartSession(?LChart $chart)
    {
        $this->setValueSession(self::INDEX_CHART, $chart);
        return $chart;
    }

    /**
     * Get chart in the session
     *
     * @return null|LChart null if failed, else LUser
     */
    public function getChartSession()
    {
        return $this->getValueSession(self::INDEX_CHART);
    }

    /**
     * Destroy the session
     *
     * @return void
     */
    public function destroySession()
    {
        $_SESSION = null;
        session_destroy();
    }

    /**
     * Get a value from the session thanks the key
     *
     * @param string $key key in the session
     * @return null|mixed null if failed, elsevc   result that is stored in the session
     */
    private function getValueSession(string $key)
    {
        $result = null;
        if (isset($_SESSION[$key])) {
            $result = $_SESSION[$key];
        }
        return $result;
    }

    /**
     * Set a value in the session
     *
     * @param string $key key in the session
     * @param mixed $value value that is associated to the key
     * @return void
     */
    private function setValueSession(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }
}
