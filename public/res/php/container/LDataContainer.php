<?php

/** LDataContainer
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LDataContainer
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a container for a data container in a chart
 * a data container can be a bar or a line in a chart for example
 */
class LDataContainer implements JsonSerializable
{

    /** Id of the data container */
    private $id;

    /** name of the data container */
    private $name;

    /** Points of the data container */
    private $points;

    /**
     * Set the id of the line
     *
     * @param integer|null $id id of the line
     * @return void
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets the id of the line
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set the name 
     *
     * @param string $name name of the line
     * @return void
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * Get the name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set points 
     *
     * @param array|null $points points int the data container
     * @return void
     */
    private function setPoints(?array $points): void
    {
        $this->points = $points;
    }

    /**
     * Get points of the data container
     *
     * @return array|null
     */
    public function getPoints(): ?array
    {
        return $this->points;
    }

    /**
     * Create an instance of LDataContainer
     *
     * @param integer|null $InId id of the line
     * @param string $InName name of the line
     * @param array|null $InPoints array of LPoint
     */
    public function __construct(?int $InId = 0, string $InName = "", ?array $InPoints = array())
    {
        $this->setId($InId);
        $this->setName($InName);
        $this->setPoints($InPoints);
    }

    /**
     * Get the x coord
     *
     * @param integer $index index of the point
     * @return integer|date gets a data or a number as x coord 
     */
    public function getCoordX($index)
    {
        return $this->getPoints()[$index]->getXValue();
    }

    /**
     * Get the y coord
     *
     * @param integer $index index of the point
     * @return string|int gets a string or a number as y coord 
     */
    public function getCoordY($index)
    {
        $response = "";
        if ($index < count($this->getPoints())) {
            $response = $this->getPoints()[$index]->getYValue();
        }
        return $response;
    }

    /**
     * Modify a point in the line
     *
     * @param array $point coord x and y
     * @param integer $index index of the data
     * @return void
     */
    public function modifyPoint(LPoint $point, int $index)
    {
        $this->points[$index] = $point;
    }

    /**
     * Add a point on the data container
     *
     * @param LPoint $point point on the data container
     * @return void
     */
    public function addPoint(LPoint $point)
    {
        array_push($this->points, $point);
    }

    /**
     * Delete a point in the data container
     *
     * @param integer $index of the point to delete
     * @return void
     */
    public function deletePoint(int $index)
    {
        unset($this->points[$index]);
        $this->points = array_values($this->points);
    }

    /**
     * Serialize for json
     * @copyright 2019 https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members
     *
     * @return string
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
