<?php

/** LPoint
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LPoint
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a for a point in a graph
 * A point is a data in the graph that is defined by a x and a y value
 */
class LPoint implements JsonSerializable
{
    /** Id of the point */
    private $id;

    /** y value of the point */
    private $xValue;

    /** y value of the point */
    private $yValue;

    /**
     * Set the id of the graph
     *
     * @param integer|null $id id of the graph
     * @return void
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets the id of the graph
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set the x value of the point
     *
     * @param mixed $xValue x value of the point
     * @return void
     */
    private function setXValue($xValue): void
    {
        $this->xValue = $xValue;
    }

    /**
     * Get the X Value of the point
     *
     * @return mixed|null
     */
    public function getXValue()
    {
        return $this->xValue;
    }

    /**
     * Set the y value of the point
     *
     * @param mixed $yValue y value of the point
     * @return void
     */
    private function setYValue($yValue): void
    {
        $this->yValue = $yValue;
    }

    /**
     * Get the Y Value of the point
     *
     * @return mixed|null
     */
    public function getYValue()
    {
        return $this->yValue;
    }

    /**
     * Create an instance of LPoint
     *
     * @param integer|null $InId id of the point
     * @param string|float|date $InXValue value on x, can be a date, a string or a integer
     * @param float $InYValue value on Y, will always be a number
     */
    public function __construct(?int $InId = null, $InXValue = "", float $InYValue = 0)
    {
        $this->setId($InId);
        $this->setXValue($InXValue);
        $this->setYValue($InYValue);
    }

    /**
     * Serialize for json
     * @copyright 2019 https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members
     *
     * @return string
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
