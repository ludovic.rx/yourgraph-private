<?php

/** LChart
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LChart
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a container for a graph
 */
class LChart implements JsonSerializable
{
    /** Id of the graph */
    private $id;

    /** name of the graph */
    private $name;

    /** X Axis of the graph */
    private $xAxis;

    /** Y Axis of the graph */
    private $yAxis;

    /** Type of the graph */
    private $typeChart;

    /** Array of LDataContainer  */
    private $dataContainers;

    /** Tell if the chart is saved or not */
    private $isSaved;

    /**
     * Set the id of the graph
     *
     * @param integer|null $id id of the graph
     * @return void
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets the id of the graph
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set the anme of the graph
     *
     * @param string $name name of the graph
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Gets the id of the graoh
     *
     * @return integer
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the X Axis of the graph
     *
     * @param LAxis|null $xAxis x axis of the graph
     * @return void
     */
    public function setXAxis(?LAxis $xAxis): void
    {
        $this->xAxis = $xAxis;
    }

    /**
     * Get the XAxis og the graph
     *
     * @return LAxis|null
     */
    public function getXAxis(): ?LAxis
    {
        return $this->xAxis;
    }

    /**
     * Set the Y Axis of the graph
     *
     * @param LAxis|null $yAxis y axis of the graph
     * @return void
     */
    public function setYAxis(?LAxis $yAxis): void
    {
        $this->yAxis = $yAxis;
    }

    /**
     * Get the YAxis og the graph
     *
     * @return LAxis|null
     */
    public function getYAxis(): ?LAxis
    {
        return $this->yAxis;
    }

    /**
     * Set the type of the chart
     *
     * @param LChartType|null $typeChart type of the chart
     * @return void
     */
    public function setTypeChart(?LChartType $typeChart): void
    {
        $this->typeChart = $typeChart;
    }

    /**
     * Get the type of the chart
     *
     * @return LChartType|null
     */
    public function getTypeChart(): ?LChartType
    {
        return $this->typeChart;
    }

    /**
     * Set the data container for the graph
     *
     * @param array|null $dataContainers containers of data
     * @return void
     */
    public function setDataContainers(?array $dataContainers): void
    {
        $this->dataContainers = $dataContainers;
    }


    /**
     * Get isSaved
     *
     * @return bool
     */
    public function getIsSaved(): bool
    {
        return $this->isSaved;
    }

    /**
     * Set true or false for isSaved
     *
     * @param bool $state true if is saved, else false
     * @return void
     */
    public function setIsSaved(bool $state): void
    {
        $this->isSaved = $state;
    }


    /**
     * Get the data containers from the graph
     *
     * @return array|null
     */
    public function getDataContainers(): ?array
    {
        return $this->dataContainers;
    }

    /**
     * Create an instance of Graph
     *
     * @param integer|null $InId id of the graph
     * @param string $InName name of the graph
     * @param LAxis $InXAxis X AXis of the graph
     * @param LAxis $InYAxis Y Axis of the graph
     * @param LChartType $InTypeChart type of the chart
     * @param array|null $InDataContainers array of LDataContainer
     * @param bool $InIsSaved true if is saved, false if gchart is not saved in the db
     */
    public function __construct(?int $InId = 0, string $InName = "", LAxis $InXAxis = null,  LAxis $InYAxis = null, LChartType $InTypeChart = null, ?array $InDataContainers = null, $InIsSaved = false)
    {
        $this->setId($InId);
        $this->setName($InName);
        $this->setXAxis($InXAxis);
        $this->setYAxis($InYAxis);
        $this->setTypeChart($InTypeChart);
        $this->setDataContainers($InDataContainers);
        $this->setIsSaved($InIsSaved);
    }

    /**
     * Add a data container to the chart
     *
     * @param LDataContainer $dataContainer the data container such as a line or a bar
     * @return void
     */
    public function addDataContainer(LDataContainer $dataContainer)
    {
        array_push($this->dataContainers, $dataContainer);
    }

    /**
     * Add data to the lines
     *
     * @param string|int|date $valueX
     * @param array $valuesY array of int
     * @return boolean true if data could have been added, else false
     */
    public function addDataToDataContainer($valueX, array $valuesY): bool
    {
        $returnResult = false;
        if (
            LTools::verifyType($this->getXAxis()->getAxisType()->getName(), $valueX) &&
            LTools::verifyArrayType($this->getYAxis()->getAxisType()->getName(), $valuesY)
        ) {
            $dataContainers = $this->getDataContainers();
            for ($i = 0; $i < count($dataContainers); $i++) {
                $yValue = 0;
                if (count($valuesY) > $i) {
                    $yValue = $valuesY[$i];
                }
                $dataContainers[$i]->addPoint(new LPoint(null, $valueX, $yValue));
            }
            $returnResult = true;
        }
        return $returnResult;
    }

    /**
     * Add data to the lines
     *
     * @param string|int|date $valueX
     * @param array $valuesY array of int
     * @param integer $index index of data to change
     * @return boolean true if data could have been modify, else false
     */
    public function modifyDataToDataContainer($valueX, array $valuesY, int $index): bool
    {
        $returnResult = false;
        if (
            LTools::verifyType($this->getXAxis()->getAxisType()->getName(), $valueX) &&
            LTools::verifyArrayType($this->getYAxis()->getAxisType()->getName(), $valuesY)
        ) {
            $dataContainers = $this->getDataContainers();
            for ($i = 0; $i < count($dataContainers); $i++) {
                $dataContainers[$i]->modifyPoint(new LPoint(null, $valueX, $valuesY[$i]), $index);
            }
            $returnResult = true;
        }
        return $returnResult;
    }

    /**
     * Delete a data to data container
     *
     * @param integer $index index of data to change
     * @return boolean, true if succeed else false
     */
    public function deleteDataToDataContainer(int $index): bool
    {
        $returnResult = false;
        foreach ($this->dataContainers as $dataContainer) {
            $dataContainer->deletePoint($index);
            $returnResult = true;
        }
        return $returnResult;
    }

    /**
     * Modify a data
     *
     * @param int $index index of the data
     * @param string $newName new value 
     * @return boolean true if succeed, else false
     */
    public function modifyNameDataContainer($index, $newName): bool
    {
        $returnResult = false;
        if ($index < count($this->dataContainers)) {
            $this->dataContainers[$index]->setName($newName);
            $returnResult = true;
        }
        return $returnResult;
    }

    /**
     * Delete a line
     *
     * @param integer $index index of the line
     * @return boolean true if succeed, else false
     */
    public function deleteDataContainer(int $index): bool
    {
        $returnResult = false;
        if ($index < count($this->getDataContainers())) {
            unset($this->dataContainers[$index]);
            $this->dataContainers = array_values($this->dataContainers);
            $returnResult = true;
        }
        return $returnResult;
    }

    /**
     * Get the x data of the cahrt
     *
     * @return array
     */
    public function getXData(): array
    {
        $response = array();
        if (count($this->getDataContainers()) > 0) {
            foreach ($this->getDataContainers()[0]->getPoints() as $point) {

                array_push($response, new LPoint(null, $point->getXValue(), 0));
            }
        }
        return $response;
    }

    /**
     * Serialize for json
     * @copyright 2019 https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members
     *
     * @return string
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
