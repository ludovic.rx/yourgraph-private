<?php

/** LAxisType
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LAxisType
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a container for a type of axis
 */
class LAxisType extends LType
{
   
}
