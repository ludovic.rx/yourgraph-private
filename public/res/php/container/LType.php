<?php

/** LAxisType
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LAxisType
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a container for a type of axis
 */
abstract class LType implements JsonSerializable
{
    /** Id of the type */
    private $id;

    /** name of the type */
    private $name;

    /**
     * Set the id of the type
     *
     * @param integer|null $id id of the type
     * @return void
     */
    private function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets the id of the type
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Sets the name of the type
     *
     * @param string|null $name data of the type
     * @return void
     */
    private function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get the name of the type
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Create an instance of LType
     *
     * @param integer|null $InId id of the axis
     * @param string|null $InName name of the axis
     */
    public function __construct(?int $InId = 0, ?string $InName = "")
    {
        $this->setId($InId);
        $this->setName($InName);
    }

    /**
     * Serialize for json
     * @copyright 2019 https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members
     *
     * @return string
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
