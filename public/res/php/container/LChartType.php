<?php

/** LChartType
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LChartType
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a container for a type of chart
 */
class LChartType extends LType
{
  
}