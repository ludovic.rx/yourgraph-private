<?php

/** LAxis
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LAxis
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a container for an axis of a graph
 */
class LAxis implements JsonSerializable
{
    /** Id of the axis */
    private $id;

    /** name of the axis */
    private $name;

    /** type of the axis */
    private $axisType;

    /**
     * Set the id of the axis
     *
     * @param integer|null $id id of the axis
     * @return void
     */
    private function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets the id of the axis
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Sets the name of the axis
     *
     * @param string $name data of the axis
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get the name of the axis
     *
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets the axis type
     *
     * @param LAxisType|null $axisType type of the axis
     * @return void
     */
    public function setAxisType(?LAxisType $axisType): void
    {
        $this->axisType = $axisType;
    }

    /**
     * Gets the type of the axis
     *
     * @return LAxisType|null
     */
    public function getAxisType(): ?LAxisType
    {
        return $this->axisType;
    }

    /**
     * Create an instance of LAxis
     *
     * @param integer|null $InId id of the axis
     * @param string $InName name of the axis
     * @param LAxisType $InAxisType type of the axis
     */
    public function __construct(?int $InId = 0, string $InName = null, LAxisType $InAxisType = null)
    {
        $this->setId($InId);
        $this->setName($InName);
        $this->setAxisType($InAxisType);
    }

    /**
     * Serialize for json
     * @copyright 2019 https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members
     *
     * @return string
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}