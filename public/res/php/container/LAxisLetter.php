<?php

/** LAxisLetter
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LAxisLetter
 *  @author Ludovic Roux
 */

/**
 * Abstract class of axis letter
 * idea from : https://stackoverflow.com/questions/254514/enumerations-on-php
 */
abstract class LAxisLetter {
    /** X Axis letter */
    const X = "X";
    /** Y Axis Letter */
    const Y = "Y";
}