<?php

/** LUser
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LUser
 *  @author Ludovic Roux
 */

/**
 * @brief This class is a container for a user
 */
class LUser
{
    /**> Id of the user */
    private $id;

    /**> First Name of the user */
    private $firstName;

    /**> Last name of the user */
    private $lastName;

    /**> Email of the user */
    private $email;

    /**
     * Set the id of the user
     *
     * @param integer $id id of the user
     * @return void
     */
    private function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets the id of the user
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Sets the first name of the user
     *
     * @param string $firstName first name of the user
     * @return void
     */
    private function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * Gets the firstName of the user
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * Sets the last name of the user
     *
     * @param string $lastName last name of the user
     * @return void
     */
    private function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * Gets the last name of the user
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Sets the email of the user
     *
     * @param string $email email of the user
     * @return void
     */
    private function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * Gets the email of the user
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Create an instance of user
     *
     * @param integer $InId Id of the user
     * @param string $InFirstName first name of the user
     * @param string $InLastName last name of the ser
     * @param string $InEmail email of the user
     */
    public function __construct(int $InId = 0, string $InFirstName = "", string $InLastName = "", string $InEmail = "")
    {
        $this->setId($InId);
        $this->setFirstName($InFirstName);
        $this->setLastName($InLastName);
        $this->setEmail($InEmail);
    }

    /**
     * Write the first name and the name of the user
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }
}
