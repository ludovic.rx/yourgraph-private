<?php

/** LToolsHTMLModal
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief These are function about modal
 *  @author ludovic.rx@eduge.ch
 */

/**
 * Class that has only static functions
 * These are function about modal
 */
class LToolsHTMLModal
{
    /*--------CONSTANTS--------*/

    /** ID create graph modal */
    private const ID_CREATE_GRAPH = "createGraphModal";
    /** Id of the success create modal */
    private  const ID_SUCCESS_CREATE = "successCreateModal";
    /** Id of the success save modal */
    private const ID_SUCCESS_SAVE = "successSaveModal";
    /** ID of the fail save modal */
    private const ID_FAIL_SAVE = "failSaveModal";
    /** id of the success delete modal */
    private const ID_SUCCESS_DELETE = "sucessDeleteModal";
    /** Id of the fail delete modal */
    private const ID_FAIL_DELETE = "failDeleteModal";
    /** ID of the logout modal */
    private const ID_LOUGOUT = "logoutModal";
    /** ID of the import modal */
    private const ID_IMPORT = "importModal";
    /** ID of the fail import modal */
    private const ID_FAIL_IMPORT = "failImportModal";
    /** ID of the success import modal */
    private const ID_SUCCESS_IMPORT = "successImportModal";

    /*--------END CONSTANTS--------*/

    /*--------PUBLIC--------*/

    /**
     * Writes the create graph modal
     *
     * @return string create graph modal
     */
    public static function writeCreateGraphModal(): string
    {
        return self::showModal(self::ID_CREATE_GRAPH, "Create a new Graph", LToolsHTMLForm::writeCreateGraphForm());
    }

    /**
     * Write the success create save modal
     *
     * @return string success create modal
     */
    public static function writeSuccessCreateModal(): string
    {
        return self::showModal(self::ID_SUCCESS_CREATE, "Success", "Your graph has been well created!", LToolsHTMLModal::writeButtonCloseModal("Ok", "success"));
        // include_once(SUCCESS_CREATE_PATH);
    }

    /**
     * Writes the success save modal
     *
     * @return string create the success save modal
     */
    public static function writeSuccessSaveModal(): string
    {
        return self::showModal(self::ID_SUCCESS_SAVE, "Success", "Your graph successfully saved!", LToolsHTMLModal::writeButtonCloseModal("Ok"));
    }

    /**
     * Write the fail save modal
     *
     * @return string fail save modal
     */
    public static function writeFailSaveModal(): string
    {
        return self::showModal(self::ID_FAIL_SAVE, "Fail", "Your graph could not be saved!", LToolsHTMLModal::writeButtonCloseModal("Ok"));
    }

    /**
     * Writes the success delete modal
     *
     * @return string create the success delete modal
     */
    public static function writeSuccessDeleteModal(): string
    {
        return self::showModal(self::ID_SUCCESS_DELETE, "Success", "Your graph successfully deleted!", LToolsHTMLModal::writeButtonCloseModal("Ok"));
    }

    /**
     * Write the delete save modal
     *
     * @return string fail delete modal
     */
    public static function writeFailDeleteModal(): string
    {
        return self::showModal(self::ID_FAIL_DELETE, "Fail", "Your graph could not be deleted!", LToolsHTMLModal::writeButtonCloseModal("Ok"));
        // include_once(FAIL_DELETE_PATH);
    }

    /**
     * Writes the logout modal
     *
     * @return string result as HTML
     */
    public static function writeLogoutModal(): string
    {
        $footer = LToolsHTMLModal::writeButtonCloseModal("Cancel");
        $footer .=  '<a class="btn btn-primary" href="api/logout.php">Logout</a>';
        return self::showModal(self::ID_LOUGOUT, "Ready to Leave?", "Select \"Logout\" below if you are ready to end your current session.", $footer);
    }

    /**
     * Write the import modal
     *
     * @return string result as HTML
     */
    public static function writeImportModal(): string
    {
        return self::showModal(self::ID_IMPORT, "Import", LToolsHTMLForm::writeImportForm());
    }

    /**
     * Write the popu fail import modal
     *
     * @return string result as HTML
     */
    public static function writeFailImportModal(): string
    {
        return self::showModal(self::ID_FAIL_IMPORT, "Fail", "The importation failed!", LToolsHTMLModal::writeButtonCloseModal("Ok"));
    }

    /**
     * Write the popu fail import modal
     *
     * @return string result as HTML
     */
    public static function writeSuccessImportModal(): string
    {
        return self::showModal(self::ID_SUCCESS_IMPORT, "Success", "The importation worked!", LToolsHTMLModal::writeButtonCloseModal("Ok"));
    }

    /**
     * Show a success create graph modal
     *
     * @param boolean $state true if modal needs to be shown
     * @return string function js if we need show modal
     */
    public static function showSuccessCreateModal($state): string
    {
        return self::showModalJS($state, self::ID_SUCCESS_CREATE);
    }

    /**
     * Show the success save modal
     *
     * @param bool $state true need to save modal, else false
     * @return string js function to write
     */
    public static function showSuccessSaveModal($state): string
    {
        return self::showModalJS($state, self::ID_SUCCESS_SAVE);
    }

    /**
     * Show a save fail modal
     *
     * @param boolean $state true if modal needs to be shown
     * @return string function js if we need show modal
     */
    public static function showFailSaveModal($state): string
    {
        return self::showFailModal($state, self::ID_FAIL_SAVE);
    }

    /**
     * Show the success save modal
     *
     * @param bool $state true if modal needs to be shown
     * @return string js function to write
     */
    public static function showSuccessDeleteModal($state): string
    {
        return self::showModalJS($state, self::ID_SUCCESS_DELETE);
    }

    /**
     * Show a delete fail modal
     *
     * @param boolean $state true if modal needs to be shown
     * @return string function js if we need show modal
     */
    public static function showFailDeleteModal($state): string
    {
        return self::showFailModal($state, self::ID_FAIL_DELETE);
    }
    /**
     * Show the success import modal
     *
     * @param bool $state if true need to show modal
     * @return string js function to write
     */
    public static function showSuccessImportModal($state): string
    {
        return self::showModalJS($state, self::ID_SUCCESS_IMPORT);
    }

    /**
     * Show a import fail modal
     *
     * @param boolean $state true if modal needs to be shown
     * @return string function js if we need show modal
     */
    public static function showFailImportModal($state): string
    {
        return self::showFailModal($state, self::ID_FAIL_IMPORT);
    }

    /*--------END PUBLIC--------*/

    /*--------PRIVATE--------*/

    /**
     * Show a modal
     *
     * @param boolean $state true if needs to be show
     * @param string $idModal id of the HTML modal
     * @return string function js if true, else false
     */
    private static function showModalJS($state, $idModal): string
    {
        $result = "";
        if ($state === true) {
            $result = "LToolsJS.showModal(\"$idModal\");";
        }
        return $result;
    }

    /**
     * Shows a modal 
     *
     * @param string $id id of the modal
     * @param string $title title of the modal
     * @param string $content content of the modaé
     * @param string $footer content on footer 
     * @return string
     */
    private static function showModal(string $id, string $title, string $content, string $footer = ""): string
    {
        $response = "";
        $idLabel = "label" . lcfirst($id);
        $response .= '<div class="modal fade" id="' . $id . '" tabindex="-1" role="dialog" aria-labelledby="' . $idLabel . '" aria-hidden="true">';
        $response .= '<div class="modal-dialog" role="document">';
        $response .= '<div class="modal-content">';
        $response .= '<div class="modal-header">';
        $response .= '<h5 class="modal-title" id="' . $idLabel . '">';
        $response .= $title;
        $response .= '</h5>';
        $response .= '<button class="close" type="button" data-dismiss="modal" aria-label="Close">';
        $response .= '<span aria-hidden="true">×</span>';
        $response .= '</button>';
        $response .= '</div>';
        // if there is no footer, we only write the content
        if (strlen($footer) === 0) {
            $response .= $content;
        } else {
            $response .= '<div class="modal-body">';
            $response .= $content;
            $response .= '</div>';
            $response .= '<div class="modal-footer">';
            $response .= $footer;
            $response .= '</div>';
        }
        $response .= '</div>';
        $response .= '</div>';
        $response .= '</div>';
        return $response;
    }

    /**
     * Show a fail modal
     *
     * @param bool $state show the modal when the state is false
     * @param string $name name of the modal to show
     * @return string js function to write
     */
    private static function showFailModal($state, $name): string
    {
        $response = "";
        if ($state === false) {
            $response = self::showModalJS(true, $name);
        }
        return $response;
    }

    /**
     * Write the button to close a modal
     *
     * @param string $text text on the button
     * @param string $color color of the button
     * @return string result as HTML
     */
    public static function writeButtonCloseModal(string $text, $color = "secondary"): string
    {
        return '<button class="btn btn-' . $color . '" type="button" data-dismiss="modal">' . $text . '</button>';
    }

    /*--------END PRIVATE--------*/
}
