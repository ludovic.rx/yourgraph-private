<li class="nav-item">
    <a class="nav-link" href="graphs.php">
        <i class="far fa-fw fa-folder-open"></i>
        <span>Open Graph</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="api/save.php">
        <i class="far fa-fw fa-save"></i>
        <span>Save Graph</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Data
</div>

<li class="nav-item">
    <a class="nav-link" href="#" data-toggle="modal" data-target="#importModal">
        <i class="fas fa-fw fa-download"></i>
        <span>Import</span></a>
</li>

<!-- Nav Item - Exprot Collapse Menu -->
<li class="nav-item">
    <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-upload"></i>
        <span>Export</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="api/exportCSV.php" id="exportCsv">CSV</a>
            <a class="collapse-item" href="#" id="exportSvg">SVG</a>
        </div>
    </div>
</li>