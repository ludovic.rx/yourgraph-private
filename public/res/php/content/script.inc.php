<!-- Bootstrap core JavaScript-->
<script src="res/vendor/jquery/jquery.min.js"></script>
<script src="res/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="res/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="res/js/sb-admin-2.min.js"></script>

<!-- Tools JS -->
<script src="res/js/LToolsJS.js"></script>