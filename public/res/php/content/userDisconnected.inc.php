<a href="login.php" class="btn btn-light btn-icon-split">
    <span class="text">Log In Now</span>
    <span class="icon text-gray-600">
        <i class="fas fa-arrow-right"></i>
    </span>
</a>