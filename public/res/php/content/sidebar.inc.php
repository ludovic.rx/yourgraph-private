 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

     <!-- Sidebar - Brand -->
     <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
         <div class="sidebar-brand-icon rotate-n-15">
             <i class="fas fa-chart-bar"></i>
         </div>
         <div class="sidebar-brand-text mx-3">Your Graph</div>
     </a>

     <!-- Divider -->
     <hr class="sidebar-divider my-0">

     <!-- Nav Item - New Graph -->
     <li class="nav-item">
         <a class="nav-link" href="#" data-toggle="modal" data-target="#createGraphModal">
             <i class="far fa-fw fa-plus-square"></i>
             <span>New Graph</span></a>
     </li>

    <?= LToolsHTML::writeSidebarContent()  ?>

     <!-- Divider -->
     <hr class="sidebar-divider d-none d-md-block">

     <!-- Sidebar Toggler (Sidebar) -->
     <div class="text-center d-none d-md-inline">
         <button class="rounded-circle border-0" id="sidebarToggle"></button>
     </div>

 </ul>
 <!-- End of Sidebar -->