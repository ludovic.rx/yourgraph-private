<?php

/** LToolsHTMLForm
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class that makes query for html form
 *  @author ludovic.rx@eduge.ch
 */

/**
 * Class that makes query for html form
 */
class LToolsHTMLForm
{
    /*--------CONSTANTS--------*/

    /** Size for a label with input in a form for name of data container */
    const SIZE_LABEL_FORM_DATA_CONTAINER_NAME = 10;
    /** Size for a label of coord x or y in the form */
    const SIZE_LABEL_FORM_DATA_CONTAINER_COORD = 2;

    /** Name of the inpu to import file */
    const NAME_INPUT_FILE_IMPORT = "importFile";

    /*--------END CONSTANTS--------*/

    /*--------PUBLIC--------*/

    /**
     * Write the form to create a graph
     *
     * @return string result as HTML
     */
    public static function writeCreateGraphForm(): string
    {
        $response = "";
        $response .= '<form method="POST" action="index.php">';
        $response .= '<div class="modal-body">';
        $response .= '<div class="form-row">';
        $response .= '<div class="col-md-6 mb-3">';
        $response .= '<label for="chartName">Name of your graph</label>';
        $response .= '<input required ' . self::writeTypeInput("string") . ' class="form-control" id="chartName" name="chartName" placeholder="My Super Graph!">';
        $response .= '</div>';
        $response .= '</div>';
        $response .= '<label>Type of your graph</label>';
        $response .=  self::writeRadioTypeChart();
        $response .= '</div>';
        $response .= '<div class="modal-footer">';
        $response .=  LToolsHTMLModal::writeButtonCloseModal("Cancel");
        $response .= '<button name="createChart" value="create" class="btn btn-success btn-icon-split">';
        $response .= '<span class="icon text-white-50">';
        $response .= '<i class="fas fa-check"></i>';
        $response .= '</span>';
        $response .= '<span class="text">Create</span>';
        $response .= '</button>';
        $response .= '</div>';
        $response .= '</form>';
        return $response;
    }


    /**
     * Writes the select axis type
     *
     * @param integer $idTypeSelected id of the type that is selected. Default -1 means that nothing is selected
     * @param boolean $isDisabled if true, we disabled the select
     * @return string select as HTML
     */
    public static function writeSelectAxisType(int $idTypeSelected = -1, bool $isDisabled = false): string
    {
        static $numSelect = 0;
        $idSelect = "selectType$numSelect";
        $result  = "<div class=\"input-group\">";
        $result .= "<div class=\"input-group-prepend\">";
        $result .= "<label class=\"input-group-text\" for=\"$idSelect\">Type</label>";
        $result .= "</div>";
        $result .= "<select class=\"custom-select\" id=\"$idSelect\" required name=\"$idSelect\" " . self::writeDisabled($isDisabled) . " >";
        $result .= "<option value=\"\">Choose...</option>";
        foreach ((new LAxisTypeDB())->getAllAxisType() as $axisType) {
            $result .= "<option value=\"" . $axisType->getId() . "\" " . self::writeSelected($axisType->getId(), $idTypeSelected) . " >" . $axisType->getName() . "</option>";
        }
        $result .= "</select>";
        $result .= "</div>";
        $numSelect++;

        return $result;
    }

    /**
     * Write the form to add the axes
     *
     * @return string result as HTML
     */
    public static function writeAxesForm(): string
    {
        $response = "";
        if (($chart = LSession::getInstance()->getChartSession()) !== null) {
            $xAxisName = "";
            $idXAxisType = 0;
            $yAxisName = "";
            $idYAxisType = (new LAxisTypeDB)->getIdNumber();
            $show = true;
            $isSelectXDisabled = false;
            // Select type y is always disabled
            $isSelectYDisabled = true;
            // If we achieved to get id of type number
            if ($idYAxisType !== false) {
                // Get informations from session if axe is not null
                if ($chart->getXAxis() !== null) {
                    $xAxisName = $chart->getXAxis()->getName();
                    $idXAxisType = $chart->getXAxis()->getAxisType()->getId();
                    $show = false;
                    $isSelectXDisabled = true;
                }
                if ($chart->getYAxis() !== null) {
                    $yAxisName = $chart->getYAxis()->getName();
                }

                $response .= '<div class="card shadow mb-4">';
                $response .= '<a href="#collapseAxes" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseAxes">';
                $response .= '<h6 class="m-0 font-weight-bold text-primary">Axes</h6>';
                $response .= '</a>';
                $response .= '<div class="collapse ' . ($show ? "show" : "") . '" id="collapseAxes">';
                $response .= '<div class="card-body">';
                $response .= '<form method="POST" action="index.php">';
                $response .= self::writeAxisLineForm("x", $xAxisName, $idXAxisType, $isSelectXDisabled);
                $response .= '<hr />';
                $response .= self::writeAxisLineForm("y", $yAxisName, $idYAxisType, $isSelectYDisabled);
                $response .= '<div class="form-row justify-content-end">';
                $response .= '<button class="btn btn-success btn-circle btn-lg" name="createAxes" value="createAxes" data-toggle="tooltip" data-placement="top" title="Add axes">';
                $response .= '<i class="fas fa-check"></i>';
                $response .= '</button>';
                $response .= '</div>';
                $response .= '</form>';
                $response .= '</div>';
                $response .= '</div>';
                $response .= '</div>';
            }
        }
        return $response;
    }

    /**
     * Write the form to enter the data
     *
     * @return string result as HTML
     */
    public static function writeDataForm(): string
    {
        $response = "";
        // If there is a chart that is created
        if ($chart = LSession::getInstance()->getChartSession()) {
            if ($chart->getXAxis() !== null) {
                $response .= '<div class="card shadow mb-4">';
                $response .= '<div class="card-header py-3">';
                $response .= '<h6 class="m-0 font-weight-bold text-primary">Data</h6>';
                $response .= '</div>';
                $response .= '<div class="card-body">';
                $response .= self::writeDataContainerNameForm($chart);
                $response .= self::writeDataContainerCoordForm($chart);

                $response .= '</div>';
                $response .= '</div>';
            }
        }
        return $response;
    }

    /**
     * Write options form of chart, for name and chart type 
     *
     * @param LChart|null $chart chart 
     * @return string result as HTML
     */
    public static function writeOptionsForm(?LChart $chart): string
    {
        $response = "";
        if ($chart !== null) {
            $response .= '<div class="card shadow mb-4">';
            $response .= '<div class="card-header py-3">';
            $response .= '<h6 class="m-0 font-weight-bold text-primary">Options</h6>';
            $response .= '</div>';
            $response .= '<div class="card-body">';
            $response .= '<form method="POST" action="index.php">';
            $response .= '<div class="form-row">';
            $response .= '<div class="col-md-7"><label for="floatingChartName">Chart name</label>';
            $response .= '<input ' . self::writeTypeInput("string") . ' class="form-control" id="floatingChartName" name="chartName" placeholder="Name of the chart" value="' . $chart->getName() . '"></div>';
            $response .= '<div class="col-md-4 mb-3">';
            $response .= '<div class="input-group"><label for="floatingChartName">Type of Chart</label>';
            $response .= '<div class="input-group">';
            $response .= '<div class="input-group-prepend"><label class="input-group-text" for="chartType">Type</label></div>';
            $response .= self::writeSelectChartType($chart->getTypeChart());
            $response .= '</div>';
            $response .= '</div>';
            $response .= '</div>';
            $response .= '<button class="btn btn-success btn-circle btn-lg align-self-center" name="modifyChart" value="modifyChart" data-toggle="tooltip" data-placement="top" title="Modify chart">';
            $response .= '<i class="fas fa-check"></i>';
            $response .= '</button>';
            $response .= '</div>';
            $response .= '</form>';
            $response .= '</div>';
            $response .= '</div>';
        }
        return $response;
    }

    /**
     * Write the import form
     *
     * @return string result as html
     */
    public static function writeImportForm(): string
    {
        $response = "";
        $response .= '<form method="POST" action="api/import.php" enctype="multipart/form-data">';
        $response .= '<div class="modal-body">';
        $response .= '<div class="row mb-3">';
        $response .= '<div class="col">';
        $response .= ' Your CSV file has to have this format :';
        $response .= '<ul>';
        $response .= '<li>The first column contains the x value</li>';
        $response .= '<li>The following columns contains value of each data container</li>';
        $response .= '<li>If there is more y value than data containers that you have created, empty data containers will be created</li>';
        $response .= '<li>The first row will determine the number of data containers you have</li>';
        $response .= ' </ul>';
        $response .= ' </div>';
        $response .= '<div class="col">';
        $response .= '<img src="res/assets/csvFormat.PNG">';
        $response .= ' </div>';
        $response .= ' </div>';
        $response .= '<div class="input-group mb-3">';
        $response .= '<div class="input-group-prepend">';
        $response .= '<span class="input-group-text" id="inputGroupFileAddon01">Upload</span>';
        $response .= ' </div>';
        $response .= '<div class="custom-file">';
        $response .= '<input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="' . self::NAME_INPUT_FILE_IMPORT . '" accept=".csv">';
        $response .= '<label class="custom-file-label" for="inputGroupFile01">Choose file</label>';
        $response .= ' </div>';
        $response .= ' </div>';
        $response .= '</div>';
        $response .= '<div class="modal-footer">';
        $response .=  LToolsHTMLModal::writeButtonCloseModal("Cancel");
        $response .= '<button name="import" value="import" class="btn btn-success btn-icon-split">';
        $response .= '<span class="icon text-white-50">';
        $response .= '<i class="fas fa-check"></i>';
        $response .= '</span>';
        $response .= '<span class="text">Submit</span>';
        $response .= '</button>';
        $response .= '</div>';
        $response .= '</form>';
        $response .= '</div>';
        return $response;
    }

    /**
     * Add a class to show if an input is valid or not
     *
     * @param boolean $isValid is input valid ?
     * @return string result as HTML class name 
     */
    public static function writeValidityInput($isValid): string
    {
        $response = "";
        if ($isValid !== null) {
            $response = "is-";
            if (!$isValid) {
                $response .=  "invalid";
            } else {
                $response .= "valid";
            }
        }
        return $response;
    }

    /*--------END PUBLIC--------*/

    /*--------PRIVATE--------*/

    /**
     * Write the radio buttons for the different type of charts
     *
     * @return string Radion Buttons for type of chart
     */
    private static function writeRadioTypeChart(): string
    {
        $result = "<div class=\"btn-group btn-group-toggle row mb-3\" data-toggle=\"buttons\">";
        $chartTypeDB = new LChartTypeDB();
        $chartType = $chartTypeDB->getAllChartType();
        if ($chartType) {
            foreach ($chartType as $type) {
                $result .= "<label class=\"btn btn-secondary col-4 rounded-0\">";
                $result .= "<input required type=\"radio\" name=\"chartType\" id=\"chartType" . $type->getId() . "\" value=\"" . $type->getId() . "\">";
                $result .= LToolsHTML::getIcon($type->getName(), 6);
                $result .= "</label>";
            }
        }
        $result .= "</div>";
        return $result;
    }

    /**
     * Write a component for a form to enter the data
     *
     * @param string $label Label to write
     * @param string $id id of the component same as the name for the form
     * @param int $i position in the list
     * @param integer $size size of the label with input texte
     * @param mixed $value value of the input
     * @param string $type type of input
     * @return string
     */
    private static function writeComponentFormData($label, $id, $i = "", $size = 5, $value = "", $type = "string"): string
    {
        $response = "";
        $num = "";
        if ($i !== "") {
            $num = ($i + 1);
        }
        $response .= '<div class="col-md-' . $size . ' mb-3">';
        $response .= '<label for="' . $id . '">' . $label . ' ' . $num . '</label>';
        $response .= '<input ' . self::writeTypeInput($type) . ' class="form-control" id="' . $id . '" name="' . $id . '" value="' . $value . '">';
        $response .= '</div>';
        return $response;
    }

    /**
     * Writes the form toadd the data container 
     *
     * @param LChart $chart chart
     * @return string result as HTML
     */
    private static function writeDataContainerNameForm(LChart $chart): string
    {
        $response = "";
        $countDataContainer = count($chart->getDataContainers());
        $response .= '<div class=""><h6 class="m-0 font-weight-bold text-primary">Data containers</h6></div>';
        for ($i = 0; $i <= $countDataContainer; $i++) {
            $name = "";
            if ($i != $countDataContainer) {
                $name = $chart->getDataContainers()[$i]->getName();
            }
            $response .= '<form method="POST" action="index.php" class="form-row">';
            $response .= '<input type="hidden" id="idDataContainer" name="idDataContainer" value="' . $i . '">';
            $response .= self::writeComponentFormData("Name Data Container", "nameDataContainer", $i, self::SIZE_LABEL_FORM_DATA_CONTAINER_NAME, $name);
            $response .= self::writeButtonsData($i, $countDataContainer, "DataContainer");
            $response .= "</form>";
        }
        return $response;
    }

    /**
     * Writes the form to add coord in the Line chart
     *
     * @param LChart $chart chart
     * @return string result as HTML
     */
    private static function writeDataContainerCoordForm(LChart $chart): string
    {
        $response = "";
        if (count($chart->getDataContainers()) > 0) {
            /** Number of x coords */
            $countX = count($chart->getDataContainers()[0]->getPoints());
            $countLine = count($chart->getDataContainers());
            $response .= '<div class=""><h6 class="m-0 font-weight-bold text-primary">Data in containers</h6></div>';
            for ($i = 0; $i <= $countX; $i++) {
                $valueX = "";
                if ($i != $countX) {
                    $valueX = $chart->getDataContainers()[0]->getCoordX($i);
                }
                $response .= '<form method="POST" action="index.php" class="form-row border p-2 mb-2 rounded">';
                $response .= '<input type="hidden" id="idCoord" name="idCoord" value="' . $i . '">';
                $response .= self::writeComponentFormData("n°", "coordX", $i, self::SIZE_LABEL_FORM_DATA_CONTAINER_COORD, $valueX, $chart->getXAxis()->getAxisType()->getName());
                for ($j = 0; $j < $countLine; $j++) {
                    $valueY = "";
                    $name = $chart->getDataContainers()[$j]->getName();
                    if ($i != $countX) {
                        $valueY = $chart->getDataContainers()[$j]->getCoordY($i);
                    }
                    $response .= self::writeComponentFormData("$name", "coordY$j", "", self::SIZE_LABEL_FORM_DATA_CONTAINER_COORD, $valueY, $chart->getYAxis()->getAxisType()->getName());
                }

                $response .= self::writeButtonsData($i, $countX, "Point");
                $response .= "</form>";
            }
        }
        return $response;
    }

    /**
     * Write buttons for the form data
     *
     * @param integer $i position in the list
     * @param integer $size size of the list
     * @param string $name name of the component
     * @return string result as HTML
     */
    private static function writeButtonsData(int $i, int $size, string $name): string
    {
        $response = '<div class="col-md-2 mb-3 align-self-end">';
        $response .= '<button class="btn btn-success btn-circle btn-md" name="' . self::writeNameAddModify($i, $size, $name) . '" value="' . self::writeNameAddModify($i, $size, $name) . '" data-toggle="tooltip" data-placement="top" title="Apply changes">';
        if ($i == $size) {
            $response .= '<i class="fas fa-plus"></i>';
        } else {
            $response .= '<i class="fas fa-pencil-alt"></i>';
        }
        $response .= '</button>';
        if ($i != $size) {
            $response .= '<button class="btn btn-danger btn-circle btn-md mx-2" name="delete' . $name . '" value="delete' . $name . '" data-toggle="tooltip" data-placement="top" title="Delete">';
            $response .= '<i class="fas fa-trash"></i>';
            $response .= '</button>';
        }
        $response .= '</div>';
        return $response;
    }

    /**
     * Sets the name of a submit button with modify if not the last one, or with add if the last one
     *
     * @param int $i current line
     * @param int $size size of the array
     * @param string $type name of the type of the gaph
     * @return string 
     */
    private static function writeNameAddModify($i, $size, $type): string
    {
        return ($i == $size ? "add" : "modify") . $type;
    }

    /**
     * Write a line of the form to add axius
     *
     * @param string $letterAxis letter of the axis (X or Y)
     * @param string $value name of the axis
     * @param int $idType type of the axis
     * @param boolean $isSelectDisabled if true, disabled the select
     * @return string result as HTML
     */
    private static function writeAxisLineForm($letterAxis, $value, $idType, $isSelectDisabled): string
    {
        $idInput = 'floatingInput' . strtoupper($letterAxis) . 'Axis';
        $nameInput = strtolower($letterAxis) . 'AxisName';
        $response = "";
        $response .= '<div class="form-row">';
        $response .= '<div class="col-md-6">';
        $response .= '<label for="floatingInputXAxis">Name of ' . strtoupper($letterAxis) . ' Axis</label>';
        $response .= '<input type="text" class="form-control" id="' . $idInput . '" name="' . $nameInput . '" value="' . $value . '">';
        $response .= '</div>';
        $response .= '<div class="col-md-6 mb-3">';
        $response .= '<div class="input-group">';
        $response .= '<label for="' . $idInput . '">Type of ' . strtoupper($letterAxis) . ' Axis</label>';
        $response .= self::writeSelectAxisType($idType, $isSelectDisabled);
        $response .= '</div>';
        $response .= '</div>';
        $response .= '</div>';
        return $response;
    }

    /**
     * Write selected if it is the same id
     *
     * @param integer $currentId
     * @param integer $selectedId
     * @return string selected or ""
     */
    private static function writeSelected($currentId, $selectedId): string
    {
        return ($currentId == $selectedId ? "selected" : "");
    }

    /**
     * Write the select for chart type
     *
     * @param LChartType $chartTypeSelected chart type sleected
     * @return string select as HTML
     */
    private static function writeSelectChartType(LChartType $chartTypeSelected): string
    {
        $response = '<select class="custom-select" name="chartType" id="chartType">';
        $lChartTypeDB = new LChartTypeDB();
        foreach ($lChartTypeDB->getAllChartType() as $chartType) {
            $response .= "<option value='" . $chartType->getId() . "' " . self::writeSelected($chartType->getId(), $chartTypeSelected->getId()) . " >";
            $response .= $chartType->getName();
            $response .= "</option>";
        }
        $response .= "</select>";
        return $response;
    }

    /**
     * Write disabled for an input if true
     *
     * @param boolean $isDisabled if true, we disable the input
     * @return string
     */
    private static function writeDisabled(bool $isDisabled): string
    {
        return ($isDisabled ? "disabled" : "");
    }

    /**
     * Write the type of input and other attributes
     *
     * @param string $type type of input
     * @return string attributes to write on input
     */
    private static function writeTypeInput(string $type): string
    {
        $result = "type=";
        switch ($type) {
            case 'number':
                $result .= '"number" step="0.01"';
                break;
            case 'date':
                $result .= '"date"';
                break;
            case 'string':
            default:
                $result .= '"text"';
                break;
        }
        return $result;
    }

    /*--------END PRIVATE--------*/
}
