<?php

/** All Inc
 *  -------
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief File taht include all the files needed
 *  @author ludovic.rx@eduge.ch
 */

require_once(__DIR__ . DIRECTORY_SEPARATOR . "path.inc.php");

require_once(DB_PATH . "EDatabase.php");
require_once(DB_PATH . "LAxisDB.php");
require_once(DB_PATH . "LAxisTypeDB.php");
require_once(DB_PATH . "LChartDB.php");
require_once(DB_PATH . "LChartTypeDB.php");
require_once(DB_PATH . "LDataContainerDB.php");
require_once(DB_PATH . "LDataDB.php");
require_once(DB_PATH . "LUserDB.php");

require_once(CONTAINER_PATH . "LType.php");
require_once(CONTAINER_PATH . "LAxis.php");
require_once(CONTAINER_PATH . "LAxisLetter.php");
require_once(CONTAINER_PATH . "LChartType.php");
require_once(CONTAINER_PATH . "LAxisType.php");
require_once(CONTAINER_PATH . "LChart.php");
require_once(CONTAINER_PATH . "LDataContainer.php");
require_once(CONTAINER_PATH . "LPoint.php");
require_once(CONTAINER_PATH . "LUser.php");

require_once(__DIR__ . DIRECTORY_SEPARATOR . "LSession.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . "LTools.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . "LToolsFilter.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . "LToolsHTML.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . "LToolsHTMLForm.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . "LToolsHTMLModal.php");

/** Get instance is called to start the session before we write any content on the pages */
LSession::getInstance();