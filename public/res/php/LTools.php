<?php

/** LTools
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class for all the general functions of the project
 *  @author ludovic.rx@eduge.ch
 */

/**
 * Class that has only static functions
 * These are the functions for the project
 */
class LTools
{
    /*--------CONSTANTS--------*/

    /** Separator of csv file */
    private const CSV_SEPARATOR = ",";

    /*--------END CONSTANTS--------*/

    /*--------PUBLIC--------*/

    /**
     * Disconnect the user
     *
     * @return void
     */
    public static function disconnectUser()
    {
        LSession::getInstance()->setUserSession(null);
        LTools::redirect(".." . DIRECTORY_SEPARATOR . "login.php");
    }

    /**
     * Connects the user and redirect him to index
     *
     * @param LUser $user user that is connected
     * @return void
     */
    public static function connectUser(LUser $user)
    {
        LSession::getInstance()->setUserSession($user);
        LTools::redirect("graphs.php");
    }

    /**
     * Save a chart in the database
     * Transaction are used beacause if there is an error, we have to get previous informations
     *
     * @return boolean true if it has worked, else false
     */
    public static function saveChart(): bool
    {
        $result = false;
        $chart = LSession::getInstance()->getChartSession();
        $user = LSession::getInstance()->getUserSession();
        EDatabase::getInstance()->beginTransaction();
        if ($chart !== null) {
            // If axes are created
            if ($chart->getXAxis() !== null && $chart->getYAxis() !== null && !$chart->getIsSaved()) {
                $chartDB = new LChartDB();
                if ($chart->getId() !== null) {
                    self::deleteChart($chart->getId(), $user->getId());
                }
                // If there was no error before to insert
                if ($chartDB->insertChart($chart->getName(), $chart->getTypeChart()->getId(), $user->getId())) {
                    // If to get id of the chart
                    if ($idChart = EDatabase::getInstance()->lastInsertId()) {
                        $lAxisDB = new LAxisDB();
                        $insertXAxisResult = $lAxisDB->insertXAxis($chart->getXAxis()->getName(), $chart->getXAxis()->getAxisType()->getId(), $idChart);
                        $insertYAxisResult = $lAxisDB->insertYAxis($chart->getYAxis()->getName(), $chart->getYAxis()->getAxisType()->getId(), $idChart);
                        // If nor error inserting axes
                        if ($insertXAxisResult && $insertYAxisResult) {
                            $dataContainerDB = (new LDataContainerDB);
                            if ($dataContainerDB->insertArrayDataContainer($chart->getDataContainers(), $idChart)) {
                                // If no error to get chart
                                if ($chart = $chartDB->getChartById($idChart, $user->getId())) {
                                    $result = true;
                                    $chart->setIsSaved(true);
                                    LSession::getInstance()->setChartSession($chart);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($result) {
            EDatabase::getInstance()->commit();
        } else {
            EDatabase::getInstance()->rollBack();
        }
        return $result;
    }

    /**
     * Create the axes in the database
     *
     * @param LAxis $xAxis x Axis
     * @param LAxis $yAxis y Axis
     * @return boolean True is succeed else false
     */
    public static function createAxes(LAxis $xAxis, LAxis $yAxis): bool
    {
        $returnResult = false;
        if ($chart = LSession::getInstance()->getChartSession()) {
            $lAxisDB = new LAxisTypeDB();
            $xAxisType = $lAxisDB->getAxisTypeById($xAxis->getAxisType()->getId());
            $yAxisType = $lAxisDB->getAxisTypeById($yAxis->getAxisType()->getId());
            // Verify that as well got the axes types
            if ($xAxisType && $yAxisType) {
                $xAxis->setAxisType($xAxisType);
                $yAxis->setAxisType($yAxisType);
                $chart->setXAxis($xAxis);
                $chart->setYAxis($yAxis);
                $chart->setIsSaved(false);
                LSession::getInstance()->setChartSession($chart);
                $returnResult = true;
            }
        }
        return $returnResult;
    }

    /**
     * Delete a chart
     *
     * @param integer $idChart id of the chart to delete
     * @param integer $idUser
     * @return boolean true if succeed, else false
     */
    public static function deleteChart(int $idChart, int $idUser): bool
    {
        $resultDeleteData = false;
        $resultDeleteChart = false;
        $lChartDB = new LChartDB();
        if ($dataContainers = $lChartDB->getChartById($idChart, $idUser)) {
            $resultDeleteData = (new LDataDB)->deleteArrayXData($dataContainers->getDataContainers()[0]);
            $resultDeleteChart =  $lChartDB->deleteChart($idChart, $idUser);
        }
        return ($resultDeleteChart && $resultDeleteData);
    }

    /**
     * Redirect a user
     *
     * @param string $url the url where we are redirect
     * @return void
     */
    public static function redirect($url): void
    {
        header("Location: $url");
        exit();
    }

    /**
     * Verifies the type of the data that the uer enter
     *
     * @param string $type type that the var has to have
     * @param mixed $var var to verify
     * @return boolean true if right type, else false
     */
    public static function verifyType(string $type, $var): bool
    {
        $returnResult = false;
        switch ($type) {
            case "number":
                $returnResult = is_numeric($var);
                break;
            case "date":
                if (DateTime::createFromFormat("Y-m-d", $var)) {
                    $returnResult = true;
                }
                break;
            case "string":
                $returnResult = true;
                break;
        }
        return $returnResult;
    }

    /**
     * Verify type of an array of data
     *
     * @param string $type type that values must have
     * @param array $vars array of values
     * @return boolean true if valid, else false
     */
    public static function verifyArrayType(string $type, array $vars): bool
    {
        $returnResult = true;
        foreach ($vars as $value) {
            if (!self::verifyType($type, $value)) {
                $returnResult = false;
            }
        }
        return $returnResult;
    }

    /**
     * Encode a chart that is stored in the session
     *
     * @return string encoded json chart
     */
    public static function encodeChart(): string
    {
        return json_encode(LSession::getInstance()->getChartSession(), JSON_PRETTY_PRINT);
    }

    /**
     * Upload file on server
     *
     * @return string name of the file if succeed, else "" 
     */
    public static function upload(): string
    {
        $response = "";

        // If we have something the $_FILES
        $data = $_FILES[LToolsHTMLForm::NAME_INPUT_FILE_IMPORT];
        // Error 4 means that no fil was downloaded
        if ($data["error"] != 4) {
            // If the file is a csv
            $name = $data["name"];
            $tempName = $data["tmp_name"];
            if (self::isCsv($name)) {
                // If upload works
                $uniqueName = self::createUniqueName("import_", $name);
                $filename = IMPORT_PATH . $uniqueName;
                if (move_uploaded_file($tempName,  $filename)) {
                    $response = $filename;
                }
            }
        }

        return $response;
    }

    /**
     * Import data to a chart
     *
     * @param LChart $chart chart
     * @param string $filename path to the file
     * @return boolean true if succeed, else false
     */
    public static function import(LChart $chart, string $filename): bool
    {
        $response = true;
        // Verify that chart exists
        if (file_exists($filename)) {
            // Verify that we got axis
            if ($chart->getXAxis() !== null && $chart->getYAxis() !== null) {
                $handle = fopen($filename, "r");
                $numColumn = 0;
                $nbDataContainers = count($chart->getDataContainers());
                $newChart = new LChart($chart->getId(), $chart->getName(), $chart->getXAxis(), $chart->getYAxis(), $chart->getTypeChart(), $chart->getDataContainers(), false);
                while (!feof($handle)) {
                    if ($line = fgetcsv($handle, 0, ",", '"')) {
                        $xValue = $line[0];

                        // Add data containers if mora data that data containers
                        if ($numColumn === 0) {
                            $numColumn = count($line);
                            if ($numColumn - 1 > $nbDataContainers) {
                                // Need to add data containers if there is more data
                                for ($i = 0; $i < $numColumn - $nbDataContainers; $i++) {
                                    $newChart->addDataContainer(new LDataContainer(null, "", $chart->getXData()));
                                }
                            }
                        }

                        array_shift($line);
                        if (!$newChart->addDataToDataContainer($xValue, $line)) {
                            $response = false;
                            break;
                        }
                    }
                }
                fclose($handle);
                if ($response) {
                    LSession::getInstance()->setChartSession($newChart);
                }
            }
        }
        return $response;
    }

    /**
     * Export to csv a file, show a dialog to save
     * @copyright https://www.php.net/manual/fr/function.readfile.php
     * 
     * @param LChart $chart
     * @return boolean true if succeed, else false
     */
    public static function exportCSV(LChart $chart): bool
    {
        $response = false;
        if ($csvData = self::writeDataToCSV($chart)) {
            $file = 'data.csv';
            $fullPathName = ".." . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "export" . DIRECTORY_SEPARATOR . self::createUniqueName("export", $file);
            if (file_put_contents($fullPathName, $csvData)) {
                if (file_exists($fullPathName)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . $file . '"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($fullPathName));
                    readfile($fullPathName);
                    $response = true;
                }
            }
        }
        return $response;
    }

    /**
     * Write the data to a csv format in an array
     *
     * @param LChart $chart
     * @return void
     */
    private static function writeDataToCSV(LChart $chart)
    {
        $response = false;
        $dataContainers = $chart->getDataContainers();
        if (count($dataContainers) > 0) {
            $response = array();
            for ($i = 0; $i < count($dataContainers[0]->getPoints()); $i++) {
                // First add the x data
                $line = $dataContainers[0]->getCoordX($i);
                for ($j = 0; $j < count($dataContainers); $j++) {
                    $line .= self::CSV_SEPARATOR . $dataContainers[$j]->getCoordY($i);
                }
                array_push($response, $line . PHP_EOL);
            }
        }
        return $response;
    }

    /*--------END PUBLIC--------*/

    /*--------PRIVATE--------*/

    /**
     * Determin if the file is a csv or not
     *
     * @param string $path path to the file
     * @return boolean true if csv, else false
     */
    private static function isCsv(string $path): bool
    {
        return pathinfo($path, PATHINFO_EXTENSION) == "csv";
    }

    /**
     * Create a unique name for a file
     *
     * @param string $prefix prefix for the file
     * @param string $name name of the file to get the extension
     * @return string the unique name
     */
    private static function createUniqueName(string $prefix, string $name): string
    {
        return uniqid($prefix, true) . "." . pathinfo($name, PATHINFO_EXTENSION);
    }


    /*--------END PRIVATE--------*/
}
