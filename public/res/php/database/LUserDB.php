<?php

/** LUserDB
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LUserDB
 *  @author ludovic.rx@eduge.ch
 */

/**
 * @brief Class that makes query on DB for users
 */
class LUserDB
{
    /**>Host of database */
    private $dbh = null;

    /**>Prepare statement for get user by id */
    private $psUserById = null;
    /**>Sql for det user by id */
    private $sqlUserById = "";

    /**>Prepare statement for user by email */
    private $psUserByEmail = null;
    /**>Sql for user by email */
    private $sqlUserByEmail = "";

    /**>Prepare statement for insert user */
    private $psInsertUser = null;
    /**>Sql for insert user */
    private $sqlInsertUser = "";

    /**>Preapre statement for update password */
    private $psUpdatePassword = null;
    /**>Sql for update password */
    private $sqlUpdatePassword = "";

    /**>Prepare for verify password by id */
    private $psVerifyPasswordById = null;
    /**>Sql for verify password by id*/
    private $sqlVerifyPasswordById =  "";

    /**>Prepare for verify password by email */
    private $psVerifyPasswordByEmail = null;
    /**>Sql for verify password  by email*/
    private $sqlVerifyPasswordByEmail =  "";

    /**>Prepare for update user */
    private $psUpdateUser = null;
    /**>Sql for update user */
    private $sqlUpdateUser = "";

    /**
     * Create an instance of LUserDB that can make queries on the database
     */
    public function __construct()
    {
        // Sets all the sql queries
        $this->sqlUserById = "SELECT idUser, firstName, lastName, email, password FROM users WHERE idUser = :ID_USER;";
        $this->sqlUserByEmail = "SELECT idUser, firstName, lastName, email, password FROM users WHERE email LIKE :EMAIL;";
        $this->sqlInsertUser = "INSERT INTO users (firstName, lastName, email, password) VALUES(:FIRST_NAME, :LAST_NAME, :EMAIL, :PASSWORD)";
        $this->sqlUpdatePassword = "UPDATE users SET password = :PASSWORD WHERE iduser LIKE :ID_USER";
        $this->sqlVerifyPasswordById =  "SELECT password FROM users WHERE idUser = :ID_USER";
        $this->sqlVerifyPasswordByEmail =  "SELECT password FROM users WHERE email = :EMAIL";
        $this->sqlUpdateUser = "UPDATE users SET firstName = :FIRST_NAME, lastName = :LAST_NAME, email = :EMAIL, WHERE iduser LIKE :ID_USER";

        // Prepare all the queries
        try {
            $this->dbh = EDatabase::getInstance();
            $this->psUserById =  $this->dbh->prepare($this->sqlUserById);
            $this->psUserByEmail =  $this->dbh->prepare($this->sqlUserByEmail);
            $this->psInsertUser =  $this->dbh->prepare($this->sqlInsertUser);
            $this->psUpdatePassword =  $this->dbh->prepare($this->sqlUpdatePassword);
            $this->psVerifyPasswordById =  $this->dbh->prepare($this->sqlVerifyPasswordById);
            $this->psVerifyPasswordByEmail =  $this->dbh->prepare($this->sqlVerifyPasswordByEmail);
            $this->psUpdateUser =  $this->dbh->prepare($this->sqlUpdateUser);
        } catch (PDOException $e) {
            echo $e->getMessage();
            error_log($e->getMessage());
        }
    }

    /**
     * Get a specific user thanks his id
     *
     * @param int $idUser id of the user
     * @return LUser if succeed, else false if failure
     */
    public function getUserById(int $idUser)
    {
        $returnResult = false;
        try {
            $this->psUserById->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $this->psUserById->execute();

            if ($result = $this->psUserById->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = new LUser(intval($result["idUser"]), $result["firstName"], $result["lastName"], $result["email"]);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Get a specific user thanks to the email
     *
     * @param string $email email of the user
     * @return LUSer|false LUser if succeed, else false
     */
    public function getUserByEmail(string $email)
    {
        $returnResult = false;
        try {
            $this->psUserByEmail->bindParam(":EMAIL", $email, PDO::PARAM_STR);
            $this->psUserByEmail->execute();

            if ($result =  $this->psUserByEmail->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = new LUser(intval($result["idUser"]), $result["firstName"], $result["lastName"], $result["email"]);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Verify if an email exists
     *
     * @param string $email email that we verify
     * @return bool true if the email exists, else false
     */
    public function emailExists(string $email)
    {
        return $this->getUserByEmail($email);
    }

    /**
     * Insert a user in the database
     *
     * @param string $firstName first name of the user
     * @param string $lastName last name of the user
     * @param string $email email of the user
     *  @param string $password password of the user
     * @return bool true if succeed, else false
     */
    public function insertUser(string $firstName, string $lastName, string $email, string $password)
    {
        $returnResult = false;
        try {
            $hashPassword = self::hashPassword($password);
            $this->psInsertUser->bindParam(":FIRST_NAME", $firstName, PDO::PARAM_STR);
            $this->psInsertUser->bindParam(":LAST_NAME", $lastName, PDO::PARAM_STR);
            $this->psInsertUser->bindParam(":EMAIL", $email, PDO::PARAM_STR);
            $this->psInsertUser->bindParam(":PASSWORD", $hashPassword, PDO::PARAM_STR);
            $returnResult = $this->psInsertUser->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Update the password from a user
     *
     * @param int $idUser id of the user
     * @param string $newPassword new password for the user
     * @return bool true if succeed, else false
     */
    public function updatePassword(int $idUser, string $newPassword)
    {
        $returnResult = false;
        try {
            $this->psUpdatePassword->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $this->psUpdatePassword->bindParam(":PASSWORD", self::hashPassword($newPassword), PDO::PARAM_STR);
            $returnResult = $this->psUpdatePassword->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Verify the password with the id
     *
     * @param int $idUser id of the user
     * @param string $password password that the user has entered
     * @return bool true if the same password, else false
     */
    public function verifyPasswordById(int $idUser, string $password)
    {
        $returnResult = false;
        try {
            $this->psVerifyPasswordById->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $this->psVerifyPasswordById->execute();
            $result = $this->psVerifyPasswordById->fetchAll(PDO::FETCH_ASSOC);
            $returnResult = self::verifyPassword($password, $result);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Verify the password with the email
     *
     * @param string $email email of the user
     * @param string $password password that the user has entered
     * @return bool true if the same password, else false
     */
    public function verifyPasswordByEmail(string $email, string $password)
    {
        $returnResult = false;
        try {
            $this->psVerifyPasswordByEmail->bindParam(":EMAIL", $email, PDO::PARAM_STR);
            $this->psVerifyPasswordByEmail->execute();
            $result = $this->psVerifyPasswordByEmail->fetchAll(PDO::FETCH_ASSOC);
            $returnResult = self::verifyPassword($password, $result);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Update the user
     *
     * @param int $idUser id of the user
     * @param string $firstName first name of the user
     * @param string $lastName last name of the user
     * @param string $newEmail new email for the user
     * @param string $newUsername new username for the user
     * @return bool true if succeed, else false
     */
    public function updateUser(int $idUser, string $firstName, string $lastName, string $newEmail, string $newUsername)
    {
        $returnResult = false;
        try {
            $this->psUpdateUser->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $this->psUpdateUser->bindParam(":FIRST_NAME", $firstName, PDO::PARAM_STR);
            $this->psUpdateUser->bindParam(":LAST_NAME", $lastName, PDO::PARAM_STR);
            $this->psUpdateUser->bindParam(":EMAIL", $newEmail, PDO::PARAM_STR);
            $this->psUpdateUser->bindParam(":USERNAME", $newUsername, PDO::PARAM_STR);
            $returnResult = $this->psUpdateUser->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Hash the password
     *
     * @param string $password password to hash
     * @return string password that is hashed
     */
    private static function hashPassword(string $password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }


    /**
     * Verify a password
     *
     * @param string $password password entered
     * @param array $result user selected
     * @return bool true if right password, else false
     */
    private static function verifyPassword($password, $result)
    {
        $returnResult = false;
        // If there is only one result
        if (count($result) === 1) {
            $returnResult = password_verify($password, $result[0]["password"]);
        }
        return $returnResult;
    }
}
