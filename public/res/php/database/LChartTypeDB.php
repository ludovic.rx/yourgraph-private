<?php

/** LChartTypeDB
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LChartTypeDB
 *  @author ludovic.rx@eduge.ch
 */

/**
 * @brief Class that makes query on DB for types of charts
 */
class LChartTypeDB
{
    /**>Host of database */
    private $dbh = null;

    /**>Prepare get chart type by id */
    private $psGetChartTypeById = null;
    /**>Sql for get chart type by id*/
    private $sqlGetChartTypeById = "";

    /**>Prepare get all chart type */
    private $psGetAllChartType = null;
    /**>Sql for get all chart type*/
    private $sqlGetAllChartType = "";

    /**
     * Create an instance of LChartType that can make queries on the database
     */
    public function __construct()
    {

        // Sets all the sql queries
        $this->sqlGetChartTypeById = "SELECT idChartType, chartTypeName FROM chartsTypes WHERE idChartType = :ID_CHART_TYPE";
        $this->sqlGetAllChartType = "SELECT idChartType, chartTypeName FROM chartsTypes ORDER BY chartTypeName ASC";

        // Prepare all the queries
        try {
            $this->dbh = EDatabase::getInstance();
            $this->psGetChartTypeById = $this->dbh->prepare($this->sqlGetChartTypeById);
            $this->psGetAllChartType = $this->dbh->prepare($this->sqlGetAllChartType);
        } catch (PDOException $e) {
            echo $e->getMessage();
            error_log($e->getMessage());
        }
    }

    /**
     * Get a specifix chart type by id
     *
     * @param integer $id id of the chart type
     * @return LChartType|false LChartType if succeed, else false
     */
    public function getChartTypeById(int $id)
    {
        $returnResult = false;
        try {
            $this->psGetChartTypeById->bindParam(":ID_CHART_TYPE", $id, PDO::PARAM_INT);
            $this->psGetChartTypeById->execute();
            if($result = $this->psGetChartTypeById->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = new LChartType($result["idChartType"], $result["chartTypeName"]);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Get all the charts types
     *
     * @return array|false array of LChartType if succeed else false
     */
    public function getAllChartType()
    {
        $returnResult = false;
        try {
            $this->psGetAllChartType->execute();

            if ($result = $this->psGetAllChartType->fetchAll(PDO::FETCH_ASSOC)) {
                $returnResult = array();
                foreach ($result as $key => $type) {
                    array_push($returnResult, new LChartType($type["idChartType"], $type["chartTypeName"]));
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }


}
