<?php

/** LChartDB
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LChartDB
 *  @author ludovic.rx@eduge.ch
 */

/**
 * @brief Class that makes query on DB for chart
 */
class LChartDB
{
    /** Host of database */
    private $dbh = null;

    /** Prepare to get chart by id */
    private $psGetChartById = null;
    /** Sql to get chart by id */
    private $sqlGetChartById = "";

    /** Ps to get charts from user */
    private $psGetChartFromUser = null;
    /** Sql to get charts from user */
    private $sqlGetChartFromUser = "";

    /** Prepare to get chart by id */
    private $psGetIdLastChart = null;
    /** Sql to get chart by id */
    private $sqlGetIdLastChart = "";

    /** Prepare to insert a chart */
    private $psInsertChart = null;
    /** Sql to insert insert a chart */
    private $sqlInsertChart = "";

    /** Prepare to delete chart */
    private $psDeleteChart = null;
    /** Sql to delete chart */
    private $sqlDeleteChart = "";

    /**
     * Create an instance of LChartDB that can make queries on the database
     */
    public function __construct()
    {
        // Sets all the sql queries
        $this->sqlGetChartById = "SELECT idChart, chartName, idChartType FROM charts WHERE idChart = :ID_CHART AND idUser = :ID_USER";
        $this->sqlGetChartFromUser = "SELECT idChart FROM charts WHERE idUser = :ID_USER";
        $this->sqlGetIdLastChart = "SELECT idChart FROM charts ORDER BY idChart LIMIT 1";
        $this->sqlInsertChart = "INSERT INTO charts(chartName, idChartType, idUser) VALUES (:CHART_NAME, :ID_CHART_TYPE, :ID_USER)";
        $this->sqlDeleteChart = "DELETE FROM charts WHERE idChart = :ID_CHART AND idUser = :ID_USER;";

        // Prepare all the queries
        try {
            $this->dbh = EDatabase::getInstance();
            $this->psGetChartById = $this->dbh->prepare($this->sqlGetChartById);
            $this->psGetChartFromUser = $this->dbh->prepare($this->sqlGetChartFromUser);
            $this->psGetIdLastChart = $this->dbh->prepare($this->sqlGetIdLastChart);
            $this->psInsertChart = $this->dbh->prepare($this->sqlInsertChart);
            $this->psDeleteChart = $this->dbh->prepare($this->sqlDeleteChart);
        } catch (PDOException $e) {
            echo $e->getMessage();
            error_log($e->getMessage());
        }
    }

    /**
     * Get a chart thanks to the id
     *
     * @param integer $idChart id of the chart
     * @param integer $idUser id of the user
     * @return LChart|false LChart if succeed, else false
     */
    public function getChartById(int $idChart, int $idUser)
    {
        $returnResult = false;
        try {
            $this->psGetChartById->bindParam(":ID_CHART", $idChart, PDO::PARAM_INT);
            $this->psGetChartById->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $this->psGetChartById->execute();

            if ($result = $this->psGetChartById->fetch(PDO::FETCH_ASSOC)) {
                $lChartTypeDB = new LChartTypeDB();
                if ($chartType = $lChartTypeDB->getChartTypeById(intval($result["idChartType"]))) {
                    $lAxisDB = new LAxisDB();
                    $xAxis = $lAxisDB->getXAxisFromChart(intval($result["idChart"]));
                    $yAxis = $lAxisDB->getYAxisFromChart(intval($result["idChart"]));
                    $dataContainers = (new LDataContainerDB)->getDataContainersByChart(intval($result["idChart"]));
                    // Verification that we didn't have problem before
                    if ($xAxis && $yAxis && $dataContainers) {
                        $returnResult = new LChart(intval($result["idChart"]), $result["chartName"], $xAxis, $yAxis, $chartType, $dataContainers, true);
                    }
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Get the id of the last chart entered
     *
     * @return int|false int if succeed, else false
     */
    public function getIdLastChart()
    {
        $returnResult = false;
        try {
            $this->psGetIdLastChart->execute();
            if ($result = $this->psGetIdLastChart->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = intval($result["idChart"]);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Get charts from a user
     *
     * @param integer $idUser id of the user
     * @return array|false array of LChart if succeed, else false
     */
    public function getChartsFromUser(int $idUser)
    {
        $returnResult = false;
        try {
            $this->psGetChartFromUser->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $this->psGetChartFromUser->execute();
            while ($result = $this->psGetChartFromUser->fetch(PDO::FETCH_ASSOC)) {
                if (!$returnResult) {
                    $returnResult = array();
                }
                // Adds the chart if not false
                if ($chart = $this->getChartById(intval($result["idChart"]), $idUser)) {
                    array_push($returnResult, $chart);
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Insert a chart in the database
     *
     * @param string $chartName name of the chart
     * @param integer $idChartType id of the chart type
     * @param integer $idUser id of the user
     * @return bool true if succeed, else false
     */
    public function insertChart(string $chartName, int $idChartType, int $idUser)
    {
        $returnResult = false;
        try {
            $this->psInsertChart->bindParam(":CHART_NAME", $chartName, PDO::PARAM_STR);
            $this->psInsertChart->bindParam(":ID_CHART_TYPE", $idChartType, PDO::PARAM_INT);
            $this->psInsertChart->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $returnResult = $this->psInsertChart->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }


    /**
     * Delete a specific chart
     * Id of the user is specified to prevent another user to delete a chart that doesn't belong to him
     *
     * @param integer $idChart id of the chart to delete
     * @param integer $idUser id of the user
     * @return boolean true if succeed, else falses
     */
    public function deleteChart(int $idChart, int $idUser): bool
    {
        $returnResult = false;
        try {
            $this->psDeleteChart->bindParam(":ID_CHART", $idChart, PDO::PARAM_INT);
            $this->psDeleteChart->bindParam(":ID_USER", $idUser, PDO::PARAM_INT);
            $returnResult = $this->psDeleteChart->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }
}
