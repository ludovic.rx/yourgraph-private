<?php

/** LDataContainerDB
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LDataContainerDB
 *  @author ludovic.rx@eduge.ch
 */

/**
 * @brief Class that  makes query on data container
 */
class LDataContainerDB
{
    /** Host of database */
    private $dbh = null;

    /** Preapre to get data container for a chart */
    private $psGetDataContainersByChart = null;
    /** Sql to get the data for a chart */
    private $sqlGetDataContainersByChart = "";

    /** Prepare to insert data  */
    private $psInsertDataContainer = null;
    /** Sql to insert data */
    private $sqlInsertDataContainer = "";

    /**
     * Create an instance of LDataContainerDB taht makes query on data container
     */
    public function __construct()
    {
        // Sets all the sql queries
        $this->sqlGetDataContainersByChart = "SELECT idDataContainer, dataContainerName FROM dataContainers WHERE idChart = :ID_CHART";
        $this->sqlInsertDataContainer = "INSERT INTO dataContainers (dataContainerName, idChart) VALUES(:DATA_CONTAINER_NAME, :ID_CHART);";

        // Prepare all the queries
        try {
            $this->dbh = EDatabase::getInstance();
            $this->psGetDataContainersByChart = $this->dbh->prepare($this->sqlGetDataContainersByChart);
            $this->psInsertDataContainer = $this->dbh->prepare($this->sqlInsertDataContainer);
        } catch (PDOException $e) {
            echo $e->getMessage();
            error_log($e->getMessage());
        }
    }

    /**
     * Get data containers for a chart
     *
     * @param integer $idChart id of the chart
     * @return array|false array of LDataContainer if succeed, else false
     */
    public function getDataContainersByChart(int $idChart)
    {
        $returnResult = false;
        try {
            $this->psGetDataContainersByChart->bindParam(":ID_CHART", $idChart, PDO::PARAM_INT);
            $this->psGetDataContainersByChart->execute();

            $dataDB = new LDataDB();
            do {
                $result = $this->psGetDataContainersByChart->fetch(PDO::FETCH_ASSOC);

                // while ($result = $this->psGetDataContainersByChart->fetch(PDO::FETCH_ASSOC)) {
                if ($returnResult === false) {
                    $returnResult = array();
                }
                if ($result) {

                    $idDataContainer = intval($result["idDataContainer"]);
                    $data = $dataDB->getDataByDataContainer($idDataContainer);
                    if ($data === false) {
                        $returnResult = false;
                        break;
                    } else {
                        array_push($returnResult, new LDataContainer($idDataContainer, $result["dataContainerName"], $data));
                    }
                }
            } while ($result);
            // }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Insert a data container
     *
     * @param string $dataContainerName name of the data container
     * @param integer $idChart id of the data container
     * @return boolean true if succeed else false
     */
    public function insertDataContainer(string $dataContainerName, int $idChart): bool
    {
        $returnResult = false;
        try {
            $this->psInsertDataContainer->bindParam(":DATA_CONTAINER_NAME", $dataContainerName, PDO::PARAM_STR);
            $this->psInsertDataContainer->bindParam(":ID_CHART", $idChart, PDO::PARAM_INT);
            $returnResult = $this->psInsertDataContainer->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Insert an array of data container
     *
     * @param array $dataContainers array of LDataContainer
     * @param integer $idChart id of the chart 
     * @return bool true if succeed, else false
     */
    public function insertArrayDataContainer(array $dataContainers, int $idChart): bool
    {
        $returnResult = true;
        foreach ($dataContainers as $d) {
            if (!$this->insertDataContainer($d->getName(), $idChart)) {
                $returnResult = false;
                break;
            }
        }
        if ($returnResult) {
            // if no error
            if ($dataContainersWithId = $this->getDataContainersByChart($idChart)) {
                // insert the data in the containers that we got in the database                
                for ($i = 0; $i < count($dataContainersWithId); $i++) {
                    $dataContainers[$i]->setId($dataContainersWithId[$i]->getId());
                }
                $returnResult = (new LDataDB)->insertArrayData($dataContainers);
            } else {
                $returnResult = false;
            }
        }
        return $returnResult;
    }
}
