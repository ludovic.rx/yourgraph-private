<?php

/** LDataDB
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LDataDB
 *  @author ludovic.rx@eduge.ch
 */

/**
 * @brief Class that makes query on DB on Data to get data from the data containers
 */
class LDataDB
{
    /** Host of database */
    private $dbh = null;

    /** Preapre to get data for a chart */
    private $psGetDataByDataContainer = null;
    /** Sql to get the data for a chart */
    private $sqlGetDataByDataContainer = "";

    /** Preapre to insert x data */
    private $psInsertXData = null;
    /** Sql to insert x data */
    private $sqlInsertXData = "";

    /** Preapre to insert y data */
    private $psInsertYData = null;
    /** Sql to insert y data */
    private $sqlInsertYData = "";

    /** Preapre to deleete x data */
    private $psDeleteXData = null;
    /** Sql to delete x data */
    private $sqlDeleteXData = "";

    /**
     * Create an instance of LDataDB that can make queries on the database
     */
    public function __construct()
    {
        // Sets all the sql queries
        $this->sqlGetDataByDataContainer = "SELECT idYData, xData.value as xDataValue, yData.value as yDataValue FROM xData JOIN yData USING(idXData) WHERE idDataContainer = :ID_DATA_CONTAINER";
        $this->sqlInsertXData = "INSERT INTO xData (value) VALUES(:VALUE)";
        $this->sqlInsertYData = "INSERT INTO yData (value, idDataContainer, idXData) VALUES(:VALUE, :ID_DATA_CONTAINER, :ID_X_DATA)";
        $this->sqlDeleteXData = "DELETE FROM xData WHERE idXData = :ID_X_DATA";

        // Prepare all the queries
        try {
            $this->dbh = EDatabase::getInstance();
            $this->psGetDataByDataContainer = $this->dbh->prepare($this->sqlGetDataByDataContainer);
            $this->psInsertXData = $this->dbh->prepare($this->sqlInsertXData);
            $this->psInsertYData = $this->dbh->prepare($this->sqlInsertYData);
            $this->psDeleteXData = $this->dbh->prepare($this->sqlDeleteXData);
        } catch (PDOException $e) {
            echo $e->getMessage();
            error_log($e->getMessage());
        }
    }


    /**
     * Get data from a data container
     *
     * @param integer $idDataContainer id of the dataContainer
     * @return array|false array of LPoint if succeed, else false
     */
    public function getDataByDataContainer(int $idDataContainer)
    {
        $returnResult = false;
        try {
            $this->psGetDataByDataContainer->bindParam(":ID_DATA_CONTAINER", $idDataContainer, PDO::PARAM_INT);
            $this->psGetDataByDataContainer->execute();

            do {
                $result = $this->psGetDataByDataContainer->fetch(PDO::FETCH_ASSOC);

                // while ($result = $this->psGetDataByDataContainer->fetch(PDO::FETCH_ASSOC)) {
                if ($returnResult === false) {
                    $returnResult = array();
                }
                if ($result) {
                    array_push($returnResult, new LPoint(intval($result["idYData"]), $result["xDataValue"], $result["yDataValue"]));
                }
            } while ($result);
            // }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }
    /**
     * Insert an xData
     *
     * @param $xData xData of a point, can be a date, a number or a string
     * @return boolean true if succeed, else false
     */
    public function insertXData($xData): bool
    {
        $returnResult = false;
        try {
            $this->psInsertXData->bindParam(":VALUE", $xData, PDO::PARAM_STR);
            $returnResult = $this->psInsertXData->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Insert a data on Y axis
     *
     * @param float $value value that is always a number
     * @param integer $idDataContainer id of the data container
     * @param integer $idXData id of the x data that is related to
     * @return boolean true if succeed, else false
     */
    public function insertYData(float $value, int $idDataContainer, int $idXData): bool
    {
        $returnResult = false;
        try {
            $this->psInsertYData->bindParam(":VALUE", $value);
            $this->psInsertYData->bindParam(":ID_DATA_CONTAINER", $idDataContainer, PDO::PARAM_INT);
            $this->psInsertYData->bindParam(":ID_X_DATA", $idXData, PDO::PARAM_INT);
            $returnResult = $this->psInsertYData->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Insert all the data of several dataContainers
     *
     * @param array $dataContainers array of LDataContainer
     * @return boolean true if succeed, else false
     */
    public function insertArrayData(array $dataContainers)
    {
        $returnResult = true;
        for ($indexPoint = 0; $indexPoint < count($dataContainers[0]->getPoints()); $indexPoint++) {
            $this->insertXData($dataContainers[0]->getCoordX($indexPoint));
            $lastInsertIdX = $this->dbh->lastInsertId();
            foreach ($dataContainers as $dataContainer) {
                // If there is an error we return false adn go outside of the loop
                if (!$this->insertYData($dataContainer->getCoordY($indexPoint), $dataContainer->getId(), $lastInsertIdX)) {
                    $returnResult = false;
                    break;
                }
            }
            // Get outside of the loop is there is an error
            if (!$returnResult) {
                break;
            }
        }
        return $returnResult;
    }

    /**
     * Delete an x data
     *
     * @param integer $idXData id of the x data
     * @return boolean true if suceed, else false
     */
    public function deleteXData($idXData): bool
    {
        $returnResult = false;
        try {
            $this->psDeleteXData->bindParam(":ID_X_DATA", $idXData, PDO::PARAM_INT);
            $returnResult = $this->psDeleteXData->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Delete several x data
     *
     * @param LDataContainer $dataContainer data container that has some x data
     * @return boolean true if succeed else false
     */
    public function deleteArrayXData(LDataContainer $dataContainer): bool
    {
        $returnResult = true;
        foreach ($dataContainer->getPoints() as $point) {
            if (!$this->deleteXData($point->getXValue())) {
                $returnResult = false;
                break;
            }
        }
        return $returnResult;
    }
}
