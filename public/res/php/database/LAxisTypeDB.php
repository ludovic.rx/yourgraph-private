<?php

/** LAxesTypeDB
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LAxesTypeDB
 *  @author ludovic.rx@eduge.ch
 */

/**
 * @brief Class that makes query on DB for axes types
 */
class LAxisTypeDB
{
    /**>Host of database */
    private $dbh = null;

    /**>Prepare get axis type by id */
    private $psGetAxisTypeById = null;
    /**>Sql for get axis type by id*/
    private $sqlGetAxisTypeById = "";

    /**>Prepare get all axis type */
    private $psGetAllAxisType = null;
    /**>Sql for get all axis type*/
    private $sqlGetAllAxisType = "";

    /**>Prepare id of type number */
    private $psGetIdNumber = null;
    /**>Sql to get id of type number*/
    private $sqlGetIdNUmber = "";

    /**
     * Create an instance of LAxisTypeDB that can make queries on the database
     */
    public function __construct()
    {

        // Sets all the sql queries
        $this->sqlGetAxisTypeById = "SELECT idAxisType, axisTypeName FROM axesTypes WHERE idAxisType = :ID_AXIS_TYPE";
        $this->sqlGetAllAxisType = "SELECT idAxisType, axisTypeName FROM axesTypes";
        $this->sqlGetIdNUmber = "SELECT idAxisType FROM axesTypes WHERE axisTypeName = 'number'";

        // Prepare all the queries
        try {
            $this->dbh = EDatabase::getInstance();
            $this->psGetAxisTypeById = $this->dbh->prepare($this->sqlGetAxisTypeById);
            $this->psGetAllAxisType = $this->dbh->prepare($this->sqlGetAllAxisType);
            $this->psGetIdNumber = $this->dbh->prepare($this->sqlGetIdNUmber);
        } catch (PDOException $e) {
            error_log($e->getMessage());
        }
    }

    /**
     * Get a specifix axis type by id
     *
     * @param integer $id id of the axis type
     * @return LAxisType|false LAxisType if succeed, else false
     */
    public function getAxisTypeById(int $id)
    {
        $returnResult = false;
        try {
            $this->psGetAxisTypeById->bindParam(":ID_AXIS_TYPE", $id, PDO::PARAM_INT);
            $this->psGetAxisTypeById->execute();
            if ($result = $this->psGetAxisTypeById->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = new LAxisType($result["idAxisType"], $result["axisTypeName"]);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
        }
        return $returnResult;
    }

    /**
     * Get all the axes types
     *
     * @return array|false array of LAxisType if succeed else false
     */
    public function getAllAxisType()
    {
        $returnResult = false;
        try {
            $this->psGetAllAxisType->execute();

            if ($result = $this->psGetAllAxisType->fetchAll(PDO::FETCH_ASSOC)) {
                $returnResult = array();
                foreach ($result as $key => $type) {
                    array_push($returnResult, new LAxisType($type["idAxisType"], $type["axisTypeName"]));
                }
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
        }
        return $returnResult;
    }

    /**
     * Return the id of the number type
     *
     * @return integer|boolean int if succeed, else fale
     */
    public function getIdNumber()
    {
        $returnResult = false;
        try {
            $this->psGetIdNumber->execute();

            if ($result = $this->psGetIdNumber->fetchAll(PDO::FETCH_ASSOC)) {
                if(count($result) == 1) {
                    $returnResult = intval($result[0]['idAxisType']);
                }
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
        }
        return $returnResult;
    }
}
