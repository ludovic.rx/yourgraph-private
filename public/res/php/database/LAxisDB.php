<?php

/** LAxisDB
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class LAxisDB
 *  @author ludovic.rx@eduge.ch
 */

/**
 * @brief Class that makes query on DB for axes types
 */
class LAxisDB
{
    /** Host of database */
    private $dbh = null;

    /** Prepare to get axis by id */
    private $psGetAxisById = null;
    /** Sql to get one axis*/
    private $sqlGetAxisById = "";

    /** Prepare to get last axis */
    private $psGetAxisFromChart = null;
    /** Sql to get last axis*/
    private $sqlGetAxisFromChart = "";

    /** Preapre for the insert of axis*/
    private $psInsertAxis = null;
    /** Sql for the insert of an axis */
    private $sqlInsertAxis  = "";

    /** Preapre for the delete an axis*/
    private $psDeleteAxis = null;
    /** Sql to delete an axis */
    private $sqlDeleteAxis  = "";

    /**
     * Create an instance of LAxisDB that can make queries on the database
     */
    public function __construct()
    {

        // Sets all the sql queries
        $this->sqlGetAxisById = "SELECT idAxis, axisName, idAxisType, axisTypeName FROM axes JOIN axesTypes USING(idAxisType) WHERE idAxis = :ID_AXIS";
        $this->sqlGetAxisFromChart = "SELECT idAxis, axisName, idAxisType, axisTypeName FROM axes JOIN axesTypes USING(idAxisType) WHERE idChart = :ID_CHART AND axisLetter = :AXIS_LETTER";
        $this->sqlInsertAxis = "INSERT INTO axes (axisName, axisLetter, idAxisType, idChart) VALUES(:AXIS_NAME, :AXIS_LETTER, :ID_AXIS_TYPE, :ID_CHART)";
        $this->sqlDeleteAxis = "DELETE FROM axes WHERE idAxis = :ID_AXIS";

        // Prepare all the queries
        try {
            $this->dbh = EDatabase::getInstance();
            $this->psGetAxisById = $this->dbh->prepare($this->sqlGetAxisById);
            $this->psGetAxisFromChart = $this->dbh->prepare($this->sqlGetAxisFromChart);
            $this->psInsertAxis = $this->dbh->prepare($this->sqlInsertAxis);
            $this->psDeleteAxis = $this->dbh->prepare($this->sqlDeleteAxis);
        } catch (PDOException $e) {
            echo $e->getMessage();
            error_log($e->getMessage());
        }
    }

    /**
     * Get an axis thanks the Id
     *
     * @param integer $id id of the axis
     * @return LAxis|false
     */
    public function getAxisById(int $id)
    {
        $returnResult = false;
        try {
            $this->psGetAxisById->bindParam(":ID_AXIS", $id, PDO::PARAM_INT);
            $this->psGetAxisById->execute();
            if ($result = $this->psGetAxisById->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = new LAxis(intval($result["idAxis"]), $result["axisName"], new LAxisType(intval($result["idAxisType"]), $result["axisTypeName"]));
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Get an axis from a chart
     *
     * @param integer $idChart id of the chart
     * @param string $letter X or Y
     * @return LAxis|false LAxis if succeed, else false
     */
    public function getAxisFromChart(int $idChart, string $letter)
    {
        $returnResult = false;
        try {
            $this->psGetAxisFromChart->bindParam(":AXIS_LETTER", $letter, PDO::PARAM_STR);
            $this->psGetAxisFromChart->bindParam(":ID_CHART", $idChart, PDO::PARAM_INT);
            $this->psGetAxisFromChart->execute();
            if ($result = $this->psGetAxisFromChart->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = new LAxis(intval($result["idAxis"]), $result["axisName"], new LAxisType(intval($result["idAxisType"]), $result["axisTypeName"]));
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Get the x axis of the chart
     *
     * @param integer $idChart id of the chart
     * @return LAxis|false Laxis if succeed, else false
     */
    public function getXAxisFromChart(int $idChart) {
        return $this->getAxisFromChart($idChart, LAxisLetter::X);
    }
    
    /**
     * Get the y axis of the chart
     *
     * @param integer $idChart id of the chart
     * @return LAxis|false Laxis if succeed, else false
     */
    public function getYAxisFromChart(int $idChart) {
        return $this->getAxisFromChart($idChart, LAxisLetter::Y);
    }

    /**
     * Get the last axis inserted
     *
     * @return LAxis|false
     */
    public function getLastAxis()
    {
        $returnResult = false;
        try {
            $this->psGetLastAxis->execute();
            if ($result = $this->psGetLastAxis->fetch(PDO::FETCH_ASSOC)) {
                $returnResult = new LAxis(intval($result["idAxis"]), $result["axisName"], new LAxisType($result["idAxisType"], $result["axisTypeName"]));
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Insert an axis in the db
     * 
     * @param string $axisName name of the axis
     * @param integer|null $idAxisType type of the axis
     * @param integer $idChart id of the chart
     * @param string $axisLetter letter of the axis
     *
     * @return bool true if succeed else false
     */
    public function insertAxis(string $axisName, ?int $idAxisType, int $idChart, string $axisLetter)
    {
        $returnResult = false;
        try {
            $this->psInsertAxis->bindParam(":AXIS_NAME", $axisName, PDO::PARAM_STR);
            $this->psInsertAxis->bindParam(":AXIS_LETTER", $axisLetter, PDO::PARAM_STR);
            $this->psInsertAxis->bindParam(":ID_AXIS_TYPE", $idAxisType, PDO::PARAM_INT);
            $this->psInsertAxis->bindParam(":ID_CHART", $idChart, PDO::PARAM_INT);
            $returnResult = $this->psInsertAxis->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }

    /**
     * Insert an X Axis
     *
     * @param string $axisName name of the axis
     * @param integer|null $idAxisType id of the axis type
     * @param integer $idChart id of the chart
     * @return boolean true if succeed else false
     */
    public function insertXAxis(string $axisName, ?int $idAxisType, int $idChart): bool
    {
        return $this->insertAxis($axisName, $idAxisType, $idChart, LAxisLetter::X);
    }

    /**
     * Insert an Y Axis
     *
     * @param string $axisName name of the axis
     * @param integer|null $idAxisType id of the axis type
     * @param integer $idChart id of the chart
     * @return boolean true if succeed else false
     */
    public function insertYAxis(string $axisName, ?int $idAxisType, int $idChart): bool
    {
        return $this->insertAxis($axisName, $idAxisType, $idChart, LAxisLetter::Y);
    }

    /**
     * Delete an axis
     *
     * @param integer $idAxis id of the axis to delete
     * @return boolean true if succeed, else false
     */
    public function deleteAxis(int $idAxis): bool
    {
        $returnResult = false;
        try {
            $this->psDeleteAxis->bindParam(":ID_AXIS", $idAxis, PDO::PARAM_INT);
            $returnResult = $this->psDeleteAxis->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $returnResult;
    }
}
