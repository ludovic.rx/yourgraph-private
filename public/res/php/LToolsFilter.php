<?php

/** LToolsFilter
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class for filters
 *  @author ludovic.rx@eduge.ch
 */

/**
 * Class that has only static functions
 * These are the functions for the project to filter input
 */
class LToolsFilter
{
    /*--------PUBLIC--------*/

    /**
     * Filter the first name
     *
     * @param string $firstName var the will contain the first name
     * @return bool true if valid, else false
     */
    public static function filterFirstName(&$firstName): bool
    {
        return self::filterInput($firstName, INPUT_POST, "firstName", FILTER_SANITIZE_STRING);
    }

    /**
     * Filter the first name
     *
     * @param string $lastName var the will contain the last name
     * @return bool true if valid, else false
     */
    public static function filterLastName(&$lastName): bool
    {
        return self::filterInput($lastName, INPUT_POST, "lastName", FILTER_SANITIZE_STRING);
    }

    /**
     * Filter the email
     *
     * @param string $email var the will contain the email
     * @return bool true if valid, else false
     */
    public static function filterEmail(&$email): bool
    {
        return self::filterInput($email, INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Filter the password
     *
     * @param string $password var the will contain password
     * @return bool true if valid, else false
     */
    public static function filterPassword(&$password): bool
    {
        return self::filterInput($password, INPUT_POST, "password", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }

    /**
     * Filter the repeat password
     *
     * @param string $repeatPassword var the will contain the password repeated
     * @return bool true if valid, else false
     */
    public static function filterRepeatPassword(&$repeatPassword): bool
    {
        return self::filterInput($repeatPassword, INPUT_POST, "repeatPassword", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }

    /**
     * Fitler the chart name 
     *
     * @param string $chartName chart name
     * @return bool true if valid, else false
     */
    public static function filterChartName(&$chartName): bool
    {
        return self::filterInput($chartName, INPUT_POST, "chartName", FILTER_SANITIZE_STRING);
    }

    /**
     * Filter the chart type
     *
     * @param integer $chartType type of the chart
     * @return bool true if valid else false
     */
    public static function filterChartType(&$chartType): bool
    {
        return self::filterInt($chartType, INPUT_POST, "chartType");
    }

    /**
     * Fitler the X axis name
     *
     * @param string $xAxisName name of the x axis
     * @return bool true if valid, else false
     */
    public static function filterXAxisName(&$xAxisName): bool
    {
        return self::filterInput($xAxisName, INPUT_POST, "xAxisName", FILTER_SANITIZE_STRING);
    }

    /**
     * Fitler the Y axis name
     *
     * @param string $yAxisName name of the y axis
     * @return bool true if valid, else false
     */
    public static function filterYAxisName(&$yAxisName): bool
    {
        return self::filterInput($yAxisName, INPUT_POST, "yAxisName", FILTER_SANITIZE_STRING);
    }

    /**
     * Filter the x axis type
     *
     * @param integer $idXAxisType id of the x axis type
     * @return bool true if valid, else false
     */
    public static function filterIdXAxisType(&$idXAxisType): bool
    {
        return self::filterInt($idXAxisType, INPUT_POST, "selectType0");
    }

    /**
     * Filter the y axis type
     *
     * @param integer $idYAxisType id of the x axis type
     * @return bool true if valid, else false
     */
    public static function filterIdYAxisType(&$idYAxisType): bool
    {
        return self::filterInt($idYAxisType, INPUT_POST, "selectType1");
    }

    /**
     * Filter the data for the x axis
     *
     * @param mixed $dataXAxis data on the x axis
     * @return boolean true if valid, else false
     */
    public static function filterDataXAxis(&$dataXAxis): bool
    {
        return self::filterInput($dataXAxis, INPUT_POST, "dataXAxis", FILTER_SANITIZE_STRING);
    }

    /**
     * Filter the data for the y axis
     *
     * @param mixed $dataYAxis Axis data on the y axis
     * @return boolean true if valid, else false
     */
    public static function filterDataYAxis(&$dataYAxis): bool
    {
        return self::filterInput($dataYAxis, INPUT_POST, "dataYAxis", FILTER_SANITIZE_STRING);
    }

    /**
     * Filter the success save 
     *
     * @param boolean $saveSuccess defines if we succeed to save
     * @return boolean true if saved, else false
     */
    public static function filterSaveModal(&$saveSuccess): bool
    {
        return self::filterBool($saveSuccess, INPUT_GET, "saveSuccess");
    }

    /**
     * Filter the success import 
     *
     * @param boolean $importSuccess defines if we succeed to import
     * @return boolean true if import, else false
     */
    public static function filterImportModal(&$importSuccess): bool
    {
        return self::filterBool($importSuccess, INPUT_GET, "importSuccess");
    }

    /**
     * Filter the success save 
     *
     * @param boolean $deleteSuccess defines if we succeed to delete
     * @return boolean true if valid, else false
     */
    public static function filterDeleteModal(&$deleteSuccess): bool
    {
        return self::filterBool($deleteSuccess, INPUT_GET, "deleteSuccess");
    }

    /**
     * Filter a the id of the chart
     *
     * @param int $idChart id of the chart in th db
     * @return boolean, true if valid, else false
     */
    public static function filterIdChart(&$idChart): bool
    {
        return self::filterInt($idChart, INPUT_GET, "idChart");
    }

    /**
     * Filter the name of the data container
     *
     * @param string $nameDataContainer name of the data container
     * @return boolean true if valid, else false
     */
    public static function filterNameDataContainer(&$nameDataContainer): bool
    {
        return self::filterInput($nameDataContainer, INPUT_POST, "nameDataContainer", FILTER_SANITIZE_STRING);
    }

    /**
     * Filter the index of the data container
     *
     * @param int $idDataContainer id of the data container
     * @return boolean true if valid, else false
     */
    public static function filterIdDataContainer(&$idDataContainer): bool
    {
        return self::filterInt($idDataContainer, INPUT_POST, "idDataContainer");
    }

    /**
     * Filter the idCoord
     *
     * @param int $idCoord number of coord line
     * @return boolean true if valid, else false
     */
    public static function filterIdCoord(&$idCoord): bool
    {
        return self::filterInt($idCoord, INPUT_POST, "idCoord");
    }

    /**
     * Filter the coord X
     *
     * @param string $coordX x coord
     * @param string $xAxisType type of the x axis
     * @return boolean true if valid, else false
     */
    public static function filterCoordX(&$coordX, $xAxisType): bool
    {
        $returnResult = false;
        $name = "coordX";
        $type = INPUT_POST;
        switch ($xAxisType) {
            case 'number':
                $returnResult = self::filterFloat($coordX, $type, $name);
                break;
            case 'date':
                $returnResult = self::filterInput($coordX, $type, $name, FILTER_SANITIZE_STRING);
                if ($returnResult) {
                    if (!DateTime::createFromFormat("Y-m-d", $coordX)) {
                        $returnResult = false;
                    } else {
                        $returnResult = true;
                    }
                }
                break;
            case 'string':
            default:
                $returnResult = self::filterInput($coordX, $type, $name, FILTER_SANITIZE_STRING);
                break;
        }
        return $returnResult;
    }

    /**
     * Filter the coord Y
     *
     * @param array $coordY y coord
     * @param LChart $chart chart
     * @return boolean true if valid, else false
     */
    public static function filterCoordY(&$coordY, $chart): bool
    {
        $result = true;
        $coordY = array();
        $type = INPUT_POST;
        for ($i = 0; $i < count($chart->getDataContainers()); $i++) {
            $name = "coordY$i";
            $resultFilter = self::filterFloat($currentCoord, $type, $name);
            if ($resultFilter) {
                array_push($coordY, $currentCoord);
            } else {
                $result = false;
            }
        }
        return $result;
    }

    /*--------END PUBLIC--------*/

    /*--------PRIVATE--------*/

    /**
     * Filter an input
     *
     * @param mixed $var var that has the result of filter
     * @param integer $type type of data
     * @param string $name name of the input
     * @param int $filter filter to apply
     * @param int $filterValidate filter that validate. Optional. Default = FILTER_DEFAULT
     * @param int $filterFlag flag of the filter DEFAULT 0, says that no flag will be applied
     * @return bool true if is valid, else false
     */
    private static function filterInput(&$var, string $type, string $name, int $filter, int $filterValidate = FILTER_DEFAULT, int $filterFlag = 0): bool
    {
        $var = null;
        if ($filterFlag == 0) {
            $var = filter_input($type, $name, $filter);
        } else {
            $var = filter_input($type, $name, $filter, $filterFlag);
        }
        return filter_var($var, $filterValidate);
    }

    /**
     * Fitler an integer
     *
     * @param integer|null $number number
     * @param integer $type type of input
     * @param string $name name of the input
     * @return void
     */
    private static function filterInt(?int &$number, int $type, string $name): bool
    {
        $result = self::filterInput($number, $type,  $name, FILTER_SANITIZE_NUMBER_INT, FILTER_VALIDATE_INT);
        if ($result !== null || $result === "0") {
            $number = intval($number);
            $result = true;
        }
        return $result;
    }

    /**
     * Fitler an integer
     *
     * @param float|null $number number
     * @param integer $type type of input
     * @param string $name name of the input
     * @return void
     */
    private static function filterFloat(?float &$number, int $type, string $name): bool
    {
        $result = self::filterInput($number, $type,  $name, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_VALIDATE_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        if ($result !== null || $result === "0") {
            $number = floatval($number);
            $result = true;
        }
        return $result;
    }

    /**
     * Filter a boolean
     *
     * @param boolean|null $var var that contains result filtered
     * @param integer $type type of input
     * @param integer $name name of input
     * @return bool true if is valid, else false
     */
    private static function filterBool(?bool &$var, int $type, string $name): bool
    {
        $returnResult = self::filterInput($var, $type, $name, FILTER_SANITIZE_NUMBER_INT, FILTER_VALIDATE_BOOLEAN);
        if ($var !== null) {
            $var = boolval($var);
        }
        return $returnResult;
    }

    /*--------END PRIVATE--------*/
}
