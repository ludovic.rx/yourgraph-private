-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: YourGraph
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.27-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `axes`
--

DROP TABLE IF EXISTS `axes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `axes` (
  `idAxis` int(11) NOT NULL AUTO_INCREMENT,
  `axisName` varchar(150) NOT NULL DEFAULT 'Empty Axis',
  `axisLetter` varchar(1) NOT NULL DEFAULT 'X',
  `idAxisType` int(11) DEFAULT NULL,
  `idChart` int(11) NOT NULL,
  PRIMARY KEY (`idAxis`),
  KEY `axes_FK` (`idAxisType`),
  KEY `axes_FK_Chart` (`idChart`),
  CONSTRAINT `axes_FK_AxisType` FOREIGN KEY (`idAxisType`) REFERENCES `axesTypes` (`idAxisType`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `axes_FK_Chart` FOREIGN KEY (`idChart`) REFERENCES `charts` (`idChart`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `axesTypes`
--

DROP TABLE IF EXISTS `axesTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `axesTypes` (
  `idAxisType` int(11) NOT NULL AUTO_INCREMENT,
  `axisTypeName` varchar(100) NOT NULL,
  PRIMARY KEY (`idAxisType`),
  UNIQUE KEY `axesTypes_UN` (`axisTypeName`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bars`
--

DROP TABLE IF EXISTS `bars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bars` (
  `idBar` int(11) NOT NULL AUTO_INCREMENT,
  `barName` varchar(100) NOT NULL,
  `value` int(11) NOT NULL,
  `idChart` int(11) NOT NULL,
  PRIMARY KEY (`idBar`),
  KEY `bars_FK` (`idChart`),
  CONSTRAINT `bars_FK` FOREIGN KEY (`idChart`) REFERENCES `charts` (`idChart`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `charts`
--

DROP TABLE IF EXISTS `charts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charts` (
  `idChart` int(11) NOT NULL AUTO_INCREMENT,
  `chartName` varchar(255) NOT NULL,
  `idChartType` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idChart`),
  KEY `charts_FK` (`idChartType`),
  KEY `charts_FK_User` (`idUser`),
  CONSTRAINT `charts_FK` FOREIGN KEY (`idChartType`) REFERENCES `chartsTypes` (`idChartType`),
  CONSTRAINT `charts_FK_User` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chartsTypes`
--

DROP TABLE IF EXISTS `chartsTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chartsTypes` (
  `idChartType` int(11) NOT NULL AUTO_INCREMENT,
  `chartTypeName` varchar(100) NOT NULL,
  PRIMARY KEY (`idChartType`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dataContainers`
--

DROP TABLE IF EXISTS `dataContainers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataContainers` (
  `idDataContainer` int(11) NOT NULL AUTO_INCREMENT,
  `dataContainerName` varchar(100) NOT NULL,
  `idChart` int(11) NOT NULL,
  PRIMARY KEY (`idDataContainer`),
  KEY `lines_FK` (`idChart`),
  CONSTRAINT `lines_FK` FOREIGN KEY (`idChart`) REFERENCES `charts` (`idChart`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(150) NOT NULL,
  `lastName` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `users_UN` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xData`
--

DROP TABLE IF EXISTS `xData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xData` (
  `idXData` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(150) NOT NULL,
  PRIMARY KEY (`idXData`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `yData`
--

DROP TABLE IF EXISTS `yData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yData` (
  `idYData` int(11) NOT NULL AUTO_INCREMENT,
  `value` float NOT NULL,
  `idDataContainer` int(11) NOT NULL,
  `idXData` int(11) NOT NULL,
  PRIMARY KEY (`idYData`),
  KEY `yData_FK` (`idDataContainer`),
  KEY `yData_FK_idXData` (`idXData`),
  CONSTRAINT `yData_FK_Line` FOREIGN KEY (`idDataContainer`) REFERENCES `dataContainers` (`idDataContainer`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `yData_FK_idXData` FOREIGN KEY (`idXData`) REFERENCES `xData` (`idXData`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'YourGraph'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-13 21:08:50
