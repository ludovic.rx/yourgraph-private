CREATE USER '[NameOfUser]'@'%';
ALTER USER '[NameOfUser]'@'%'
IDENTIFIED BY '[Password]' ;
GRANT Select, Update, Insert, Delete ON YourGraph.* TO '[NameOfUser]'@'%';
