# LOGBOOK

## 03/05/2021

* lecture de l'énoncé
* Rédaction du product backlog et du plan de test en même temps
* Rédaction du planning prévisionnel (finalisation 10h50)
* Première Réunion avec les deux experts TPI ->
  * OK pour journal de bord électronique
  * Importation de données très importante, format n'est pas imposé
* création maquette site

* Schéma qui montre comment on choisit le type du graphique pour un utilisateur connecté

![Schéma choix du type de graphique](images/choixtype.png)

* Schéma qui montre comment on enregistre

![Schéma choix de l'enregistrement](images/enregistrement.png)

* Schéma base de données

![Schéma de la base de données](images/modeleDB.png)

* Visite de M.Aliprandi
  * Suggestion d'enregistrement JSON -> choisit de rester sur une base de données qui est plus rigide que le JSON
  * éclaircissement sur import export

* Création de la structure du site web

* [Template du site trouvées](https://startbootstrap.com/previews/sb-admin-2)
* Il faut écrire le nom de celui qui a fait la template dans le footer

* Finalement j'ai décidé de ne pas refaire de maquette pour le projet et de m'inspirer des maquettes données comme base pour créer le site

* Implémentation dans mon site des template, j'ai du supprimer les fichiers inutiles

* Je n'ai pas réussi à faire toutes les tâches de mon planning, car la planification m'a pris plus de temps que prévu, cependant j'ai presque fini la création des pages de connexion et d'enregistrement, çA me prendra moins de temps car j'ai trouvé une bonne template
* L'arborescence a été assez rapide à mettre en place car j'ai repris ce que je faisais dans les aurtes projets web

* Problème avec le git avec des fichiers Zone.Identifier, à régler demain

## 04/05/2021

* Problème des fichiers Zone.Identifier réglé, il faut rajouter dans le .gitignore : ``` **/*Zone.Identifier ```

* utilisation de class pour connecteur database de M.Aigroz
* je mets le fichiers de constantes pour la base de données dans le gitignore pour ne pas mettre l'identifiant et le mot de passe sur le git, j'ai écrit das le [README](../README.md) la marche à suivre pur créer ce fichier de constantes

* Création utilisteur et base de données

* Je me rends compte qu'il faudrait stocket le type de l'axe pour vérifier que l'utilisateur est cohérent lorsqu'il entre ses données donc je rajoute une table AxesTypes [Axes customizing Google Charts](https://developers.google.com/chart/interactive/docs/customizing_axes)
* J'ai remarqué que la table data devra avoir des champs en type texte, même si l'axe est du type int, puis je convertiras dans le code suivant le type
* Je décide de préciser que la foreign key *idUser* dans la tyble charts est en cascade, car si on supprime un user, on supprime aussi ses graphiques
* Je fais la même chose pour la foreign key *idChart* dans la table data, si on supprie un grpahique, toutes ses données seront supprimées
* à 9h00 j'ai fini de créer la base de données sur mon serveur
![Diagramme du 04 05 2021](images/dbDiagram040521.png)

* Erreur recontrée depuis le PHP car  il y avait un dollars dans le password du user de la database -> changer le mot de passe

* J'ai pris un peu plus de temps pour mettre en place la base de données, car j'ai du écrire le instructions dans le [README](../README.md)

* Pour changer le css, j'ai eu un problème, car le css utilisé dans les pages est sous le format .min.css ce qui veut dire que le css est sur une seule ligne, cepedant puisqu'on a aussi les css avec le format normal, j'ai changé ce fichier et j'ai utilisié : [Minifier](https://www.minifier.org/) puis j'ai remplacé le contenu du fichier min.css

* J'ai finalisé la page de connection et de création de compte en changeant les images utilsiation de [Pixabay](https://pixabay.com/fr/) pour les images
* Rajout de l'icone du site
* Recherche d'icone pour page graphiques [Font Awesome](https://fontawesome.com/)
* Problème avec sidebar qui n'est pas la même sur toutes les pages
* Création d'une class LTools qui contient toutes les fonctiones globales, nécessaires au projet
* Création de fonction pour inclure certaines parties de l'HTML
* Utilisation de forms bootstrap [Bootstrap Forms](https://getbootstrap.com/docs/5.0/forms/overview/)
* J'ai pris plus de temps pour finir le squelette de la page de modification de graphique, mais j'ai pu mettre en place des choses pour le reste du projet
* J'ai fini plus rapidement la création de la page d'affichages des graphiques que prévu
* Pour gérer les utilisateurs, j'ai repris une classe que j'ai faite en atelier qui gère les utilisateurs d'une base de donnée. J'utilise également la class LUser qui encapsule un user (aussi utilisée dans d'autres projets) je dois juste la modifier pour qu'elle corresponde à ma base de données
* J'ai fini l'enregistrement d'un utilisateur
* J'ai préféré me concentrer sur le code et laissé la documentation technique pour aujourd'hui

## 05/05/2021

* Je dois créer une classe Session pour gérer la sesssion car j'en ai besoin pour les users, je reprend la forme de la class LSession que j'ai créée dans d'autres projets WEB
* Discussion avec un camarade par rapport au getter setter pour les classes, alors j'en rajoute sur la classe LUser
* je me rends compte que j'ai meilleur temps de faire un singleton et alors une classe static pour la session, comme ca je n'ai pas besoin de l'instancier, pour faire mon singleton, j'ai repris la base de la calsse EDatabase
* J'hésite à faire des fonctions dans login et register ou à simplement écrire dans la page
* Problème avec session ``` Warning: session_start(): Cannot start session when headers already sent ``` pour régler ça, j'utilise LSesssion::getIsntance() dans la page all.inc
* la connexion, et la déconexion fonctionnent

* Mainentant il faut que je crée des classes en fonctions des tables que j'ai pour pouvoir stocker mes graphiques en PHP
* Je remarque que ma classe LChartType et LAxisType sont très similaires, j'ai alors décidé de faire une classe LType que LAxisTYpe et LChartType héritent our ne pas faire de la redondance [Héritage PHP](https://www.php.net/manual/fr/language.oop5.inheritance.php) [Abstract class](https://www.php.net/manual/fr/language.oop5.abstract.php)
* Je vais mainteant commencer à faire les classes qui gèrent les différentes classes au niveau de la base de données
* Changement du nom de deux champs et d'une table pour cohérence au niveau du nommage ![DB Diagram 05 052021 11:10](images/dbDiagram050521.png)
* Problème avec base de données parce que j'avais changé le nomm de certains champs, j'ai exporté la base de données, sauf qu'il falait que je change l'ordre de création des tables, car certaines tables qui avait de clés étrangères étaient créées avant les tables qui contenait leur clé étrangère
* Obtenir tous les type de chart fonctionne
* J'ai rencontré un problème avec le champ name qui est présent dans plusieurs tables pour le php, pour faciliter, j'ai renommé les champs des table pour qui soient spécifique ![DbDiagram050221v2](images/dbDiagram050221v2.png)
* Question : faut faire une table enrte axes et chart ?
* Est-ce que c'est mieux de faire des join ou pas ? après avoir parlé avec un autre élève, je pense que je ne suis pas obligé de faire de Join
* Je prends plus de temps pour faire la création d'un graphique dans la base de données, mais ca me permettra de faire les autres tâches sur la base de données plus rapidement
* J'ai créer la modal pour créer un graphique

## 06/05/2021

* Tests effectués selon le plan de test
* Créer fonctions pour filtrer submit ?
* changer certains champs pour les mettre null
* On peut créer un graphique dans la base de données
* Je me rends compte qu'il faut que je puiss d'abord sauvegarder dans la session puis après enregistrer dans la base de données
* La sauvegarde dans la session est fonctionnelle
* Ajout de données dans la session
* Il faut que je rajoute un champ order sur la table data, pour savoir à quelle position est une donnée
* Pour insérer un tablea u de données, j'ai du tuiliser les trnsaction, car si il y a une erreur dans les données, je dois revenir en arrière.
* J'ai eu des errerus car j'avais mis le type mixed et il n'acceptait ps un string en tant que type mixte alors j'ai enlevé la spécification du type
* Je met en place l'ajout des axes et des données
* Une popup est montrée pour indiquer si la sauvegarde a fonctionnée ou pas
* [Tooltip](https://getbootstrap.com/docs/4.6/components/tooltips/)
* Il faudra que lorsque je sauvegarde un graphique, je supprime toutes ses anciennes données (sorte de synchronisation entre la session et la base de données )
* J'ai passé beaucoup de temps pour enregistrer correctement dans la session, ca me permettra de plus facilement modifier dans la base de données
* J'ai fait un peu de documentation technique

## 10/05/2021

* Finir affichage graphique
* En discutant avec Juliano qui utilse aussi des graphiques, je décide de créer une page qui fait en encode json du chart de ma session [json_encode()](https://www.php.net/manual/fr/json.constants.php)
  * Problème avec le json_encode qui ne permet pas n'affiche les propritétés privées de mon objet, solution : [JsonSerializable](https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members), je rajoute cette interface sur toutes mes classses containers et j'ajoute la fonction jsonSerialize()
  * Utilisation d'un fetch [fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch)
  * Utilsiation de location.href dans le fetch pour lien absolu
  * type de graphique avec [freeze](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze)
  * Finalement je n'utilise pas de fetch
  * Erreur avec les types de données en js, donc je crée une fonction qui converti en nombre les données de l'axe qui sont du type nombre [Parse int](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/parseInt)
* Problème de conception de la base de données résolution ![Résuolution type de charts en base de données](images/resolution%20ProblemTypeChart.png)
  * Nouveau diagrame : ![diagramme aprè modifications](images/dbDiagram100521.png)
  * Il faut échanger X et Y
* Je suis en train de créer des forumlaires différents en focntion du type de graphique, car onn'entre pas les données de la même manière
* Je m'occupe en même temps de la modification du graphique, je fais tout cela dans la session pour l'instant, l'idée est que lorsque l'utilisateur appuie sur sauvegarder, cela enregistre son graphique dans la base de données, sinon, ça le garde en session
* J'ai travaillé un peu en js pour pouvoir afficher le graphique en fonction du type choisi mais il manque encore des choses
* NB: il faut faire l'update des axes
* On peut créer maintenant un bar chart et il s'affiche correctement
* Mettre les liens dans la doc technique à la fin car sinon les liens changent tout le temps
* Il faudra refaire un diagramme sur diagram.io car a changé

## 11/05/2021

* Nouveau réglages avec la base de données ![Nouveau diagramme](images/dbDiagram110521.png)
* L'export en svg fonctionne
* On peut ajouter supprimer et modifier une bar d'un barchart
* Dans mon planning effectif, je change légèrement une tâche Sauvegarde graphique dans la session devient Sauvegarde et modification d'un graphique, car je me suis rendu compte que je peux d'abord enregistrer dans la session puis saucegarder dans la base de données
* Je me rends compte que l'insertion dans la base de données des barChart et des LineChart ou AreaChart est différente (il y a plusieurs lignes sur LineChart). L'insertion dans la table chart ne change pas, mais c'est l'insertion des données qui change
* Maintenant je met à jour l'insertion d'un graphique dans la base de données pour un barChart ![InsertBarChart](images/insertBarChart.png)
* J'ai décidé que mes barchart n'auront qu'une barre par données
* J'ai l'idée de créer une class abstraite qui gère les données, car on a des fonctions siilaires, getDataByChart, insertData que ce soit un linechart ou un barchart
* Je décide de delete toutes les data lorsqu'on sauve à nouveau un chart au lieu de faire un update
* Est-ce qu'on peut changer le type d'un graphique ?
* Maintenant qu'un user peut correctement sauvegarder un graphique dans la base de données, je travaille sur l'affichage des graphiques des user
* Affichage des charts fonctionne correctement
* Supprimer fonctionne correctement, de plus puisque j'ai mis les contraintes en cascade, cependant, les axes ne se suppriments pas automatiquement, il faut que je fasse une fonction de *Grabage Collector* qui supprime les axes qui ne correspondent à aucun graphique, je décide alors de faire une fonction qui supprime les axes qui sont contenu dans le chart que l'on supprime
* Je remarque qu'il y a un problème avec les axes. Il faut que je stocke l'idChart dans la tables axes et je rajoute un champ type qui n'accepte que X ou Y ![Probleme axes](images/resolution%20ProbelmeAxes.png)
* Je fais une class abstraite pour stocker X et  qui sont les seules valeurs que l'on peut donner pour la lettre de l'axe [idée](https://stackoverflow.com/questions/254514/enumerations-on-php)
* Je pourrais changer la class LAxis et stocker aussi la lettre de l'axe, mais pour éviter de perdre du temps, je pars du principe qu'un axe est stocké dans un chart et dans ce chart, on sait quel est l'axe x et y
* La sauvegarde dans la base de données est fixée
* Problème avec la sauvegarde
  * La transaction m'empêche de récupérer le dernier id créer donc je ne fais pas de transaction pour le save

## 12/05/2021

* Je décide de faire de l'héritage pour les classes LBar et LLine car elles se ressemblent beaucoup
* Rendez-vous avec les experts :
  * Discussion sur la manière de stocker les graphiques :
    * Solution pluis expansible serait de stocker cela avec une seule table et avec les données en json
    * Cependant c'est écrit dans le cahier des charges que c'est au minimum deux graphiques, aide pour faire la décision
La solution de stocker les données en JSON est intéressante, car plus extensible et moins restricitve, cependant, cela me prendrait en tout cas 1 jours pour mettre en place cette solution, je prendrais alors trop de risque. De plus, je me rend compte que les chart de Google ont deux manière de stocker les graphiques :

### Modèle de données pour un graphique linéaire

``` JS
var data = google.visualization.arrayToDataTable([
     ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
     ['2004/05',  165,      938,         522,             998,           450,      614.6],
     ['2005/06',  135,      1120,        599,             1268,          288,      682],
     ['2006/07',  157,      1167,        587,             807,           397,      623],
     ['2007/08',  139,      1110,        615,             968,           215,      609.4],
     ['2008/09',  136,      691,         629,             1026,          366,      569.6]
   ]);
```

### Modèle de données pour un graphique en barres

``` JS
var data = google.visualization.arrayToDataTable([
       ["Element", "Density", { role: "style" } ],
       ["Copper", 8.94, "#b87333"],
       ["Silver", 10.49, "silver"],
       ["Gold", 19.30, "gold"],
       ["Platinum", 21.45, "color: #e5e4e2"]
     ]);
```

Alors, ma base de données permet d'implémenter plus de graphiques que je ne le pensais, car beaucoup de types de graphique utilisent ces modèles pour les données

 ![Graphique que je peux implémenter](images/typeCharts.png)

Par ailleurs, je me rends compte que j'aurais du mieux analyser l'outil que j'utilise, cela m'aurait fait gagner du temps.

* Pour mieux disséquer mon code, j'ai creér la classe LToolsFilter qui contient toutes les fonctions de filtrage.

* En réfléchissant, je me rends compte que l'axe Y d'un graphique un nombre, aussi je doit enlever la possibilité à l'utilisateur de choisir cette option.
* J'ai créé la class LDataContainer qui peut gérer n'importe quel graphique au niveau des données, elle a un tableau de LPoint. Lorsque je parle de DataContainer, je parle d'une barre dans un chart, ou d'une ligne
* Je peux maintenant rajouter les autres types de graphiques, ils se sauvegardent correctement dans la session
* Il faut que je rajoute la possibilité de changer le nom du graphique et le type, puisque tous les graphiques de mon programme ont la même strcuture de données, on peut changer le type
* Maintenant que tout les types de graphiques disponibles se sauvegardent dans la session, il ne me reste qu'à refaire le save dans la base de données

## 17/05/2021

[Comment récupérer le dernier id inséré](https://stackoverflow.com/questions/49786047/do-i-need-transactions-to-get-the-correct-last-inserted-id-in-a-sequence)

[Last insert Id PHP.net](https://www.php.net/manual/fr/pdo.lastinsertid.php)

* Avec la modification de la base de données, j'ai pu mettre en place beaucoup plus de type de graphiques
* J'ai divisé ma classs LToolsHTML en créeant :
  * LToolsHTMLForm qui s'occupe des formulaires html
  * LToolsModal qui s'occupe des modal
* Import
  * Attention à changer le owner du dossier import ````sudo chown www-data:www-data ./import``` sinon il y a une erreur
  * Pour l'instant l'import marche lorsqu'il n'y a pas de données erronées dans le fichier csv

* Visite des experts
  * Il faut faire le résumé du TPI
  * Rajouter les actions entreprises à la suite du résultat des tests

* Visite de Monsieur Aliprandi
  * Changer l'affichage du formulaire de données pour que ce soit plus lisible

* Réalisation document technique
* Création modal importation

* J'ai changé le nom des contraintes

## 18/05/2021

* J'ai rajouté l'id user dans la requête qui récupère un graphique pour s'assurer que c'est bien le graphique de l'utilisateur.

* Documentation technique
* Effectuer les test, je dois mettre à jour les tests, car j'ai ajouté le concept de data containers

* Je met en place le système qui affiche à l'utilisateur les entrées invalides et valides. Je me rends compte que j'aurais pu créer un objet input qui stocke la valeur de l'input, est-ce qu'il est valide, son nom, son type, ses filtres. Cela me prendrait trop de temps de mettre cela en place, en tout cas une journée, alors je décide de pas mettre en place cette classe.

## 19/05/2021

* Réalisation de la documentation technique
* Réalisation du manuel utilisateur
* Résolution de bugs trouvés grâce aux tests effectués
* Résolution de problèmes visuels
* Export de données du graphique en session
  * [https://www.php.net/manual/fr/function.readfile.php](https://www.php.net/manual/fr/function.readfile.php)
  * [https://www.php.net/manual/fr/function.file-put-contents.php](https://www.php.net/manual/fr/function.file-put-contents.php)

## 20/05/2021

* Correction du manuel utilisateur
* Correction du manuel technique
* Finitions de la documentation technique
  * Comparaison des plannings
* Résumé du TPI
* Correction du texte dans le site
* Exportation du code source
* Génération de la doc avec doxygen
