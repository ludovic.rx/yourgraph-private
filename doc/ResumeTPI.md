# Résumé du TPI

## *Your Graph*

### Situation de départ

Afin de valider mon CFC d'informatique en développement d'application j'ai du effectuer le TPI (**T**ravail **P**ratique **I**ndividuel). Ce travail a duré 88 heures et était réparti sur 11 jours. On m'a demandé de faire un site web de gestion de graphiques. Le site doit permettre de créer un graphique et l'utilisateur doit pouvoir choisir entre au moins 2 types de graphiques. Il peut modifier les axes du graphique. L'utilisateur doit pouvoir entrer les données manuellement, ou il peut les importer. L'utilisateur doit pouvoir exporter son graphique. S'il le souhaite, l'utilisateur peut créer un compte et s'y connecter. Cela lui permet de sauvegarder son graphique.

### Mise en oeuvre

Pour réaliser ce projet, j'ai commencé par planifier les tâches que j'allais devoir implémenter. J'ai utilisé principalement dû PHP et des requêtes SQL pour réaliser mon site. J'ai décidé de réaliser ce projet en orienté objet. J'ai créé trois types de classes. Tout d'abord, les classes container qui contiennent des objets qui peuvent être mis dans la base de données. Puis, il y a les classes de la base de données que l'on peut instancier pour faire des requêtes sur la base de données. Enfin, les classes qui ne contiennent que des fonctions statiques. J'ai utilisé l'API Google Charts pour afficher les graphiques. J'ai alors créé ma classe qui interagit avec cette API et s'occupe par exemple de l'export. J'ai aussi créer certains fichiers dans une API interne. Ils me permettent par exemple d'ouvrir un fichier et de le stocker dans la session ou d'importer des données.

### Résultat

Finalement, j'ai réussi à développer mon site. Tous les points du cahier des charges sont présents. On peut créer un compte et s'y connecter. On peut créer un graphique, lui donner des axes et des données. De plus on peut même changer le type du graphique, on a 9 types de graphiques à choix. On peut importer des données sous le format csv, et on peut exporter les données sous le format csv ou svg. Lorsque l'on est connecté, on peut sauvegarder son graphique dans la base de données. Puis, on peut modifier ou supprimer ses graphiques enregistrés.
