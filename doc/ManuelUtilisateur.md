# Manuel utilisateur <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Introduction](#introduction)
- [Barre de navigation pour un utilisateur non authentifié](#barre-de-navigation-pour-un-utilisateur-non-authentifié)
- [Connexion au compte](#connexion-au-compte)
- [Inscription](#inscription)
- [Barre de navigation pour un utilisateur authentifié](#barre-de-navigation-pour-un-utilisateur-authentifié)
- [Déconnexion](#déconnexion)
- [Barre latérale sur la page de consultation des graphiques](#barre-latérale-sur-la-page-de-consultation-des-graphiques)
- [Création de graphique](#création-de-graphique)
  - [Fenêtre de création de graphique](#fenêtre-de-création-de-graphique)
  - [Création du graphique réussie](#création-du-graphique-réussie)
- [Modification du graphique](#modification-du-graphique)
  - [Options du graphique](#options-du-graphique)
  - [Axes du graphique](#axes-du-graphique)
    - [Création](#création)
    - [Modification](#modification)
  - [Données](#données)
    - [Boutons](#boutons)
    - [Conteneurs de données](#conteneurs-de-données)
    - [Données de conteneurs](#données-de-conteneurs)
- [Affichage du graphique](#affichage-du-graphique)
- [Barre latérale sur la page de graphique](#barre-latérale-sur-la-page-de-graphique)
  - [Logo](#logo)
  - [Ouvrir un graphique](#ouvrir-un-graphique)
  - [Sauvegarder un graphique](#sauvegarder-un-graphique)
  - [Importation](#importation)
  - [Exportation](#exportation)
    - [Exportation en CSV](#exportation-en-csv)
    - [Exportation en SVG](#exportation-en-svg)
- [Visualisation des graphiques](#visualisation-des-graphiques)
  - [Ouverture d'un graphique](#ouverture-dun-graphique)
  - [Supprimer un graphique](#supprimer-un-graphique)
- [Bouton pour faire défiler vers le haut](#bouton-pour-faire-défiler-vers-le-haut)
- [Page d'erreur](#page-derreur)

## Introduction

Ce document est destiné aux utilisateurs de YourGraph. Il vous permettra de comprendre comment le site fonctionne et comment l'utiliser correctement.

Pour installer le site sur un serveur web, veuillez vous référer au document [readme.md](../REAME.md).

## Barre de navigation pour un utilisateur non authentifié

![Barre de navigation pour un utilisateur non authentifié](images/analyseFonctionnelle/navigationNonAuthentifiepng.png)

> *Barre de navigation pour un utilisateur non authentifié*

Pour aller sur la page de connexion, appyuez sur le bouton *Log In Now* dans la barre de navigation.

## Connexion au compte

![Connexion](images/analyseFonctionnelle/formulaireLogin.png)

> *Page de connexion*

Après avoir créé votre compte, vous pouvez vous connecter. Il faut pour cela renseigner votre adresse e-mail et votre mot de passe.

Lorsque vous avez rempli tous les champs, appuyez sur le bouton *Login* pour vous connecter.

S'il se produit une erreur, les deux champs seront affichés en rouge, sinon vous serez redirigé sur la page de consultation des graphiques.

Pour vous créer un compte, appuyez sur le le lien *Create an Account* en dessous du bouton *Login*. Vous serez redirigé sur la page d'inscription.

## Inscription

![Inscription](images/analyseFonctionnelle/formulaireCreateAccount.png)

> *Page d'inscription*

Pour vous inscrire, il faut renseigner, votre prénom, nom de famille, adresse e-mail et un mot de passe. Vous ne pouvez pas avoir deux comptes avec la même adresse e-mail. Écrivez le même mot de passe dans les deux champs pour celui-ci (ces deux champs sont présents pour éviter les fautes de frappe).

Lorsque vous avez entré les informations de tous les champs, appuyez sur le bouton *Register Account* pour vous enregistrer.

S'il n'y a pas d'erreur, vous êtes redirigé sur la page de connexion.

Sinon, les champs qui ont subi une erreur seront affichés en rouge et les champs valides seront affichés en vert.

Si l'adresse e-mail est invalide ou déjà utilisée, le champ sera affiché en rouge.

Si les deux mots de passe que vous entrez ne sont pas les mêmes, les champs pour le mot de passe seront en rouge.

Appuyez sur le lien *Already have an account? Login!* en dessous du bouton *Register Account* si vous avez déjà un compte.

## Barre de navigation pour un utilisateur authentifié

![Barre de navigation pour un utilisateur authentifié](images/analyseFonctionnelle/navigationAuthentifiepng.png)

> *Navigation pour un utilisateur authentifié*

Lorsque vous êtes connecté, vous voyez votre prénom et votre nom. Appuyez dessus pour faire apparaître le bouton *Logout*. Ce bouton sert à vous déconnecter.

## Déconnexion

![Déconnexion](images/analyseFonctionnelle/popuLeave.png)

> *Fenêtre de déconnexion*

Appuyez sur le bouton *Logout* de la barre de navigation pour faire ouvrir cette fenêtre de déconnexion.

Appuyez sur le bouton *Logout* de la fenêtre pour vous déconnecter. Vous serez déconnecté et redirigé sur la page de connexion.

Appuyez sur le bouton *Cancel* ou sur la croix en haut à droite pour fermer la fenêtre.

## Barre latérale sur la page de consultation des graphiques

![Barre latérale page consultation graphiques](images/analyseFonctionnelle/sideBarGraphs.png)

> *Barre latérale sur la page de consultation de graphiques*

Vous avez une seule option lorsque vous êtes sur la page de consultation de graphiques. Appuyez sur *New Graph* pour ouvrir le fenêtre de création de graphique.

## Création de graphique

### Fenêtre de création de graphique

![Pop-up de création de graphique](images/analyseFonctionnelle/formCreateGraph.png)

> *Fenêtre de création de graphique*

Pour créer **votre** graphique, spécifiez un nom et un type.

Vous avez le choix entre 9 types de graphiques. Voici à quoi ils ressembleront [(images tirées du site Google Charts)](https://developers.google.com/chart/interactive/docs/gallery).

| Nom                         | Image                                                                 |
| --------------------------- | --------------------------------------------------------------------- |
| Graphique en aires          | ![AreaChart](images/analyseFonctionnelle/areaChart.png)               |
| Graphique en aires à étages | ![SteppedAreaChart](images/analyseFonctionnelle/steppedAreaChart.png) |
| Graphique en barres         | ![BarChart](images/analyseFonctionnelle/barChart.png)                 |
| Graphique en colonnes       | ![ColumnChart](images/analyseFonctionnelle/columnChart.png)           |
| Diagramme circulaire        | ![PieChart](images/analyseFonctionnelle/pieChart.png)                 |
| Diagramme en anneau         | ![DonutChart](images/analyseFonctionnelle/donutChart.png)             |
| Histogramme                 | ![Histogram](images/analyseFonctionnelle/histogram.png)               |
| Nuage de points             | ![SteppedAreaChart](images/analyseFonctionnelle/scatterChart.png)     |

Appuyez sur le bouton *Create* lorsque vous avez entré un nom et choisi le type pour créer votre graphique. Vous serez redirigé sur la page de modification de graphique.

Pour fermer la fenêtre, appuyez sur le bouton *Cancel* ou sur la croix en haut à droite.

### Création du graphique réussie

![Fenêtre création de graphique réussie](images/analyseFonctionnelle/PopupCreateGraphSuccess.png)

> *Fenêtre de création de graphique réussie*

Cette fenêtre s'affiche lorsque vous avez créé votre graphique pour vous indiquer qu'il s'est bien créé. Pour la fermer, appuyez sur *Ok* ou sur la croix en haut à droite.

## Modification du graphique

### Options du graphique

![Options du graphique](images/analyseFonctionnelle/formOptions.png)

> *Formulaire pour les options du graphique*

Lorsque vous avez créé votre graphique vous pouvez changer ses options via ce formulaire. Le premier champ permet de changer le nom du graphique et le deuxième son type. Lorsque vous avez terminé, appuyez sur le bouton vert pour appliquer les modifications.

### Axes du graphique

#### Création

![Formulaire pour créer les axes du graphique](images/analyseFonctionnelle/formAxes.png)

> *Formulaire pour créer les axes du graphique*

Pour créer l'axe x (les valeurs horizontales), spécifiez le nom et le type de l'axe, vous avez le choix entre trois types :

- string -> texte
- number -> nombre
- date -> date

Pour créer l'axe y (les valeurs verticales), spécifiez seulement un nom. Le type de l'axe y sera toujours un nombre, car vous aurez toujours une quantité sur la hauteur.

Lorsque vous avez entré toutes les informations, appuyez sur le bouton vert pour valider la création des axes.

#### Modification

![Formulaire pour modifier les axes du graphique](images/analyseFonctionnelle/formAxesCreer.png)

> *Formulaire pour modifier les axes du graphique*

Lorsque vous voulez modifier les axes du graphique, vous ne pouvez que changer le nom des axes.

Lorsque vous avez changé le nom des axes, appuyez sur le bouton vert.

### Données

#### Boutons

Il y a trois boutons principaux pour les données.

| Utilité                          | Image                                                                          |
| -------------------------------- | ------------------------------------------------------------------------------ |
| Bouton pour ajouter une donnée   | ![Bouton pour ajouter une donné](images/analyseFonctionnelle/btnAdd.png)       |
| Bouton pour modifier une donnée  | ![Bouton pour modifier une donnée](images/analyseFonctionnelle/btnModify.png)  |
| Bouton pour supprimer une donnée | ![Bouton pour supprimer une donnée](images/analyseFonctionnelle/btnDelete.png) |

#### Conteneurs de données

![Formulaire pour conteneurs de données](images/analyseFonctionnelle/formDataContainer.png)

> *Formulaire pour les conteneurs de données*

Un conteneur de données peut être défini comme une ligne dans un graphique linéaire ou une barre dans un graphique en barres.

Pour en créer un conteneur de données, donnez-lui un nom et appuyez sur le bouton d'ajout.

Pour modifier un conteneur de données, changez-lui son nom et appuyez sur le bouton de modification.

Pour supprimer un conteneur de données, appuyez sur sur le bouton de suppression. Sachez que si vous supprimez un conteneur de données, toutes ses données seront supprimées avec.

#### Données de conteneurs

![Formulaire pour les données des conteneur de données](images/analyseFonctionnelle/formData.png)

> *Formulaire pour ajouter, modifier et supprimer des données*

Pour ajouter des données, remplissez tous les champs.

En fonction du type du champ, l'affichage sera un peu différent :

- date : lorsque vous appuyez sur le champ, un sélecteur apparaît, ce qui vous permet de choisir une date.
- nombre : il y a des flèches pour changer le nombre
- texte : il n'y a rien de spécial

Le premier champ est la valeur sur l'axe x, qui sera commune à tous les conteneurs de données. Les champs suivants correspondent aux données de chaque conteneurs de données, sur l'axe y. Ce sont ces données qui sont différentes pour chaque conteneur.

Lorsque vous avez entré toutes les données, appuyez sur le bouton d'ajout.

Pour modifier des données, changez les valeurs existantes et appuyez sur le bouton modifier.

Pour supprimer des données, appuyez sur le bouton supprimer pour la ligne qui convient.

## Affichage du graphique

![Affichage du graphique](images/analyseFonctionnelle/graph.png)

> *Affichage du graphique*

Voilà comment devrait se présenter un graphique.

## Barre latérale sur la page de graphique

| Barre latérale ouverte                                     | Barre latérale fermée                                            |
| ---------------------------------------------------------- | ---------------------------------------------------------------- |
| ![Barre latérale](images/analyseFonctionnelle/sideBar.png) | ![Barre latérale](images/analyseFonctionnelle/sideBareClose.png) |

C'est la barre latérale que vous voyez lorsque vous êtes sur la page de création et de modification du graphique. Pour la fermer ou l'ouvrir, il suffit d'appuyer sur la flèche en bas.

Les différents boutons sont expliqués ci-dessous (le bouton *New Graph* a déjà été expliqué plus haut).

### Logo

Appuyez sur le logo *YOUR GRAPH* pour aller sur la page de modification du graphique.

### Ouvrir un graphique

Si vous êtes connecté, appuyez sur le bouton *Open Graph* pour accéder à la page de consultation de vos graphiques.

### Sauvegarder un graphique

Pour sauvegarder votre graphique appuyez sur le bouton *Save Graph*. Vous devez être connecté pour utiliser cette fonctionnalité. Tant que vous ne sauvegardez pas, les changements ne sont pas enregistrés. Si la sauvegarde a réussi, une fenêtre indique qu'elle a réussi, sinon la fenêtre indique qu'elle a raté.

![Pop-up sauvegarde réussie](images/analyseFonctionnelle/popupSaveGraphSuccess.png)

> *La sauvegarde a réussi*

![Pop-up sauvegarde réussie](images/analyseFonctionnelle/popupErrorSave.png)

> *La sauvegarde a échoué*

Pour fermer la fenêtre appuyez sur le bouton *Ok* ou sur la croix en haut à droite.

### Importation

Appuyez sur le bouton *Import* pour ouvrir la fenêtre d'importation.

![Pop-up formulaire importation données](images/analyseFonctionnelle/import.png)

> *Formulaire d'importation de données*

Choisissez un fichier de type `csv`. De plus, ce fichier doit suivre le format montré dans la capture d'écran. La première colonne contient les valeurs de l'axe x et les colonnes suivantes contiennent les valeurs de l'axe y pour les contenurs de données. Vous devez vous assurer que le type des données correspond à celui que vous avez défini dans le graphique.

Appuyez sur *Browse* pour choisir le fichier de données.

Appuyez sur *Submit* lorsque vous avez choisi le fichier pour importer vos données.

Appuyer sur *Cancel* ou sur la croix pour fermer la fenêtre.

Si l'importation réussi, une fenêtre vous l'indiquera, sinon une fenêtre vous indiquera qu'elle a raté.

![Pop-up importation de données réussie](images/analyseFonctionnelle/popupImportSuccess.PNG)

> *Importation réussie*

![Pop-up importation de données réussie](images/analyseFonctionnelle/popupImportFail.PNG)

> *Importation réussie*

Pour fermer la fenêtre appuyez sur *Ok* ou sur la croix en haut à droite.

### Exportation

Appuyez sur *Export* pour ouvrir ce menu déroulant.

![Menu déroulant exportation](images/analyseFonctionnelle/export.png)

> *Menu déroulant pour l'exportation*

Vous pouvez maintenant choisir entre deux types d'exportation. Si vous utilisez Firefox, vous aurez les fenêtres ci-dessous qui s'ouvrent, sinon le fichier sera directement téléchargé.

#### Exportation en CSV

![Exportation en CSV](images/analyseFonctionnelle/exportCsv.PNG)

> *Fenêtre de téléchargement des données du graphique au format CSV*

Appuyez sur le bouton *CSV*. Alors, cette fenêtre de téléchargement s'ouvre et vous propose d'enregistrer le fichier dans le format csv. Ce fichier contient les valeurs des conteneurs de données. Le format du fichier vous permet de le réimporter dans un autre graphique de YourGraph.

#### Exportation en SVG

![Exportation en SVG](images/analyseFonctionnelle/exportSvg.PNG)

> *Fenêtre de téléchargement du graphique SVG*

Appuyez sur le bouton *SVG*. Cela ouvre cette fenêtre qui vous permet de télécharger le fichier au format svg. C'est un format de dessin vectoriel. Utilisez cette option si vous voulez avoir la représentation visuelle de votre graphique.

## Visualisation des graphiques

![Visualisation des graphique de l'utilisateur](images/analyseFonctionnelle/userGraph.png)

> *Visualisation des graphiques de l'utilisateur*

### Ouverture d'un graphique

Sur cette page, vous voyez tous vos graphiques enregistrés dans la base de données. Vous pouvez voir leur nom ainsi que leur type.

Pour ouvrir votre graphique, appuyez sur le bouton avec le crayon. Attention, si vous n'avez pas sauvegardé voter graphique, vous perdrez toutes les données. Lorsque vous appuyez sur ce bouton, vous êtes redirigé sur la page de modification du graphique.

### Supprimer un graphique

Pour fermer un graphique, appuyez sur le bouton rouge avec la poubelle. Cela supprimera le graphique dans la base de données. Attention, vous ne pouvez pas revenir en arrière une fois votre graphique supprimé.

Lorsque vous avez appuyé sur le bouton de suppression, une fenêtre vous indique si la suppression a réussi ou non.

![Pop-up suppression réussie](images/analyseFonctionnelle/popupDeleteSuccess.png)

> *Fenêtre de suppression réussie*

![Pop-up de suppression échouée](images/analyseFonctionnelle/popupDeleteFail.png)

> *Fenêtre de suppression échouée*

## Bouton pour faire défiler vers le haut

![Bouton défilement vers le haut](images/analyseFonctionnelle/btnScrollTop.png)

> *Bouton de défilement vers le haut*

Il est possible que vous voyiez ce bouton apparaître si vous devez défiler énormément vers le bas. Appuyez dessus pour vous faire revenir en haut de la page.

## Page d'erreur

![Page d'erreur](images/analyseFonctionnelle/404Page.png)

> *Page d'erreur*

Vous arrrivez sur cette page lorsque vous avez entré une url incorrecte.

Vous avez alors deux possibilités :

- Appuyez sur le lien *Back to graph page* pour retourner à la page de modification de graphique.
- Appuyez sur *New Graph* pour créer un graphique.
