## Tables des matières <!-- omit in toc -->

- [1. Table des versions](#1-table-des-versions)
- [2. Introduction](#2-introduction)
- [3. Résumé du cahier des charges](#3-résumé-du-cahier-des-charges)
  - [3.1. Organisation](#31-organisation)
  - [3.2. Livrables](#32-livrables)
  - [3.3. Matériel et logiciels à disposition](#33-matériel-et-logiciels-à-disposition)
  - [3.4. Description de l'application](#34-description-de-lapplication)
  - [3.5. Points techniques évalués](#35-points-techniques-évalués)
- [4. Méthodologie](#4-méthodologie)
  - [4.1. S'informer](#41-sinformer)
  - [4.2. Planifier](#42-planifier)
  - [4.3. Décider](#43-décider)
  - [4.4. Réaliser](#44-réaliser)
  - [4.5. Contrôler](#45-contrôler)
  - [4.6. Évaluer](#46-évaluer)
- [5. Planification](#5-planification)
  - [5.1. Product Backlog](#51-product-backlog)
  - [5.2. Planning Prévisionnel](#52-planning-prévisionnel)
  - [5.3. Planning Effectif](#53-planning-effectif)
  - [5.4. Comparaison des plannings](#54-comparaison-des-plannings)
  - [5.5. Maquettes interface](#55-maquettes-interface)
- [6. Analyse Fonctionnelle](#6-analyse-fonctionnelle)
  - [6.1. Interface graphique](#61-interface-graphique)
    - [6.1.1. Inscription](#611-inscription)
    - [6.1.2. Connexion](#612-connexion)
    - [6.1.3. Utilisateur](#613-utilisateur)
      - [6.1.3.1. Utilsiateur non connecté](#6131-utilsiateur-non-connecté)
      - [6.1.3.2. Utilisateur connecté](#6132-utilisateur-connecté)
    - [6.1.4. Pop-up déconnexion](#614-pop-up-déconnexion)
    - [6.1.5. SideBar](#615-sidebar)
    - [6.1.6. Création de graphique](#616-création-de-graphique)
    - [6.1.7. Pop-up création de graphique](#617-pop-up-création-de-graphique)
    - [6.1.8. Options du graphique](#618-options-du-graphique)
    - [6.1.9. Axes du graphique](#619-axes-du-graphique)
    - [6.1.10. Données du graphique](#6110-données-du-graphique)
      - [6.1.10.1. Bouton ajouter](#61101-bouton-ajouter)
      - [6.1.10.2. Bouton modifier](#61102-bouton-modifier)
      - [6.1.10.3. Bouton supprimer](#61103-bouton-supprimer)
      - [6.1.10.4. Data Container](#61104-data-container)
      - [6.1.10.5. Données des data containers](#61105-données-des-data-containers)
    - [6.1.11. Graphique](#6111-graphique)
    - [6.1.12. Visualisation des graphiques](#6112-visualisation-des-graphiques)
    - [6.1.13. Sauvegarde du graphique](#6113-sauvegarde-du-graphique)
      - [6.1.13.1. Pop-up sauvegarde du graphique réussie](#61131-pop-up-sauvegarde-du-graphique-réussie)
      - [6.1.13.2. Pop-up sauvegarde du graphique ratée](#61132-pop-up-sauvegarde-du-graphique-ratée)
    - [6.1.14. Suppression d'un graphique](#6114-suppression-dun-graphique)
      - [6.1.14.1. Pop-up suppression réussie](#61141-pop-up-suppression-réussie)
    - [6.1.15. Pop-up suppression ratée](#6115-pop-up-suppression-ratée)
    - [6.1.16. Importation](#6116-importation)
      - [6.1.16.1. Importation réussie](#61161-importation-réussie)
      - [6.1.16.2. Importation échouée](#61162-importation-échouée)
    - [6.1.17. Exportation](#6117-exportation)
      - [6.1.17.1. CSV](#61171-csv)
    - [6.1.18. SVG](#6118-svg)
  - [6.2. Page d'erreur](#62-page-derreur)
  - [6.3. Fonctionnalités](#63-fonctionnalités)
    - [6.3.1. Visiteur](#631-visiteur)
    - [6.3.2. Utilisateur](#632-utilisateur)
  - [6.4. Description des fonctionnalités](#64-description-des-fonctionnalités)
    - [6.4.1. Création de compte](#641-création-de-compte)
    - [6.4.2. Connexion à un compte](#642-connexion-à-un-compte)
    - [6.4.3. Déconnexion](#643-déconnexion)
    - [6.4.4. Créer un graphique](#644-créer-un-graphique)
    - [6.4.5. Modifier un graphique](#645-modifier-un-graphique)
    - [6.4.6. Ajouter des axes](#646-ajouter-des-axes)
    - [6.4.7. Modifier des axes](#647-modifier-des-axes)
    - [6.4.8. Ajouter un data containers](#648-ajouter-un-data-containers)
    - [6.4.9. Modifier un data containers](#649-modifier-un-data-containers)
    - [6.4.10. Supprimer un data containers](#6410-supprimer-un-data-containers)
    - [6.4.11. Ajouter des données](#6411-ajouter-des-données)
    - [6.4.12. Modifier des données](#6412-modifier-des-données)
    - [6.4.13. Supprimer des données](#6413-supprimer-des-données)
    - [6.4.14. Importer des données](#6414-importer-des-données)
    - [6.4.15. Exporter des données](#6415-exporter-des-données)
    - [6.4.16. Sauvegarder le graphique dans la base de données](#6416-sauvegarder-le-graphique-dans-la-base-de-données)
    - [6.4.17. Visionner la liste des graphiques](#6417-visionner-la-liste-des-graphiques)
    - [6.4.18. Ouvrir un graphique enregistré](#6418-ouvrir-un-graphique-enregistré)
    - [6.4.19. Supprimer un graphique de la base de données](#6419-supprimer-un-graphique-de-la-base-de-données)
- [7. Analyse Organique](#7-analyse-organique)
  - [7.1. Technologies utilisées](#71-technologies-utilisées)
  - [7.2. Environnement](#72-environnement)
  - [7.3. Description de la base de données](#73-description-de-la-base-de-données)
    - [7.3.1. Modèle logique de données](#731-modèle-logique-de-données)
    - [7.3.2. Dictionnaire de données](#732-dictionnaire-de-données)
      - [7.3.2.1. users](#7321-users)
      - [7.3.2.2. charts](#7322-charts)
      - [7.3.2.3. chartsTypes](#7323-chartstypes)
      - [7.3.2.4. axes](#7324-axes)
      - [7.3.2.5. axesTypes](#7325-axestypes)
      - [7.3.2.6. dataContainers](#7326-datacontainers)
      - [7.3.2.7. yData](#7327-ydata)
      - [7.3.2.8. xData](#7328-xdata)
  - [7.4. Structure du projet](#74-structure-du-projet)
  - [7.5. Classes](#75-classes)
  - [7.6. Pages](#76-pages)
  - [7.7. Api interne](#77-api-interne)
  - [7.8. Librairies et outils externes](#78-librairies-et-outils-externes)
    - [7.8.1. Google Charts](#781-google-charts)
    - [7.8.2. GitLab](#782-gitlab)
    - [7.8.3. DBeaver](#783-dbeaver)
    - [7.8.4. Bootstrap](#784-bootstrap)
    - [7.8.5. StartBootstrap](#785-startbootstrap)
    - [7.8.6. Font Awesome](#786-font-awesome)
    - [7.8.7. DB Diagram](#787-db-diagram)
    - [7.8.8. Doxygen](#788-doxygen)
    - [7.8.9. MikTeX](#789-miktex)
    - [7.8.10. Minifier](#7810-minifier)
- [8. Tests](#8-tests)
  - [8.1. Environnement des tests](#81-environnement-des-tests)
  - [8.2. Plan de test](#82-plan-de-test)
  - [8.3. Rapports de test](#83-rapports-de-test)
    - [8.3.1. 18 mai 2021](#831-18-mai-2021)
    - [8.3.2. 20 mai 2021](#832-20-mai-2021)
- [9. Conclusion](#9-conclusion)
  - [9.1. Ce qui m'était demandé](#91-ce-qui-métait-demandé)
  - [9.2. Rappel des points techniques évalués](#92-rappel-des-points-techniques-évalués)
  - [9.3. Ce que j'ai fait](#93-ce-que-jai-fait)
  - [9.4. Problèmes rencontrés](#94-problèmes-rencontrés)
  - [9.5. Améliorations possibles](#95-améliorations-possibles)
  - [9.6. Bilan personnel](#96-bilan-personnel)
- [10. Bibliographie](#10-bibliographie)
- [11. Table des illustrations](#11-table-des-illustrations)
- [12. Glossaire](#12-glossaire)

## 1. Table des versions

| N° de version | Date       | Auteur                              | Changements apportés                           |
| ------------- | ---------- | ----------------------------------- | ---------------------------------------------- |
| 1.0           | 20.05.2021 | Ludovic Roux \<ludovic.rx@eduge.ch> | Version final du document pour le rendu du TPI |

## 2. Introduction

Ce document concerne la partie technique du projet. Il explique comment YourGraph a été réalisé et avec quels outils. Ce projet est réalisé lors du TPI (Travail Pratique Individuel) du 3 au 21 mai 2021. Ce travail marque la fin de mon CFC d'informatique en développement d'application.

YourGraph est un site web de création de graphiques.

## 3. Résumé du cahier des charges

*Ces informations sont tirées du document `Cahier des charges.pdf`*

### 3.1. Organisation

| Élève                                      | Maître d'apprentissage                                |
| ------------------------------------------ | ----------------------------------------------------- |
| Ludovic Roux <br /> \<ludovic.rx@eduge.ch> | Julien Aliprandi <br /> \<julien.aliprandi@edu.ge.ch> |

| Experts                                  |                                                     |
| ---------------------------------------- | --------------------------------------------------- |
| Yvan Poulin <br /> \<yvpoulin@gmail.com> | Francesco Foti <br /> \<francesco.foti@devinfo.net> |

### 3.2. Livrables

- Le projet entier avec les sources et un export des données
- Le planning réalisé lors du projet
- Un rapport technique de projet contenant le code source
- Un manuel utilisateur
- Un résumé d’une page sans image du projet
- Le journal de travail électronique

### 3.3. Matériel et logiciels à disposition

- PC standard école
- 2 écrans
- Windows 10
- Suite Office
- IDE (En l'occurence Visual Studio Code)

### 3.4. Description de l'application

YourGraph est un site web de création et gestion de diagrammes et graphiques.

On peut faire les opération suivantes :

1. Créer un graphique
2. Ajouter des données au graphique (nom du graphique, nom des axes, valeurs)
3. Importer des données pour un graphique
4. Choisir un type de graphique parmis plusieurs, au minimum 2 types différents sont requis
5. Exporter un graphique
6. Créer un utilisateur
7. Se connecter
8. Voir tous les graphiques de notre compte utilisateur
9. Supprimer un graphique
10. Modifier un graphique
11. Sauvegarder un graphique

### 3.5. Points techniques évalués

- A14. L’application est bien réalisée avec les outils demandés et elle est fonctionnelle
- A15. On peut créer un compte et se connecter au site
- A16. On peut créer un graphique et rentrer les données manuellement
- A17. Lorsqu’on est connecté, on peut voir des graphiques, les ouvrir et les modifier
- A18. On peut importer des données pour créer un graphique
- A19. Il y a différentes sortes de graphiques qui sont correctement enregistrés
- A20. Le programme a été testé suivant le protocole de test

## 4. Méthodologie

La méthodologie utilisée durant mon projet de TPI est la méthodologie en 6 étapes.

### 4.1. S'informer

Pour commencer, j'ai pris connaissance du cahier des charges pour comprendre tout ce que mon projet devait faire.

### 4.2. Planifier

Ensuite, j'ai planifié toutes les tâches que je devais faire. J'ai établi d'abord un product backlog et un plan de test. Ces différentes tâches représentent les actions qu'un utilisateur peut effectuer, c'est pour cela qu'on les appelle les *user story*. Pour chaque story, j'ai donné une valeur d'importance, ce qui aide à se rendre compte quelles tâches sont plus prioritaires.
Puis j'ai créé un planning prévisionnel en fonction de l'importance que j'ai donnée aux tâches et j'ai dû estimer le temps pour chacunes des ces tâches.
De plus, j'ai aussi utilisé le système d'issues de GitLab. Les issues correspondent en grande partie à chaque story de mon product backlog. ![Issues GitLab](images/gitLabIssue.png)
J'ai divisé les issues en 4 domaines : BackEnd, FrontEnd Documentation et Test.

### 4.3. Décider

Durant mon travail, j'ai dû prendre beaucoup de décisions en ce qui concerne la manière d'implémenter mon site. J'ai rapporté les décisions importantes dans mon journal de bord. De plus pour certaines décisions j'ai consulté mon professeur de TPI ou des camarades pour m'aider à trouver la meilleure solution.

### 4.4. Réaliser

La réalisation est la plus grande partie de mon travail. J'ai passé plus de temps à réaliser le fonctionnement du site plutôt que rédiger la documentation.

### 4.5. Contrôler

Plusieurs fois durant mon travail, j'ai effectué les tests de mon plan de test que j'ai rédigé le premier jour et que j'ai par la suite modifié en fonction des décisions prises.

### 4.6. Évaluer

À la fin de ce travail, j'ai rédigé une conclusion qui m'a permis de voir quelles ont été les points positifs de ce projet et les problèmes que j'ai rencontrés.

## 5. Planification

### 5.1. Product Backlog

\* -> secondaire

\*\* -> important

\*\*\* -> obligatoire

| Nom                   | S1 : Mise en place d'une base de données                                                                                          |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| Description           | Le projet doit contenir une base de données qui est décrite par un MLD et qui permet de contenir les informations des graphiques. |
| Critère d'acceptation | Test n°1 passe                                                                                                                    |
| Priorité              | \*\*\*                                                                                                                            |

| Nom                   | S2 : Création d'un compte                                                        |
| --------------------- | -------------------------------------------------------------------------------- |
| Description           | Un utilisateur peut créer un compte qui sera enregistré dans la base de données. |
| Critère d'acceptation | Tests n°2.1 à 2.3 passent                                                        |
| Priorité              | \*\*\*                                                                           |

| Nom                   | S3 : Connexion au compte                                                                                     |
| --------------------- | ------------------------------------------------------------------------------------------------------------ |
| Description           | Un utilisateur peut se connecter à son compte lorsqu'il entre le bon mot de passe et la bonne adresse e-mail. |
| Critère d'acceptation | Tests n°3.1 à 3.3 passent                                                                                    |
| Priorité              | \*\*\*                                                                                                       |

| Nom                   | S4 : Déconnexion                                 |
| --------------------- | ------------------------------------------------ |
| Description           | L'utilisateur peut se déconnecter de son compte. |
| Critère d'acceptation | Test n°4 passe                                   |
| Priorité              | \*\*                                             |

| Nom                   | S5 : Création d'un graphique                                                                            |
| --------------------- | ------------------------------------------------------------------------------------------------------- |
| Description           | Un utilisateur peut créer un graphique en choisissant le type de graphique de son choix et avec un nom. |
| Critère d'acceptation | Tests n°5.1 à 5.2 passent                                                                               |
| Priorité              | \*\*\*                                                                                                  |

| Nom                   | S6 : Modification d'un graphique                                                                                          |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| Description           | L'utilisateur peut ajouter des données à son graphique, il peut modifier son nom, ses axes et les données qu'il contient. |
| Critère d'acceptation | Tests n°6.1 à 6.10 passent                                                                                                |
| Priorité              | \*\*\*                                                                                                                    |

| Nom                   | S7 : Importation de données                                                                  |
| --------------------- | -------------------------------------------------------------------------------------------- |
| Description           | L'utilisateur peut importer un fichier au format csv pour importer des données au graphique. |
| Critère d'acceptation | Tests 7.1 à 7.3 passent                                                                      |
| Priorité              | \*\*\*                                                                                       |

| Nom                   | S8 : Exportation d'un graphique                                     |
| --------------------- | ------------------------------------------------------------------- |
| Description           | L'utilisateur peut exporter un graphique sous le format svg ou csv. |
| Critère d'acceptation | Tests n°8.1 à 8.3 passent                                           |
| Priorité              | \*\*                                                                |

| Nom                   | S9 : Suppression d'un graphique            |
| --------------------- | ------------------------------------------ |
| Description           | L'utilisateur peut supprimer un graphique. |
| Critère d'acceptation | Test n°9                                   |
| Priorité              | \*\*                                       |

| Nom                   | S10 : Consultation des graphiques                                                                                     |
| --------------------- | --------------------------------------------------------------------------------------------------------------------- |
| Description           | L'utilisateur peut consulter ses propres graphiques dans une page dédiée à cela et choisir d'ouvrir celui qu'il veut. |
| Critère d'acceptation | Tests n°10.1 à 10.3 passent                                                                                           |
| Priorité              | \*\*\*                                                                                                                |

| Nom                   | S11 : Sauvegarder un graphique dans la base de données                                                            |
| --------------------- | ----------------------------------------------------------------------------------------------------------------- |
| Description           | L'utilisateur peut choisir d'enregistrer son graphique seulement lorsqu'il est connecté, dans la base de données. |
| Critère d'acceptation | Tests n°11.1 à 11.2                                                                                               |
| Priorité              | \*\*\*                                                                                                            |

| Nom                   | S12 : Sauvegarder un graphique dans la session                                                         |
| --------------------- | ------------------------------------------------------------------------------------------------------ |
| Description           | Le graphique de l'utilisateur se sauvegarde dans la session tant qu'il n'a pas appuyé sur enregistrer. |
| Critère d'acceptation | Test n°12 passe                                                                                        |
| Priorité              | \*\*\*                                                                                                 |

| Nom                   | S13 : Affichage d'un graphique                                                                                                      |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| Description           | Le graphique de l'utilisateur, lorsqu'il a inséré des données, s'affiche correctement. Le graphique s'affiche selon le type choisi. |
| Critère d'acceptation | Test n°14 passe                                                                                                                     |
| Priorité              | \*\*\*                                                                                                                              |

| Nom                   | S14 : Mise en place d'un git                                                                                                                  |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| Description           | Je peux utiliser un Git qui me permet de gérer mes sources. Des issues et des labels sont mis en place pour la planification des tâches.      |
| Critère d'acceptation | Un dépôt GitLab a été crée et le lien est indiqué dans la documentation pour aller le consulter. On peut consulter la progression des issues. |
| Priorité              | \*\*\*                                                                                                                                        |

### 5.2. Planning Prévisionnel

![Planning Prévisionnel](images/planningPrevisionnel.png)

> *Figure n°1 : Planning prévisionnel*

### 5.3. Planning Effectif

![Planning Effectif](images/planningEffectif.png)

> *Figure n°2 : Planning effectif*

### 5.4. Comparaison des plannings

En comparant les plannings, je vois que j'avais prévu environ 9 heures de plus de documentation. En réalité j'ai eu besoin de plus de temps de code. On voit aussi que l'ordre des tâches a changé. J'ai finalement fait d'abord la sauvegarde du graphique dans la session, pour pouvoir après enregistrer les données dans la base de données. On voit aussi qu'entre le cinquième et le huitième jour, je me suis éparpillé. J'ai avancé parfois en parallèle sur la sauvegarde dans la session et sur d'autre tâches. J'ai fait plus de debugging et de tests et j'ai pris moins de temps pour le manuel utilisateur. Par contre, j'ai pris moins de temps pour faire mon front-end, car j'ai trouvé un template qui m'a beaucoup aidé.

### 5.5. Maquettes interface

![Maquette de la page d'index](images/index.HEIC)

> *Figure n°3 : Maquette de la page d'index*

![Maquette de la page de visualisation des graphiques](images/graphs.HEIC)

> *Figure n°4 : Maquette de la page de visualisation des graphiques*

![Maquette de la page de connexion](images/signin.HEIC)

> *Figure n°5 : Maquette de la page de connexion*

![Maquette de la page de création de compte](images/register.HEIC)

> *Figure n°6 : Maquette de la page de création de compte*

## 6. Analyse Fonctionnelle

### 6.1. Interface graphique

#### 6.1.1. Inscription

Formulaire permettant de créer un compte.

Le lien *Already have an account? Login!* redirige l'utilisateur sur login.php.

![Inscription](images/analyseFonctionnelle/formulaireCreateAccount.png)

> *Figure n°7 : Inscription*

#### 6.1.2. Connexion

Formulaire permettant de se connecter.

Le lien *Create an Account!* redirige l'utilisateur sur register.php.

![Connexion](images/analyseFonctionnelle/formulaireLogin.png)

> *Figure n°8 : Connexion*

#### 6.1.3. Utilisateur

##### 6.1.3.1. Utilsiateur non connecté

Bouton sur la barre du haut qui redirige sur le formulaire de connexion.

![Utilisateur non connecté](images/analyseFonctionnelle/navigationNonAuthentifiepng.png)

> *Figure n°9 : Bouton pour se connecter*

##### 6.1.3.2. Utilisateur connecté

Lorsqu'on appuie sur son profile, un bouton pour se déconnecter apparaît.

![Utilisateur connecté](images/analyseFonctionnelle/navigationAuthentifiepng.png)

> *Figure n°10 : Bouton pour se déconnecter*

#### 6.1.4. Pop-up déconnexion

![Pop-up déconnexion](images/analyseFonctionnelle/popuLeave.png)

> *Figure n°11 : Pop-up pour se déconnecter*

#### 6.1.5. SideBar

- icone *YourGraph* : redirige sur la page d'index

- page de visualisation des graphiques (graphs.php)
  - on n'a pour option que de créer un graphique

- page d'index, où l'on crée le graphique
  - Appuyer sur l'icone permet d'aller sur la page du graphique courant
  - *New Graph* ouvre la pop-up pour créer le graphique
  - *Open Graph* redirige l'utilsateur sur graphs.php
  - *Save Graph* sauve le graphique de l'utilisateur
  - *Import* ouvre la pop-up pour importer des données
  - *Export* bouton pour exporter, ouvre un menu déroulant
  - Le bouton avec la flèche rétrécit ou agrandit la sidebar

| SideBar graphs.php                                                   | SieBar index.php                                               | SideBar fermée                                                       |
| -------------------------------------------------------------------- | -------------------------------------------------------------- | -------------------------------------------------------------------- |
| ![Pop-up déconnexion](images/analyseFonctionnelle/sideBarGraphs.png) | ![Pop-up déconnexion](images/analyseFonctionnelle/sideBar.png) | ![Pop-up déconnexion](images/analyseFonctionnelle/sideBareClose.png) |
| *Figure n°12 : SideBar graphs.php*                                       | *Figure n°13 : SideBar index.php*                                  | *Figure n°14 : SideBar fermée*                                           |

#### 6.1.6. Création de graphique

Formulaire permettant de créer un nouveau graphique.

![Pop-up création de graphique](images/analyseFonctionnelle/formCreateGraph.png)

> *Figure n°15 : Pop-up pour créer un graphique*

#### 6.1.7. Pop-up création de graphique

Pop-up s'affichant lorsque le graphique est bien créé.

![Pop-up création de graphique réussie](images/analyseFonctionnelle/PopupCreateGraphSuccess.png)

> *Figure n°16 : Pop-up de création de graphique réussie*

#### 6.1.8. Options du graphique

Formulaire permettant à l'utilisateur de changer le nom et le type du graphique.

![Formulaire options du graphique](images/analyseFonctionnelle/formOptions.png)

> *Figure n°17 : Formulaire options du graphique*

#### 6.1.9. Axes du graphique

Formulaire permettant de gérer les axes du graphique.

![Formulaire axes du graphique](images/analyseFonctionnelle/formAxes.png)

> *Figure n°18 : Formulaire axes du graphique*

![Formulaire axes du graphique axes ajoutés](images/analyseFonctionnelle/formAxesCreer.png)

> *Figure n°19 : Formulaire axes lorsqu'ils ont été ajoutés*

#### 6.1.10. Données du graphique

##### 6.1.10.1. Bouton ajouter

Ce bouton est utilisé lorsqu'on veut ajouter une donnée.

![Bouton pour ajouter une donnée](images/analyseFonctionnelle/btnAdd.png)

> *Figure n°20 : Bouton ajouter une donnée*

##### 6.1.10.2. Bouton modifier

Ce bouton est utilisé lorsqu'on veut modifier une donnée.

![Bouton pour modifier une donnée](images/analyseFonctionnelle/btnModify.png)

> *Figure n°21 : Bouton modifier une donnée*

##### 6.1.10.3. Bouton supprimer

Ce bouton est utilisé lorsqu'on veut supprimer une donnée.

![Bouton pour supprimer une donnée](images/analyseFonctionnelle/btnDelete.png)

> *Figure n°22 : Bouton supprimer une donnée*

##### 6.1.10.4. Data Container

Formulaire permettant de gérer les Data container. C'est aussi à cet endroit que l'on peut voir la liste des data containers.

![Formulaire data container graphique](images/analyseFonctionnelle/formDataContainer.png)

> *Figure n°23 : Formulaire data containers du graphique*

##### 6.1.10.5. Données des data containers

Formulaire permettant de gérer les données des data containers. C'est aussi à cet endroit que l'on peut voir la liste des données.

![Formulaire données des data containers](images/analyseFonctionnelle/formData.png)

> *Figure n°24 : Formulaire des données des data containers*

#### 6.1.11. Graphique

Affichage du graphique.

![Affichage du graphique](images/analyseFonctionnelle/graph.png)

> *Figure n°25 : Affichage du graphique*

#### 6.1.12. Visualisation des graphiques

Affichage des graphiques de l'utilisateur.

![Affichage des graphiques utilisateur](images/analyseFonctionnelle/userGraph.png)

> *Figure n°26 : Affichage des graphiques de l'utilisateur*

#### 6.1.13. Sauvegarde du graphique

##### 6.1.13.1. Pop-up sauvegarde du graphique réussie

Pop-up qui s'affiche lorsque la sauvegarde du graphique a réussie.

![Pop-up sauvegarde réussie](images/analyseFonctionnelle/popupSaveGraphSuccess.png)

> *Figure n°27 : Pop-up graphique sauvegarde réussie*

##### 6.1.13.2. Pop-up sauvegarde du graphique ratée

Pop-up qui s'affiche lorsque la sauvegarde du graphique a échoué.

![Pop-up sauvegarde ratée](images/analyseFonctionnelle/popupErrorSave.png)

> *Figure n°28 : Pop-up graphique sauvegarde ratée*

#### 6.1.14. Suppression d'un graphique

##### 6.1.14.1. Pop-up suppression réussie

Pop-up qui s'affiche lorsque la suppression du graphique a réussi.

![Pop-up suppression réussie](images/analyseFonctionnelle/popupDeleteSuccess.png)

> *Figure n°29 : Pop-up graphique suppression réussie*

#### 6.1.15. Pop-up suppression ratée

Pop-up qui s'affiche lorsque la suppression du graphique a raté.

![Pop-up suppression ratée](images/analyseFonctionnelle/popupDeleteFail.png)

> *Figure n°30 : Pop-up graphique suppression ratée*

#### 6.1.16. Importation

Pop-up qui s'affiche lorsqu'on appuie sur le bouton d'import.

![Pop-up importation données](images/analyseFonctionnelle/import.png)

> *Figure n°31 : Pop-up importation de données*

##### 6.1.16.1. Importation réussie

Pop-up qui s'affiche lorsque l'importation a réussi.

![Pop-up importation données réussie](images/analyseFonctionnelle/popupImportSuccess.PNG)

> *Figure n°32 : Pop-up d'importation réussie*

##### 6.1.16.2. Importation échouée

Pop-up qui s'affiche lorsque l'importation a raté.

![Pop-up importation données échouée](images/analyseFonctionnelle/popupImportFail.PNG)

> *Figure n°33 : Pop-up d'importation échouée*

#### 6.1.17. Exportation

Menu qui s'affiche lorsqu'on appuie sur le bouton d'export.

![Menu exportation de données](images/analyseFonctionnelle/export.png)

> *Figure n°34 : Pop-up exportation de données*

##### 6.1.17.1. CSV

![Exportation en CSV](images/analyseFonctionnelle/exportCsv.PNG)

> *Figure n°35 : Fenêtre de téléchargement des données du graphique au format CSV*

Cette fenêtre s'ouvre lorsque l'utilisateur appuie sur le bouton *CSV*. *(seulement les utilisateurs Firefox)*

#### 6.1.18. SVG

![Exportation en SVG](images/analyseFonctionnelle/exportSvg.PNG)

> *Figure n°36 : Fenêtre de téléchargement du graphique SVG*

Cette fenêtre s'ouvre lorque l'utilisateur appuie sur le bouton *SVG*. *(seulement les utilisateurs Firefox)*

### 6.2. Page d'erreur

![Page d'erreur](images/analyseFonctionnelle/404Page.png)

> *Figure n°37 : Page d'erreur*

L'utilisateur arrive sur cette page lorsqu'il entre une url incorrect.

Il a alors deux possibilités :

- Appuyer sur le lien *Back to graph page* pour retourner à la page de modification de graphique.
- Appuyer sur *New Graph* pour créer un graphique.

### 6.3. Fonctionnalités

#### 6.3.1. Visiteur

Un visiteur peut :

- Créer un compte
- Se connecter
- Créer un graphique
- Modifier un graphique
- Ajouter des axes au graphique
- Modifier les axes du graphique
- Ajouter des data containers
- Modifier des data containers
- Supprimer des data containers
- Ajouter des données au graphique
- Modifier des données du graphique
- Supprimer des données du graphique
- Importer des données
- Exporter un graphique sous format csv ou svg

#### 6.3.2. Utilisateur

Un utilisateur est un visiteur connecté :

Un utilisateur peut :

- Créer un graphique
- Modifier un graphique
- Ajouter des axes au graphique
- Modifier les axes du graphique
- Ajouter des data containers
- Modifier des data containers
- Supprimer des data containers
- Ajouter des données au graphique
- Modifier des données du graphique
- Supprimer des données du graphique
- Sauvegarder le graphique dans la base de données
- Visionner la liste de ses graphiques
- Ouvrir un graphique enregistré
- Supprimer un graphique de la base de données
- Importer des données
- Exporter un graphique sous format csv ou svg
- Se déconnecter

### 6.4. Description des fonctionnalités

Chaque fonctionnalité correspond à une *interface* et a un *prérequis*. On peut retrouver les interfaces mentionnées ici à la section [6.1 Interface graphique](#61-interface-graphique)

#### 6.4.1. Création de compte

**Prérequis :** Aucun

**Interfaces :**  Inscription

Pour créer un compte, le visiteur doit renseigner :

- Son prénom
- Son nom de famille
- Son adresse e-mail
  - Cette adresse ne doit pas déjà appartenir à un compte, elle est donc unique
- Un mot de passe
  - Le mot de passe doit être répété deux fois pour s'assurer que le visiteur ne fait pas d'erreur de frappe

Lorsque tous ces champs sont remplis correctement et que le visiteur appuie sur le bouton, il est redirigé sur la page de connexion.

Le mot de passe est hashé avec la méthode `password_hash($password, PASSWORD_BCRYPT)`.

#### 6.4.2. Connexion à un compte

**Prérequis :** Création de compte

**Interfaces :** Utilisateur non connecté, Connexion

Pour se connecter à son compte, le visiteur doit renseigner :

- Son adresse e-mail
- Son mot de passe

Lorsque les deux champs sont valides et correspondent aux informations de la base de données, l'utilisateur est connecté et redirigé sur la page de visualisation de ses graphiques.

#### 6.4.3. Déconnexion

**Prérequis :** Connexion à un compte

**Interfaces :** Utilisateur connecté, Pop-up Déconnexion

Lorsque l'utilisateur appuie sur son profile, un menu s'affiche. Sur ce menu il peut appuyer sur logout. Cela affiche une pop-up de déconnexion. Si l'utilisateur appuie sur *Logout*, il est déconnecté et redirigé sur la page de connexion.

#### 6.4.4. Créer un graphique

**Prérequis :** Aucun

**Interfaces :** SideBar, Créer un graphique, Pop-up création de graphique

Pour afficher la pop-up, l'utilisateur doit appuyer sur *New Graph*.

L'utilisateur ou le visiteur doit renseigner :

- un nom à son graphique
- un type de graphique

| Nom                         | Image                                                                                                              |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| Graphique en aires          | ![AreaChart](images/analyseFonctionnelle/areaChart.png) <br> *Figure n°38 : Graphique en aires*                        |
| Graphique en aires à étages | ![SteppedAreaChart](images/analyseFonctionnelle/steppedAreaChart.png) <br> *Figure n°39 : Graphique en aires à étages* |
| Graphique en barres         | ![BarChart](images/analyseFonctionnelle/barChart.png) <br> *Figure n°40 : Graphique en barres*                         |
| Graphique en colonnes       | ![ColumnChart](images/analyseFonctionnelle/columnChart.png) <br> *Figure n°41 : Graphique en colonnes*                 |
| Diagramme circulaire        | ![PieChart](images/analyseFonctionnelle/pieChart.png) <br> *Figure n°42 : Diagramme circulaire*                        |
| Diagramme en anneau         | ![DonutChart](images/analyseFonctionnelle/donutChart.png) <br> *Figure n°43 : Diagramm en anneau*                      |
| Histogramme                 | ![Histogram](images/analyseFonctionnelle/histogram.png) <br> *Figure n°44 : Histogramme*                               |
| Nuage de points             | ![SteppedAreaChart](images/analyseFonctionnelle/scatterChart.png) <br> *Figure n°45 : Nuage de points*                 |

(*les images sont tirées de la [documentation de google chart](https://developers.google.com/chart/interactive/docs/gallery)*)

Il faut savoir que pour un diagramme circulaire ou un un diagramme en anneau, s'il y a plusieurs data containers, seul le premier sera affiché.

Lorsqu'il a correctement rentré les informations, il est redirigé sur la page de graphique et une pop-up s'affiche lui indiquant que son graphique a bien été créé.

#### 6.4.5. Modifier un graphique

**Prérequis :** Créer un graphique

**Interfaces :** Options du graphique

L'utilisateur ou le visiteur doit renseigner :

- Le nom de du graphique
- Le type du graphique

Lorsqu'il appuie sur le bouton pour valider, le nom et le type du graphique sont changés.

#### 6.4.6. Ajouter des axes

**Prérequis :** Créer un graphique

**Interfaces :** Axes du graphique

L'utilisateur ou le visiteur doit renseigner :

- Le nom de l'axe X ainsi que son type de données, il peut choisir entre un nombre, une date et un texte
- Le nom de l'axe Y, son type sera toujours un nombre

Lorsqu'il appuie sur le bouton, il est redirigé sur la même page et peut ajouter des données.

#### 6.4.7. Modifier des axes

**Prérequis :** Ajouter des axes

**Interfaces :** Axes du graphique

L'utilisateur ou le visiteur doit renseigner :

- Le nom de l'axe X ainsi que son type de données
- Le nom de l'axe Y, son type sera toujours un nombre

Lorsqu'il appuie sur le bouton, il est redirigé sur la même page et ces axes ont été mis à jour.

#### 6.4.8. Ajouter un data containers

**Prérequis :** Ajouter des axes

**Interfaces :** Data Container, Bouton ajouter

L'utilisateur ou le visiteur doit renseigner :

- Le nom du data container

Lorsqu'il appuie sur le bouton avec le plus, l'utilisateur est redirigé sur la même page et un data container est ajouté dans la session.

#### 6.4.9. Modifier un data containers

**Prérequis :** Ajouter un data container

**Interfaces :** Data Container, Bouton modifier

Lorsqu'il appuie sur le bouton avec le crayon, l'utilisateur est redirigé sur la même page et un data container est modifié dans la session.

#### 6.4.10. Supprimer un data containers

**Prérequis :** Ajouter un data container

**Interfaces :** Data Container, Bouton supprimer

Lorsqu'il appuie sur le bouton rouge, l'utilisateur est redirigé sur la même page et un data container est supprimé dans la session.

#### 6.4.11. Ajouter des données

**Prérequis :** Ajouter un data container

**Interfaces :** Données des data containers, Bouton ajouter

L'utilisateur ou le visiteur doit renseigner :

- La valeur de l'axe X.
- Les valeurs des différents data containers du graphique pour l'axe Y.

Lorsqu'il appuie sur le bouton ajouter, il est redirigé sur la même page, les données ont été ajoutées en session et on peut les voir sur le graphique.

#### 6.4.12. Modifier des données

**Prérequis :** Ajouter des données

**Interfaces :** Données des data containers, Bouton modifier

L'utilisateur ou le visiteur doit renseigner :

- La valeur de l'axe X.
- Les valeurs des différents data containers du graphique pour l'axe Y.

Lorsqu'il appuie sur le bouton avec le crayon, la donnée est modifiée en session.

#### 6.4.13. Supprimer des données

**Prérequis :** Ajouter des données

**Interfaces :** Données des data containers, Bouton supprimer

Lorsque l'utilisateur ou le visiteur, appuie sur le bouton rouge, la donnée est supprimée dans la session.

#### 6.4.14. Importer des données

**Prérequis :** Ajouter des axes

**Interfaces :** SideBar, Importation, Pop-up importation réussie, Pop-up importation ratée

Lorsque l'utilisateur ou le visiteur appuie sur le bouton d'import, une pop-up s'affiche qui permet de sélectionner un fichier csv. De plus, des explications indiquent à l'utilisateur la forme que son fichier csv doit avoir.

Après cela, si le fichier est correctement formé, les données s'ajoutent au graphique, auquel cas une pop-up lui indique que ses données se sont bien ajoutées au graphique. Dans le cas où il y a une erreur, une pop-up lui indique qu'une erreur est survenue.

#### 6.4.15. Exporter des données

**Prérequis :** Ajouter des données

**Interfaces :** SideBar, Exportation, Exportation CSV, Exportation SVG

Lorsque l'utilisateur ou le visiteur appuie sur le bouton d'export, il a deux options, dans les deux cas, le fichier s'enregistre dans le dossier de téléchargement du navigateur :

- Exporter en CSV
  - nom : *data.csv*
  - contenu : données du graphique, formatées selon le format accepté par *YourGraph*
- Exporter en SVG
  - nom : *graph.svg*
  - contenu : représentation vectorielle du graphique

#### 6.4.16. Sauvegarder le graphique dans la base de données

**Prérequis :** Ajouter des données

**Interfaces :** SideBar, Pop-up sauvegarde réussie, Pop-up sauvegarde ratée

Lorsque le visiteur appuie sur le bouton pour sauver le graphique, il est sauvegardé en base de données. Si la sauvegarde a fonctionné, une pop-up s'affiche lui indiquant que l'opération a fonctionnée. Dans le cas où il y a eu une erreur, une pop-up s'affiche lui indiquant qu'une erreur est survenue.

Lorsqu'on sauvegarde un graphique, on supprime l'ancienne version et on réinsert le graphique en entier. C'est pour cela que des transactions sont utilisées. Cela permet de faire un rollback en cas d'erreur et de ne pas perdre les données qu'on aurait supprimé.

#### 6.4.17. Visionner la liste des graphiques

**Prérequis :** Sauvegarder la graphique dans la base de données

**Interfaces :** Visionner les graphiques, SideBar

Pour accéder à la page de visualisation de ses graphiques, l'utilisateur peut appuyer sur le bouton *Open graph* de la side bar. Ici, il a la liste de tous ses graphiques enregistrés dans la base de données.

#### 6.4.18. Ouvrir un graphique enregistré

**Prérequis :** Visionner la liste des graphiques

**Interfaces :** Visionner les graphiques

Lorsque l'utilisateur appuie sur le bouton avec le crayon, le graphique est récupéré dans la base de données, puis stocké en session, alors l'utilisateur est redirigé sur la page d'index.

#### 6.4.19. Supprimer un graphique de la base de données

**Prérequis :** Visionner la liste des graphiques

**Interfaces :** Visionner les graphiques, Pop-up suppression réussie, Pop-up suppression ratée

Lorsque l'utilisateur appuie sur le bouton rouge, le graphique est supprimé. Si la suppression est réussie, la pop-up indique que la suppression a réussi, sinon la pop-up indique que la suppression a raté.

## 7. Analyse Organique

### 7.1. Technologies utilisées

- HTML
- PHP
- JavaScript
- CSS
- Javascript
- MariaDB
- Bootstrap
- Google Charts

### 7.2. Environnement

J'ai installé un sous-système linux avec un Debian 10 dessus. Sur le Debian, j'ai installé Apache, la version 2.4, qui fait office de serveur web et j'ai installé MariaDB pour le SGBD (Système de Gestion de base de données). Cela me permet de travailler sur un environnement natif, car Apache et MariaDB sont faits pour Linux.

J'ai également installé php 7.3.27-1~deb10u1 ainsi que X-Debug. X-Debug permet de débugger le php.

J'ai utilisé Visual Sutdio Code pour mon IDE. Je l'ai utilisé pour la partie code, mais je l'ai également utilisé pour la documentation, car je l'ai faite en markdown.

Pour m'occuper de la base de données, j'ai utilisé DBeaver. Il prend totalement en charge MariaDB contrairement à MysqlWorkbench, c'est pour cela que je l'ai choisi.

### 7.3. Description de la base de données

L'encodage par défaut de la base de données est utf8mb4. Elle utilise le moteur InnoDB.

#### 7.3.1. Modèle logique de données

La table xData contient les valeurs sur l'axe horizontal et yData contient les valeurs sur l'axe vertical si l'on prend un graphique linéaire. La valeur xData appartient à plusieurs yData. Par exemple, si on a plusieurs lignes sur le graphique, chaque ligne aura les mêmes valeurs sur l'axe horizontal (xData) mais auront des valeurs différentes sur l'axe vertical (yData).

![Modèle conceptuel de données](images/finalDB.png)

> *Figure n°46 : Modèle conceptuel de données*

#### 7.3.2. Dictionnaire de données

##### 7.3.2.1. users

| Colonne           | Type         | Null | Défaut |
| ----------------- | ------------ | ---- | ------ |
| idUser (Primaire) | int(11)      | Non  |        |
| firstName         | varchar(150) | Non  |        |
| lastName          | varchar(150) | Non  |        |
| email             | varchar(150) | Non  |        |
| password          | varchar(255) | Non  |        |

| Nom de l'index | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| -------------- | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY        | BTREE        | Oui       | Non      | Oui    | 1           |
| users_UN_email | BTREE        | Oui       | Non      | Oui    | 1           |

##### 7.3.2.2. charts

| Colonne            | Type         | Null | Défaut |
| ------------------ | ------------ | ---- | ------ |
| idChart (Primaire) | int(11)      | Non  |        |
| chartName          | varchar(255) | Non  |        |
| idChartType        | int(11)      | Non  |        |
| idUser             | int(11)      | Non  |        |

| Nom de l'index        | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| --------------------- | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY               | BTREE        | Oui       | Non      | Oui    | 2           |
| charts_FK_idChartType | BTREE        | Oui       | Non      | Non    | 2           |
| charts_FK_idUser      | BTREE        | Oui       | Non      | Non    | 2           |

##### 7.3.2.3. chartsTypes

| Colonne                | Type         | Null | Défaut |
| ---------------------- | ------------ | ---- | ------ |
| idChartType (Primaire) | int(11)      | Non  |        |
| chartTypeName          | varchar(100) | Non  |        |

| Nom de l'index | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| -------------- | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY        | BTREE        | Oui       | Non      | Oui    | 10          |

##### 7.3.2.4. axes

| Colonne           | Type         | Null | Défaut       |
| ----------------- | ------------ | ---- | ------------ |
| idAxis (Primaire) | int(11)      | Non  |              |
| axisName          | varchar(150) | Non  | 'Empty Axis' |
| axisLetter        | varchar(1)   | Non  | 'X'          |
| idAxisType        | int(11)      | Oui  |              |
| idChart           | int(11)      | Non  |              |

| Nom de l'index     | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| ------------------ | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY            | BTREE        | Oui       | Non      | Oui    | 4           |
| axes_FK_idAxisType | BTREE        | Oui       | Oui      | Non    | 4           |
| axes_FK_idChart    | BTREE        | Oui       | Non      | Non    | 4           |

##### 7.3.2.5. axesTypes

| Colonne               | Type         | Null | Défaut |
| --------------------- | ------------ | ---- | ------ |
| idAxisType (Primaire) | int(11)      | Non  |        |
| axisTypeName          | varchar(100) | Non  |        |

| Nom de l'index            | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| ------------------------- | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY                   | BTREE        | Oui       | Non      | Oui    | 3           |
| axesTypes_UN_axisTypeName | BTREE        | Oui       | Non      | Oui    | 3           |

##### 7.3.2.6. dataContainers

| Colonne                    | Type         | Null | Défaut |
| -------------------------- | ------------ | ---- | ------ |
| idDataContainer (Primaire) | int(11)      | Non  |        |
| dataContainerName          | varchar(100) | Non  |        |
| idChart                    | int(11)      | Non  |        |

| Nom de l'index | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| -------------- | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY        | BTREE        | Oui       | Non      | Oui    | 3           |
| lines_FK       | BTREE        | Oui       | Non      | Non    | 3           |

##### 7.3.2.7. yData

| Colonne            | Type    | Null | Défaut |
| ------------------ | ------- | ---- | ------ |
| idYData (Primaire) | int(11) | Non  |        |
| value              | float   | Non  |        |
| idDataContainer    | int(11) | Non  |        |
| idXData            | int(11) | Non  |        |

| Nom de l'index           | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| ------------------------ | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY                  | BTREE        | Oui       | Non      | Oui    | 13          |
| yData_FK_idDataContainer | BTREE        | Oui       | Non      | Non    | 13          |
| yData_FK_idXData         | BTREE        | Oui       | Non      | Non    | 13          |

##### 7.3.2.8. xData

| Colonne            | Type         | Null | Défaut |
| ------------------ | ------------ | ---- | ------ |
| idXData (Primaire) | int(11)      | Non  |        |
| value              | varchar(150) | Non  |        |

| Nom de l'index | Type d'index | Ascendant | Nullable | Unique | Cardinalité |
| -------------- | ------------ | --------- | -------- | ------ | ----------- |
| PRIMARY        | BTREE        | Oui       | Non      | Oui    | 23          |

### 7.4. Structure du projet

Toute les fonctions, classes et pages php du projet sont détaillées dans le fichier [Generated Doc](GeneratedDoc.pdf).

```Bash
.
├── api
│   ├── delete.php
│   ├── exportCSV.php
│   ├── import.php
│   ├── logout.php
│   ├── open.php
│   └── save.php
├── .htaccess
├── 404.php
├── graphs.php
├── index.php
├── login.php
├── register.php
└── res
    ├── assets
    ├── css
    ├── export
    ├── import
    ├── js
    ├── php
    │   ├── container
    │   ├── content
    │   └── database
    └── vendor
```

- **./** la racine contient les pages du site
- **./api** contient les fichiers d'api interne
- **./res** contient les fichiers de ressources
  - **./res/assets** contient les images
  - **./res/css** contient les fichiers css du template
  - **./res/export** contient les fichiers qui sont enregistrés lors de l'export en csv
  - **./res/import** contient les fichiers csv que l'utilisateur importe, ils sont supprimés lorsque l'importation a terminé
  - **./res/js** contient les fichiers js du template et des classes js
  - **./res/php** contient tous les fichiers php
    - **./res/php/container** contient les classes container. Une classe container stocke sous forme d'objet une entrée dans la base de données. Cela permet d'utiliser des objets plutôt que des tableaux avec les index de la base de données. Alors quand je change par exemple le nom d'un champ dans la base de données, je dois changer mon code seulement dans mes fonctions de la base de données. Ces classes contiennent des getter/setter pour éviter d'interagir directement avec les variables de classe.
    - **./res/php/content** contient les fichiers php à inclure pour l'HTML
    - **./res/php/database** contient les classes qui interagissent directement avec la base de données
  - *./res/vendor* fichiers générés qui permettent notamment le fonctionnement de bootstrap, fontawesome et jquery

### 7.5. Classes

J'ai décidé de préfixer mes classes par la lettre *L* pour savoir que c'est moi qui les ai créées. Les classes se trouvent dans les sous-dossiers du dossier **./res**.

- **./res/js/LChartJS.js** classe qui utilise google charts et qui permet de dessiner le graphique
- **./res/js/LToolsJS.js** classe qui contient les fonctions js du projet
- **./res/php/LSessions.php** classe qui gère la session, ce qui m'évite d'utiliser directement la session dans mon code. Cette classe utilise le pattern singleton. Sachant que le `session_start()` se trouve dans le constructeur, il ne s'effectue alors qu'une seule fois, car on n'instancie qu'une seule fois l'objet.
- **./res/php/LTools.php** classe qui contient les fonctions générales du projet
- **./res/php/LToolsFilter.php** classe qui contient toutes les fonctions de filtre
- **./res/php/LToolsHTML.php** classe qui contient les fonctions qui retourne de l'HTML
- **./res/php/LToolsHTMLModal.php** classe qui contient les fonctions pour les modals
- **./res/php/container/LAxis.php** classe qui encapsule un axe d'un graphique
- **./res/php/container/LAxisLetter.php** classe abstraite, contient les lettres des axes
- **./res/php/container/LAxisType.php** classe qui encapsule le type d'un graphique
- **./res/php/container/LChart.php** classe qui encapsule un graphique de la base de données
- **./res/php/container/LChartType.php** classe qui encapsule le type d'un graphique
- **./res/php/container/LDataContainer.php** classe qui encapsule un data container
- **./res/php/container/LPoint.php** classe qui encapsule un point, il a une valeur x et y
- **./res/php/container/LType.php** classe abstraite qui définit un type, *LAxisType* et *LChartType* en hérite
- **./res/php/container/LUser.php** classe qui encapsule un utilisateur du site
- **./res/php/database/EDatabase.php** classe qui encapsule la connexion à la base de données. Elle utilise le pattern singleton.
- **./res/php/database/LAxisDB.php** classe qui fait les requêtes sur la table axis de la base de données
- **./res/php/database/LAxisTypeDB.php** classe qui fait les requêtes sur le type d'un axe
- **./res/php/database/LChartDB.php** classe qui fait les requêtes sur la table des graphiques
- **./res/php/database/LDataContainerDB.php** classe qui fait les requêtes sur les data containers
- **./res/php/database/LDataDB.php** clase qui fait les requêtes sur les données
- **./res/php/database/LUserDB.php** classe qui fait les requêtes pour l'utilisateur dans la base de données

### 7.6. Pages

- **./.htaccess** fichier de configuration web qui redirige sur la page 404.php
- **./404.php** page d'erreur qui s'affiche lorsqu'on on entre une url invalide
- **./graphs.php** page qui affiche la liste des graphiques de l'utilisateur
- **./index.php** page qui permet de créer et modifier son graphique
- **./login.php** page qui permet de se connecter
- **./register.php** page qui permet de s'enregistrer dans la base de données

### 7.7. Api interne

- **./api/delete.php** supprime le graphique de l'utilisateur avec l'id du graphique passé en get
- **./api/exportCSV.php** permet d'exporter les données du graphique en sesion sous le format csv
- **./api/import.php** permet d'importer les données d'un fichier dans un graphique
- **./api/logout.php** déconnecte un utilisateur, le supprime de la session
- **./api/open.php** récupère le graphique donné en paramètre et le stocke dans la session
- **./api/save.php** sauvegarde un graphique dans la base de données

### 7.8. Librairies et outils externes

#### 7.8.1. Google Charts

[Google Charts](https://developers.google.com/chart) est un api de google qui permet de créer des graphiques. On l'utilise en Javascript.

Voici un exemple tiré de [google charts](https://developers.google.com/chart/interactive/docs/gallery/linechart).

```HTML
<!-- Cette ligne charge l'api de Google -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  // Charge le package pour les graphiques de base, en spécifiant également la version
  google.charts.load('current', {'packages':['corechart']});
  // On écrit cette ligne si on veut que le graphique se dessine au lancement
  google.charts.setOnLoadCallback(drawChart);

  // Fonction pour dessiner le graphique
  function drawChart() {

    // Données du graphique sous forme de tableau, la première ligne est un en-tête
    var data = google.visualization.arrayToDataTable([
      ['Year', 'Sales', 'Expenses'],
      ['2004',  1000,      400],
      ['2005',  1170,      460],
      ['2006',  660,       1120],
      ['2007',  1030,      540]
    ]);

    //  Options du graphique, elles sont définies dans la documentation de Google Chart
    var options = {
      title: 'Company Performance',
      curveType: 'function',
      legend: { position: 'bottom' }
    };

    // Crée l'objet chart en spécifiant son type et l'endroit où il va se dessiner
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

    // Dessine le graphique
    chart.draw(data, options);
  }
</script>
```

Par exemple, si on veut un graphique en aires au lieu d'un graphique linéaire, il suffit de changer *LineChart* dans l'exemple ci-dessus par *AreaChart*.

#### 7.8.2. GitLab

GitaLab est un outil similaire à github. Il me permet de gérer mes sources et de les versionner. De plus j'ai utilisé l'outil des issues pour m'organiser dans mon projet.

#### 7.8.3. DBeaver

DBeaver est un logiciel qui permet de gérer les bases de données. Je l'utilise car il prend en charge MariaDB.
[Download](https://dbeaver.io/)

#### 7.8.4. Bootstrap

Bootstrap est une collection d'outils permettant de créer des sites web.

Ce framework met en place de nombreuses classes que l'on peut utiliser pour designer son site web.

([Wikipedia](https://fr.wikipedia.org/wiki/Bootstrap_(framework)))

#### 7.8.5. StartBootstrap

Start Bootstrap est un site qui propose des templates Bootstrap. On peut simplement télécharger un template et il fonctionne directement. On peut supprimer certains fichier et dossiers car ils ne sont là que pour l'installation :

- le dossier scss
- les fichiers package.json
- gulpfile.js
- .travis.yml
- LICENSE

Il y a beaucoup de pages HTML qui nous montrent des exemples de composants que l'on peut utiliser.

Dans le dossier vendor j'ai supprimé deux dossiers que je n'utilise pas :

- chart.js
- datables

#### 7.8.6. Font Awesome

Font Awesome propose énormément d'icones. Le template de StartBootstrap implémente cette police d'écriture. Pour utiliser un icone, il faut écrire ce code :

```HTML
<i class="fas fa-camera"></i>
```

Le nom après *fa-* est le nom de l'icone. Pour retrouver le nom de toutes les classes on peut se référer au [site officiel](https://fontawesome.com/icons).

#### 7.8.7. DB Diagram

Db Diagram est un site web qui permet de faire des diagrammes de base de données. Il suffit d'exporter la structure de sa base de données et de l'importer dans le site pour avoir un diagramme attrayant.

#### 7.8.8. Doxygen

Doxygen est un outil de documentation. Il suffit de rajouter un en-tête dans chaque fichier qu'on veut documenter puis d'utiliser la java-doc standard pour que Doxygen crée la documentation. Je l'utilise pour exporter en HTML et en LaTeX.

```Php
/** LToolsHTMLForm
 *  -------
 *  @file
 *  @copyright Copyright (c) 2021 YourGraph, MIT License, See the LICENSE file for copying permissions.
 *  @brief Class that makes query for html form
 *  @author ludovic.rx@eduge.ch
 */
```

#### 7.8.9. MikTeX

J'utilise MikTeX pour transformer le résultat LaTeX de doxygen en pdf.
[Download](https://miktex.org/download)

#### 7.8.10. Minifier

Minifier est un outil qui minifie du code js ou css. C'est-à-dire qu'il rend le code plus compacte ce qui permet au site d'aller plus vite. Généralement un site a deux fichiers. Le fichier normal par exemple script.js et le fichier min script.min.js. Et c'est ce script min qui est inclus dans le site.
[https://www.minifier.org/](https://www.minifier.org/)

## 8. Tests

### 8.1. Environnement des tests

- Debian : Version 10
- Apache : Version 2.4
- PHP : Version 7.3.27-1~deb10u1
- MariaDB : Version 10.3.27-MariaDB-0+deb10u1
- Google Chrome : Version 90.0.4430.212 (Build officiel) (64 bits)

### 8.2. Plan de test

| N°   | Description du test                                                                                                                                             | Résultat attendu                                                                                                                 |
| ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| 1    | On se connecte à la base de données                                                                                                                             | On est bien connecté et il n'y a pas d'erreur qui soit levée                                                                     |
| 2.1  | On appuie sur le lien pour créer un compte                                                                                                                      | L'utilisateur est redirigé sur la page de création de compte                                                                     |
| 2.2  | On appuie sur le bouton créer un compte avec des données correctes                                                                                              | Le compte est créé et on est redirigé sur la page pour se connecter                                                             |
| 2.3  | On appuie sur le bouton pour créer un compte avec des données incorrectes                                                                                       | Le compte n'est pas créé, les champs incorrects sont indiqués à l'utilisateur                                                    |
| 3.1  | On appuie sur le bouton sign-in depuis la page de création de graphique lorsque l'utilisateur n'est pas connecté                                                | L'utilisateur est redirigé sur la page pour se connecter                                                                         |
| 3.2  | On appuie sur le bouton pour se connecter avec les bonnes informations                                                                                          | On est connecté et redirigé sur la page de consultations de graphiques                                                           |
| 3.3  | On appuie sur le bouton pour se connecter avec des information incorrectes                                                                                      | On n'est pas connecté et on indique que les champs sont incorrects                                                               |
| 4    | On appuie sur le bouton de déconnexion quand un utilisateur est connecté                                                                                        | L'utilisateur est déconnecté et redirigé sur la page de connexion                                                                |
| 5.1  | On arrive sur la page de graphique et on appuie sur le bouton nouveau graphique ou on appuie sur le bouton nouveau graphique depuis la page de consultation de graphiques | Cela affiche une pop-up qui permet de choisir son type de graphique et le nom                                                     |
| 5.2  | On appuie sur valider sur la Pop-up de création de graphique                                                                                                    | Cela crée un graphique dans la session et on est redirigé sur la page de graphique                                               |
| 6.1  | On entre le nom et le type des axes et on appuie sur valider                                                                                                    | le nom des axes et leur type sont stockés dans la session                                                                        |
| 6.2  | On change le nom des axes et on appuie sur valider                                                                                                              | le nom des axes est changé dans la session                                                                                       |
| 6.3  | On entre le nom d'un data container et on appuie sur ajouter                                                                                                    | le data container est ajouté en session                                                                                          |
| 6.4  | On change le nom du data container et on appuie sur modifier                                                                                                    | le nom du data container est changé dans la session                                                                              |
| 6.5  | On appuie sur le bouton pour supprimer un data container                                                                                                        | le data container est supprimé dans la session                                                                                   |
| 6.6  | On entre toutes les données avec le type correct et on appuie sur ajouter                                                                                       | les données sont ajoutées aux data containers                                                                                    |
| 6.7  | On entre toutes les données, mais on met un string comme coordonnée y à un data container                                                                       | les données ne sont pas ajoutées                                                                                                    |
| 6.8  | On entre toutes les données, mais le type de la coordonnée x n'est pas le bon et on appuie sur ajouter ou modifier                                              | les données ne sont pas ajoutées                                                                                                 |
| 6.9  | On change les données avec le type correct et on appuie sur modifier                                                                                            | les données sont modifiées en session                                                                                            |
| 6.10 | On appuie sur le bouton pour supprimer une donnée                                                                                                               | les données sont supprimées                                                                                                      |
| 7.1  | On appuie sur le bouton pour importer des données lorsqu'on a créé un graphique                                                                                | une pop-up apparaît et nous propose d'importer un fichier au format csv. de plus la structure du csv est montrée à l'utilisateur |
| 7.2  | On appuie sur importer après avoir sélectionné un fichier                                                                                                      | les données sont bien rajoutées dans la session                                                                                  |
| 7.3  | On appuie sur importer avec un fichier qui a le mauvais séparateur                                                                                              | On indique avec une pop-up l'erreur à l'utilisateur                                                                               |
| 8.1  | On choisit d'exporter au format svg                                                                                                                             | le svg est téléchargé                                                                                                            |
| 8.2  | On choisit le format csv                                                                                                                                        | le csv est téléchargé                                                                                                            |
| 8.3  | On appuie sur exporter en csv ou en svg lorsqu'il n'y a pas de données                                                                                          | rien ne se passe, il n'y a pas erreur                                                                                            |
| 9    | On appuie sur le bouton pour supprimer un graphique                                                                                                             | le graphique de l'utilisateur est supprimé                                                                                       |
| 10.1 | On appuie sur le bouton ouvrir                                                                                                                                  | on est redirigé sur la page de consultation de graphiques                                                                        |
| 10.2 | On est connecté et sur la page de consultation de ses graphiques                                                                                                | Les différents graphiques de l'utilisateur sont affichés avec leur nom, de plus on peut voir quel est le type du graphique       |
| 10.3 | On appuie sur le graphique qu'on veut regarder                                                                                                                  | on est redirigé sur la page de graphique avec le bon graphique                                                                   |
| 11.1 | On appuie sur enregistrer lorsqu'on est connecté                                                                                                                | Cela enregistre dans la base de données le graphique qui est dans la session                                                     |
| 11.2 | On appuie sur enregistrer lorsqu'on est pas connecté                                                                                                            | Une pop-up s'affiche qui nous propose de se connecter pour accéder à cette fonctionnalité                                        |
| 12   | On crée un graphique, avec plusieurs données, puis on ferme l'onglet et on le réouvre                                                                           | Le graphique est toujours là car il est dans la session                                                                          |
| 13   | On appuie sur nouveau ou ouvrir alors que le graphique n'est pas sauvegardé                                                                                     | Une pop-up s'affiche et informe l'utilisateur qu'il perdra ses données s'il continue sans sauvegarder                            |
| 14   | On a un graphique avec des axes, et des données dans dans des data containers avec le type ColumnChart                                                           | Un graphique ColumnChart s'affiche correctement                                                                                  |

### 8.3. Rapports de test

✔ -> le test fonctionne / 🗶 -> le test ne fonctionne pas

#### 8.3.1. 18 mai 2021

| N°   | Résultat obtenu                                              | Validation | Solution                                                     |
| ---- | ------------------------------------------------------------ | ---------- | ------------------------------------------------------------ |
| 1    | La connexion s'effectue sans problème                        | ✔          |                                                              |
| 2.1  | On est redirigé sur la page register.php                     | ✔          |                                                              |
| 2.2  | On est redirigé sur la page login.php                        | ✔          |                                                              |
| 2.3  | Le compte n'est pas créé mais il n'y a pas d'informations données à l'utilisateur | 🗶          | Rajouter la classe is-invalid aux inputs incorrects, en l'occurrence les passwords et aussi à l'e-mail s'il est déjà utilisé |
| 3.1  | On est redirigé sur login.php                                | ✔          |                                                              |
| 3.2  | On est connecté et redirigé sur la page de consultation de graphiques | ✔          |                                                              |
| 3.3  | On n'est pas connecté et les input mail et password sont affichés en rouge | 🗶          | Rajouter la classe is-invalid aux inputs incorrects          |
| 4    | On est redirigé sur la page de connexion                     | ✔          |                                                              |
| 5.1  | La pop-up s'affiche avec le formulaire qui demande le nom et le type du graphique | ✔          |                                                              |
| 5.2  | On est redirigé sur la page d'index et le graphique est créé dans la session | ✔          |                                                              |
| 6.1  | On est redirigé sur la même page est les axes sont sauvegardés dans la session | ✔          |                                                              |
| 6.2  | On est redirigé sur la même page et les axes sont modifiés   | ✔          |                                                              |
| 6.3  | Un data container est ajouté                                 | ✔          |                                                              |
| 6.4  | Le nom du data container est bien modifié,  on le voit sur l'affichage du graphique | ✔          |                                                              |
| 6.5  | Le data container ainsi que ses données sont supprimées, mais il y a une erreur car les dataContainers ne sont pas réindexés | 🗶          | utiliser `array_values()` permettrait de réindexer le tableau |
| 6.6  | Les données sont bien rajoutées aux data container           | ✔          |                                                              |
| 6.7  | Il y a une erreur, avec une page noire                       | 🗶          | Il faudrait vérifier si la coordonnée y est un nombre avec `is_numeric()` |
| 6.8  | La donnée est ajoutée                                        | ✔          | il faudrait changer le type de l'input en fonction du type de l'axe, et faire un filter_input différent en fonction du type. il |
| 6.9  | Les données sont modifiées                                   | ✔          |                                                              |
| 6.10 | Les données sont supprimées                                  | ✔          |                                                              |
| 7.1  | La pop-up est affichée avec les informations                  | ✔          |                                                              |
| 7.2  | L'importation a une erreur                                   | 🗶          | Il faut créer le dossier import dans le dossier res et rajouter les droits à www-data |
| 7.3  | Une pop-up indique à l'utilisateur a eu un erreur            | ✔          |                                                              |
| 8.1  | Le svg est téléchargé                                        | ✔          |                                                              |
| 8.2  | Pas implémenté                                               | 🗶          | Je pourrais parcourir la liste des data containers et écrire cela dans un fichier |
| 8.3  | Rien ne se passe                                             | ✔          |                                                              |
| 9    | Le graphique est supprimé                                    | ✔          |                                                              |
| 10.1 | On est redirigé sur la page de consultation des graphiques   | ✔          |                                                              |
| 10.2 | Les graphiques sont affichés avec leur nom et leur type      | ✔          |                                                              |
| 10.3 | On est redirigé sur la page de graphique                     | ✔          |                                                              |
| 11.1 | Le graphique est enregistré dans la base de données         | ✔          |                                                              |
| 11.2 | Pas implémenté                                               | 🗶          |                                                              |
| 12   | Le graphique est toujours là                                 | ✔          |                                                              |
| 13   | Pas implémenté                                               | 🗶          | il faudrait changer le data-target du bouton si le graphique n'est pas sauvé |
| 14   | Le graphique en colonnes s'affiche correctement              | ✔          |                                                              |

#### 8.3.2. 20 mai 2021

| N°   | Résultat obtenu                                              | Validation | Solution                                                     |
| ---- | ------------------------------------------------------------ | ---------- | ------------------------------------------------------------ |
| 1    | La connexion s'effectue sans problème                        | ✔          |                                                              |
| 2.1  | On est redirigé sur la page register.php                     | ✔          |                                                              |
| 2.2  | On est redirigé sur la page login.php                        | ✔          |                                                              |
| 2.3  | Le compte n'est pas créé et les champs invalides sont en rouge, les champs valides sont en vert | ✔          |                                                              |
| 3.1  | On est redirigé sur login.php                                | ✔          |                                                              |
| 3.2  | On est connecté et redirigé sur la page de consultation de graphiques | ✔          |                                                              |
| 3.3  | On n'est pas connecté et les input mail et password sont affichés en rouge | ✔          |                                                              |
| 4    | On est redirigé sur la page de connexion                     | ✔          |                                                              |
| 5.1  | La pop-up s'affiche avec le formulaire qui demande le nom et le type du graphique | ✔          |                                                              |
| 5.2  | On est redirigé sur la page d'index et le graphique est créé dans la session | ✔          |                                                              |
| 6.1  | On est redirigé sur la même page est les axes sont sauvegardés dans la session | ✔          |                                                              |
| 6.2  | On est redirigé sur la même page et les axes sont modifiés   | ✔          |                                                              |
| 6.3  | Un data container est ajouté                                 | ✔          |                                                              |
| 6.4  | Le nom du data container est bien modifié,  on le voit sur l'affichage du graphique | ✔          |                                                              |
| 6.5  | Le data container ainsi que ses données sont supprimées      | ✔          |                                                              |
| 6.6  | Les données sont bien rajoutées aux data container           | ✔          |                                                              |
| 6.7  | La valeur invalide est remise à 0                            | ✔          |                                                              |
| 6.8  | La donnée n'est pas modifiée                                 | ✔          |                                                              |
| 6.9  | Les données sont modifiées                                   | ✔          |                                                              |
| 6.10 | Les données sont supprimées                                  | ✔          |                                                              |
| 7.1  | La pop-up est affichée avec les informations                  | ✔          |                                                              |
| 7.2  | L'importation a une erreur                                   | ✔          |                                                              |
| 7.3  | Une pop-up indique à l'utilisateur a eu un erreur            | ✔          |                                                              |
| 8.1  | Le svg est téléchargé                                        | ✔          |                                                              |
| 8.2  | Le csv est téléchargé, il a le format que YourGraph utilise. Le csv contient les données des data containers. | ✔          |                                                              |
| 8.3  | Rien ne se passe                                             | ✔          |                                                              |
| 9    | Le graphique est supprimé                                    | ✔          |                                                              |
| 10.1 | On est redirigé sur la page de consultation des graphiques   | ✔          |                                                              |
| 10.2 | Les graphiques sont affichés avec leur nom et leur type      | ✔          |                                                              |
| 10.3 | On est redirigé sur la page de graphique                     | ✔          |                                                              |
| 11.1 | Le graphique est enregistré dans la base de données         | ✔          |                                                              |
| 11.2 | Pas implémenté                                               | 🗶          | il faudrait regarder si l'utilisateur est connecté dans la session, et changé le data-target du bouton s'il n'est pas connecté |
| 12   | Le graphique est toujours là                                 | ✔          |                                                              |
| 13   | Pas implémenté                                               | 🗶          | il faudrait changer le data-target du bouton si le graphique n'est pas sauvé |
| 14   | Le graphique en colonnes s'affiche correctement              | ✔          |                                                              |

## 9. Conclusion

### 9.1. Ce qui m'était demandé

Pour ce travail on m'a demandé de faire un site de gestion de graphiques. Ce site devait permettre de créer plusieurs types de graphiques. De plus on pouvait créer un compte et s'y connecter. Être connecté à son compte devait permettre de sauvegarder ses graphiques et de pouvoir les visualiser après. On devait pouvoir donner un nom à son graphique, et choisir son type parmi plus de deux types différents. Puis après on devait pouvoir donner le nom des axes. Après avoir créer les axes il fallait pouvoir entrer des données manuellement ou les importer depuis un fichier. Après avoir sauvegarder des graphiques, on devait pouvoir les voir dans une page. En ayant sauvegardé un graphique, on devait pouvoir l'ouvrir, le modifier ou le supprimer.

### 9.2. Rappel des points techniques évalués

- A14. L’application est bien réalisée avec les outils demandés et elle est fonctionnelle
- A15. On peut créer un compte et se connecter au site
- A16. On peut créer un graphique et rentrer les données manuellement
- A17. Lorsqu’on on est connecté, on peut voir des graphiques, les ouvrir et les modifier
- A18. On peut importer des données pour créer un graphique
- A19. Il y a différentes sortes de graphiques qui sont correctement enregistrés
- A20. Le programme a été testé suivant le protocole de test

### 9.3. Ce que j'ai fait

Mon site permet à un utilisateur, qu'il soit connecté ou non de créer un graphique. Il doit spécifier le nom du graphique et le type de graphique. Il peut choisir entre 9 types de graphiques.

- Graphique en aires (AreaChart)
- Graphique en aires à étages (SteppedAreaChart)
- Graphique en barres (BarChart)
- Graphique en colonnes (ColumnChart)
- Diagramme circulaire (PieChart)
- Graphique en anneau (DonutChart)
- Histogramme (Histogram)
- Nuage de points (ScatterChart)

Quand il a créé le graphique, l'utilisateur peut ajouter le nom des axes et le type de l'axe x. Lorsqu'il a créé les axes, il peut ajouter des data containers en leur donnant un nom. Puis après, il peut rajouter les données à son graphique. De plus il peut modifier le nom et le type du graphique. Il peut aussi modifier et supprimer les data containers et les données du graphique. Pour entrer des données, il peut importer un fichier csv. De plus il peut exporter son graphique sous le format svg ou csv. Un utilisateur, lorsqu'il est connecté, peut sauvegarder son graphique dans la base de données. Tant qu'il ne l'a pas sauvegardé, son graphique est sauvegardé dans la session. L'utilisateur connecté peut regarder la liste de ses graphiques dans une page dédiée. Il peut alors ouvrir un graphique pour le modifier, ou supprimer un graphique.

Pour m'assurer que mon site fonctionnait correctement, j'ai effectué les tests de mon plan de tests.

### 9.4. Problèmes rencontrés

Le plus gros problème que j'ai eu est celui de la sauvegarde des données. Je n'ai pas assez bien réfléchi à ce problème avant de me lancer dans la programmation ce qui m'a fait changé la base de données de nombreuses fois. J'ai pu trouver un modèle qui correspondait avec plusieurs types de graphiques ce qui m'a permis d'implémenter plus de types de graphiques que je ne le pensais. J'aurais pu transformer mon graphique au format JSON et le stocker comme cela dans la base de données. Cette solution est aussi intéressante, car elle est plus extensible qu'une base de données, cependant, elle est aussi moins sécurisée, car on pourrait plus facilement rentrer des données incorrects. De plus, bien que plus facile, j'aurais dû changer toute ma base de données ce qui m'aurait pris en tout cas 1 jour et demi, et je n'aurais alors pas eu assez de temps.

J'ai aussi eu du mal avec ma planification. Je n'ai pas  pas assez bien pensé au site avant de faire ma planification, c'est pourquoi mes tâches n'étaient pas toujours assez précises. Par exemple, j'avais prévu de d'abord enregistrer le graphique dans la base de données, alors que j'ai finalement décidé de l'enregistrer d'abord dans la session, puis si l'utilisateur le veut il peut enregistrer dans la base de données.

J'ai eu aussi un problème d'organisation durant les journées de travail. Parfois je n'arrivais pas à finir une tâche jusqu'au bout et je commencais à m'éparpiller ce qui a parfois ralenti le développement.

### 9.5. Améliorations possibles

- Il serait intéressant de rendre l'importation de données plus robustes. De plus, pour l'instant, le format des données est très strict.
- Je pourrais rajouter plus d'options visuelles, comme changer la couleur d'une ligne, ou d'un barre, et d'autres options, comme l'échelle d'un graphique.
- Je pourrais informer l'utilisateur qu'il faut qu'il sauvegarde son graphique avant de créer ou d'ouvrir un autre graphique.
- L'interface pourrait être simplifié et légèrement pour certains types de graphiques.

### 9.6. Bilan personnel

J'ai beaucoup aimé faire ce projet. Même si j'ai eu des problèmes, j'était content d'avoir trouvé une manière d'avoir beaucoup de types de graphiques. J'ai pu augmenter mes connaissances en PHP objet, ce que j'ai beaucoup aimé. J'ai aussi vu l'importance de bien se préparer avant de se lancer dans le développement, car au final on gagne du temps.

## 10. Bibliographie

- W3School : [https://www.w3schools.com/](https://www.w3schools.com/)
- PHP.Net : [https://www.php.net/](https://www.php.net/)
- Bootstrap : [https://getbootstrap.com/docs/4.6](https://getbootstrap.com/docs/4.6)
- Template Bootstrap : [https://startbootstrap.com/](https://startbootstrap.com/)
- Font Awesome : [https://fontawesome.com/](https://fontawesome.com/icons/plus?style=solid)
- API Google Chart : [https://developers.google.com/chart](https://developers.google.com/chart)
- Minifier JS : [https://www.minifier.org/](https://www.minifier.org/)
- Encoder les membre privés d'une classe : [https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members](https://stackoverflow.com/questions/7005860/php-json-encode-class-private-members)
- Comment récupérer le dernier id inséré : [https://stackoverflow.com/questions/49786047/do-i-need-transactions-to-get-the-correct-last-inserted-id-in-a-sequence](https://stackoverflow.com/questions/49786047/do-i-need-transactions-to-get-the-correct-last-inserted-id-in-a-sequence)
- Wikipedia [https://fr.wikipedia.org/](https://fr.wikipedia.org/)
- Exporter en svg :
  - [https://stackoverflow.com/questions/12628968/how-can-i-save-svg-code-as-a-svg-image](https://stackoverflow.com/questions/12628968/how-can-i-save-svg-code-as-a-svg-image)
  - [https://code-boxx.com/create-save-files-javascript/](https://code-boxx.com/create-save-files-javascript/)
  - [https://stackoverflow.com/questions/23218174/](https://stackoverflow.com/questions/23218174/)
  - [https://stackoverflow.com/questions/12628968/how-can-i-save-svg-code-as-a-svg-image](https://stackoverflow.com/questions/12628968/how-can-i-save-svg-code-as-a-svg-image)

## 11. Table des illustrations

- Figure n°1 : Planning prévisionnel
- Figure n°2 : Planning effectif
- Figure n°3 : Maquette de la page d'index
- Figure n°4 : Maquette de la page de visualisation des graphiques
- Figure n°5 : Maquette de la page de connexion
- Figure n°6 : Maquette de la page de création de compte
- Figure n°7 : Inscription
- Figure n°8 : Connexion
- Figure n°9 : Bouton pour se connecter
- Figure n°10 : Bouton pour se déconnecter
- Figure n°11 : Pop-up pour se déconnecter
- Figure n°12 : SideBar graphs.php
- Figure n°13 : SideBar index.php
- Figure n°14 : SideBar fermée
- Figure n°15 : Pop-up pour créer un graphique
- Figure n°16 : Pop-up de création de graphique réussie
- Figure n°17 : Formulaire options du graphique
- Figure n°18 : Formulaire axes du graphique
- Figure n°19 : Formulaire axes lorsqu'ils ont été ajoutés
- Figure n°20 : Bouton ajouter une donnée
- Figure n°21 : Bouton modifier une donnée
- Figure n°22 : Bouton supprimer une donnée
- Figure n°23 : Formulaire data containers du graphique
- Figure n°24 : Formulaire des données des data containers
- Figure n°25 : Affichage du graphique
- Figure n°26 : Affichage des graphiques de l'utilisateur
- Figure n°27 : Pop-up graphique sauvegarde réussie
- Figure n°28 : Pop-up graphique sauvegarde ratée
- Figure n°29 : Pop-up graphique suppression réussie
- Figure n°30 : Pop-up graphique suppression ratée
- Figure n°31 : Pop-up importation de données
- Figure n°32 : Pop-up d'importation réussie
- Figure n°33 : Pop-up d'importation échouée
- Figure n°34 : Pop-up exportation de données
- Figure n°35 : Fenêtre de téléchargement des données du graphique au format CSV
- Figure n°36 : Fenêtre de téléchargement du graphique SVG
- Figure n°37 : Page d'erreur
- Figure n°38 : Graphique en aires
- Figure n°39 : Graphique en aires à étages
- Figure n°40 : Graphique en barres
- Figure n°41 : Graphique en colonnes
- Figure n°42 : Diagramme circulaire
- Figure n°43 : Diagramm en anneau
- Figure n°44 : Histogramme
- Figure n°45 : Nuage de points
- Figure n°46 : Modèle conceptuel de données

## 12. Glossaire

| Mot               | Signification                                                |
| ----------------- | ------------------------------------------------------------ |
| Data container    | Un objet contenant une liste de points ayant un coordonnée x et y. Une ligne dans un graphique est un data container puisqu'elle contient une liste de points. |
| Index             | En informatique, dans les bases de données, un index est une structure de données utilisée et entretenue par le système de gestion de base de données (SGBD) pour lui permettre de retrouver rapidement les données. ([Wikipedia](https://fr.wikipedia.org/wiki/Index_(base_de_donn%C3%A9es))) |
| Pattern Singleton | Le pattern singleton et un patron de conception qui a pour but de n'instancier qu'une seule  fois un objet. On crée un méthode statique qui retourne l'instance statique contenue dans la classe. |
| API               | **A**pplication **P**rogramming **I**nterface est une interface de programmation d'application ou interface de programmation applicative. C'est un ensemble normalisé de classes, de méthodes, de fonctions et de constantes qui sert de façade par laquelle un logiciel offre des services à d'autres logiciels. ([Wikipedia](https://fr.wikipedia.org/wiki/Interface_de_programmation#:~:text=En%20informatique%2C%20une%20interface%20de,laquelle%20un%20logiciel%20offre%20des)) |
| SVG               | *S*calable *V*ector *G*raphics est un format de dessin vectoriel. |
| Front End         | Partie du développement qui concerne la partie de l'affichage, c'est la partie visible. [(Wikipedia)](https://fr.wikipedia.org/wiki/Backend) |
| Back end          | Partie du développement qui concerne le fonctionnement du site, elle doit produire un résultat.  [(Wikipedia)](https://fr.wikipedia.org/wiki/Backend) |
