# YourGraph-private

Site de gestion de graphique
TPI - Mai 2021

## Documentation

[Documentation technique](doc/DocumentTechnique.md)

[Documentation utilisateur](doc/ManuelUtilisateur.md.md)

## Page d'erreur

Il faut changer la configuration du serveur apache. Il faut mettre l'instruction `AllowOverride All`, de base il est à none. Sur Debian, le fichier de conf s'appelle `apache2.conf`.

Il faut changer le .htaccess avec le lien correspondant à votre site pour la page d'erreur

## Création de la base de données

Pour créer la base de données, il faut exécuter le fichier `sql/db.sql` et le fichier `sql/types.db`.

- `sql/db.sql` crée la base de données et les tables
- `sql/types.db` insert les types de graphiques et le type d'axes

## Connexion database

Il faut créer le fichier ``` public/res/php/database/const.inc.php ``` avec le nom de l'utilisateur et le mot de passe que vous avez choisi.

```php
// Contenu du fichier
define("DB_TYPE", "mysql");
define("DB_HOST", "127.0.0.1");
define("DB_PORT", 3306);
define("DB_NAME", "YourGraph");
define("DB_USER", "[NameOfUser]");
define("DB_PASS", "[PasswordUser]");
define("DB_CHARSET", "utf8");
```

Il faut faire la même chose dans le fichier user.sql et l'exécuter dans la base de données.

## Importation et exportation des données

```bash
# A effectuer dans le dossier res
sudo chown www-data:www-data import
sudo chown www-data:www-data export
```

Il ne faut pas oublier de créer les dossiers d'import et d'export dans le dossier public/res/
