-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: YourGraph
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.27-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `axes`
--

DROP TABLE IF EXISTS `axes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `axes` (
  `idAxis` int(11) NOT NULL AUTO_INCREMENT,
  `axisName` varchar(150) NOT NULL DEFAULT 'Empty Axis',
  `axisLetter` varchar(1) NOT NULL DEFAULT 'X',
  `idAxisType` int(11) DEFAULT NULL,
  `idChart` int(11) NOT NULL,
  PRIMARY KEY (`idAxis`),
  KEY `axes_FK_idAxisType` (`idAxisType`),
  KEY `axes_FK_idChart` (`idChart`),
  CONSTRAINT `axes_FK_idAxisType` FOREIGN KEY (`idAxisType`) REFERENCES `axesTypes` (`idAxisType`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `axes_FK_idChart` FOREIGN KEY (`idChart`) REFERENCES `charts` (`idChart`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `axes`
--

LOCK TABLES `axes` WRITE;
/*!40000 ALTER TABLE `axes` DISABLE KEYS */;
INSERT INTO `axes` VALUES (135,'Années','X',6,78),(136,'Recettes','Y',5,78),(137,'adw','X',5,79),(138,'wd','Y',5,79),(139,'Années','X',6,80),(140,'Quantité','Y',5,80),(151,'Mois','X',6,86),(152,'Vues','Y',5,86),(153,'Pays','X',4,87),(154,'Population','Y',5,87),(155,'Tranche d&#39;age','X',4,88),(156,'Nombre de clients','Y',5,88),(157,'Années','X',6,89),(158,'Nombre d habitants','Y',5,89),(161,'x','X',5,91),(162,'y','Y',5,91),(163,'Années','X',6,92),(164,'Densité','Y',5,92),(177,'Année','X',6,99),(178,'Taux en pourcent','Y',5,99),(179,'Dinosaure','X',4,100),(180,'Quantité','Y',5,100),(181,'Années','X',6,101),(182,'Argent','Y',5,101);
/*!40000 ALTER TABLE `axes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `axesTypes`
--

DROP TABLE IF EXISTS `axesTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `axesTypes` (
  `idAxisType` int(11) NOT NULL AUTO_INCREMENT,
  `axisTypeName` varchar(100) NOT NULL,
  PRIMARY KEY (`idAxisType`),
  UNIQUE KEY `axesTypes_UN_axisTypeName` (`axisTypeName`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `axesTypes`
--

LOCK TABLES `axesTypes` WRITE;
/*!40000 ALTER TABLE `axesTypes` DISABLE KEYS */;
INSERT INTO `axesTypes` VALUES (6,'date'),(5,'number'),(4,'string');
/*!40000 ALTER TABLE `axesTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charts`
--

DROP TABLE IF EXISTS `charts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charts` (
  `idChart` int(11) NOT NULL AUTO_INCREMENT,
  `chartName` varchar(255) NOT NULL,
  `idChartType` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idChart`),
  KEY `charts_FK_idChartType` (`idChartType`),
  KEY `charts_FK_idUser` (`idUser`),
  CONSTRAINT `charts_FK_idChartType` FOREIGN KEY (`idChartType`) REFERENCES `chartsTypes` (`idChartType`),
  CONSTRAINT `charts_FK_idUser` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charts`
--

LOCK TABLES `charts` WRITE;
/*!40000 ALTER TABLE `charts` DISABLE KEYS */;
INSERT INTO `charts` VALUES (78,'Graph Ventes',9,13),(79,'f',5,13),(80,'Graphique de ventes de livres',6,13),(86,'Nombre de vue par streamer',9,14),(87,'Population mondiale',11,14),(88,'Age des clients',12,14),(89,'Evolution de la population mondiale',6,14),(91,'Coordonnées des attaques',7,14),(92,'Densité de la population dans les villes',13,14),(99,'Taux de criminalité',5,14),(100,'Tailles des dinosaurs',10,14),(101,'Ventes annuelles',4,14);
/*!40000 ALTER TABLE `charts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chartsTypes`
--

DROP TABLE IF EXISTS `chartsTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chartsTypes` (
  `idChartType` int(11) NOT NULL AUTO_INCREMENT,
  `chartTypeName` varchar(100) NOT NULL,
  PRIMARY KEY (`idChartType`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chartsTypes`
--

LOCK TABLES `chartsTypes` WRITE;
/*!40000 ALTER TABLE `chartsTypes` DISABLE KEYS */;
INSERT INTO `chartsTypes` VALUES (4,'AreaChart'),(5,'BarChart'),(6,'LineChart'),(7,'ScatterChart'),(9,'ColumnChart'),(10,'Histogram'),(11,'PieChart'),(12,'DonutChart'),(13,'SteppedAreaChart');
/*!40000 ALTER TABLE `chartsTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dataContainers`
--

DROP TABLE IF EXISTS `dataContainers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataContainers` (
  `idDataContainer` int(11) NOT NULL AUTO_INCREMENT,
  `dataContainerName` varchar(100) NOT NULL,
  `idChart` int(11) NOT NULL,
  PRIMARY KEY (`idDataContainer`),
  KEY `lines_FK_idChart` (`idChart`),
  CONSTRAINT `lines_FK_idChart` FOREIGN KEY (`idChart`) REFERENCES `charts` (`idChart`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dataContainers`
--

LOCK TABLES `dataContainers` WRITE;
/*!40000 ALTER TABLE `dataContainers` DISABLE KEYS */;
INSERT INTO `dataContainers` VALUES (40,'Jouets',78),(41,'Livres',78),(42,'Nourriture',78),(43,'wda',79),(44,'wda',79),(45,'wda',79),(46,'wda',79),(47,'wda',79),(48,'Suisse',80),(49,'France',80),(50,'Allemagne',80),(57,'ZeratoR',86),(58,'Etoiles',86),(59,'Kenny',86),(60,'Domingo',86),(61,'Sardoche',86),(62,'Gotaga',86),(63,'Skyart',86),(64,'Mister MV',86),(65,'Alderiate',86),(66,'Chap',86),(67,'Rhobalas',86),(68,'Population',87),(69,'Tranche d&#39;âge',88),(70,'Monde',89),(72,'Position',91),(73,'Genève',92),(74,'Carouge',92),(75,'Lausanne',92),(76,'Fribourg',92),(77,'Lyon',92),(94,'Criminalité',99),(95,'Taille',100),(96,'Livres',101),(97,'Jouets',101),(98,'Habits',101);
/*!40000 ALTER TABLE `dataContainers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(150) NOT NULL,
  `lastName` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `users_UN_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (13,'Ludovic','Roux','lroux@live.fr','$2y$10$eq7ZTlpGg8ps/o0LL/wtY.WGrGC78STV7TQGZ2uqeI6kE6Ya/djZ2'),(14,'Ludovic','Roux','r.ludovic.x@gmail.com','$2y$10$18DBY63u3/LmIGLseD7aEOpRiCDbXmn1mvMBZM8HPJSpaoY.iz8LW');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xData`
--

DROP TABLE IF EXISTS `xData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xData` (
  `idXData` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(150) NOT NULL,
  PRIMARY KEY (`idXData`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xData`
--

LOCK TABLES `xData` WRITE;
/*!40000 ALTER TABLE `xData` DISABLE KEYS */;
INSERT INTO `xData` VALUES (33,'2021-05-19'),(34,'2021-01-12'),(35,'2021-05-27'),(36,'2021-09-30'),(37,'2021-05-19'),(38,'2021-01-12'),(39,'2021-05-27'),(40,'2021-09-30'),(41,'2021-05-19'),(42,'2021-01-12'),(43,'2021-05-27'),(44,'2021-09-30'),(45,'2021-05-19'),(46,'2021-01-12'),(47,'2021-05-27'),(48,'2021-09-30'),(49,'2021-05-19'),(50,'2021-01-12'),(51,'2021-05-27'),(52,'2021-09-30'),(53,'2002'),(54,'2003'),(55,'2004'),(56,'2005'),(57,'2021-07-01'),(58,'2021-08-01'),(59,'2021-05-19'),(60,'2021-01-12'),(61,'2021-05-27'),(62,'2021-09-30'),(63,'2021-05-19'),(64,'2021-01-12'),(65,'2021-05-27'),(66,'2021-09-30'),(67,'2021-06-01'),(68,'2021-07-01'),(69,'2021-08-01'),(70,'2021-09-01'),(71,'2021-10-01'),(72,'2021-06-01'),(73,'2021-07-01'),(74,'2021-08-01'),(75,'Chine'),(76,'Inde'),(77,'Etats-Unis'),(78,'Indonésie'),(79,'Pakistan'),(80,'Brésil'),(81,'Nigeria'),(82,'Bangladesh'),(83,'Russie'),(84,'Mexique'),(85,'0-10 ans'),(86,'11-18 ans'),(87,'19-25 ans'),(88,'26-40 ans'),(89,'41-55 ans'),(90,'56+'),(91,'1950-01-01'),(92,'1955-01-01'),(93,'1960-01-01'),(94,'1965-01-01'),(95,'1970-01-01'),(96,'1975-01-01'),(97,'1980-01-01'),(98,'1985-01-01'),(99,'1990-01-01'),(100,'1995-01-01'),(101,'2000-01-01'),(102,'2005-01-01'),(103,'2010-01-01'),(104,'2015-01-01'),(105,'2020-01-01'),(106,'0'),(107,'5'),(108,'-8'),(109,'-8'),(110,'4'),(111,'7'),(112,'-5'),(113,'1'),(114,'-48'),(115,'458'),(116,'4'),(117,'-451'),(118,'1541'),(119,'-644'),(120,'-1512'),(121,'5'),(122,'-8'),(123,'-8'),(124,'4'),(125,'7'),(126,'-5'),(127,'1'),(128,'-48'),(129,'458'),(130,'4'),(131,'-451'),(132,'1541'),(133,'-644'),(134,'-1512'),(135,'0'),(136,'5'),(137,'0'),(138,'2000-01-01'),(139,'2005-01-01'),(140,'2010-01-01'),(141,'Acrocanthosaurus (top-spined lizard)'),(142,'Albertosaurus (Alberta lizard)'),(143,'Allosaurus (other lizard)'),(144,'Apatosaurus (deceptive lizard)'),(145,'Archaeopteryx (ancient wing)'),(146,'Argentinosaurus (Argentina lizard)'),(147,'Baryonyx (heavy claws)'),(148,'Brachiosaurus (arm lizard)'),(149,'Ceratosaurus (horned lizard)'),(150,'Coelophysis (hollow form)'),(151,'Compsognathus (elegant jaw)'),(152,'Deinonychus (terrible claw)'),(153,'Diplodocus (double beam)'),(154,'Dromicelomimus (emu mimic)'),(155,'Gallimimus (fowl mimic)'),(156,'Mamenchisaurus (Mamenchi lizard)'),(157,'Megalosaurus (big lizard)'),(158,'Microvenator (small hunter)'),(159,'Ornithomimus (bird mimic)'),(160,'Oviraptor (egg robber)'),(161,'Plateosaurus (flat lizard)'),(162,'Sauronithoides (narrow-clawed lizard)'),(163,'Seismosaurus (tremor lizard)'),(164,'Spinosaurus (spiny lizard)'),(165,'Supersaurus (super lizard)'),(166,'Tyrannosaurus (tyrant lizard)'),(167,'Ultrasaurus (ultra lizard)'),(168,'Velociraptor (swift robber)'),(169,'2010-01-01'),(170,'2011-01-01'),(171,'2012-01-01'),(172,'2013-01-01'),(173,'2010-01-01'),(174,'2011-01-01'),(175,'2010-01-01'),(176,'2011-01-01'),(177,'2012-01-01'),(178,'2010-01-01'),(179,'2011-01-01'),(180,'2012-01-01'),(181,'2013-01-01'),(182,'2010-01-01'),(183,'2011-01-01'),(184,'2012-01-01'),(185,'2013-01-01'),(186,'2005-01-01'),(187,'2006-01-01'),(188,'2007-01-01'),(189,'2008-01-01'),(190,'2009-01-01'),(191,'Acrocanthosaurus (top-spined lizard)'),(192,'Albertosaurus (Alberta lizard)'),(193,'Allosaurus (other lizard)'),(194,'Apatosaurus (deceptive lizard)'),(195,'Archaeopteryx (ancient wing)'),(196,'Argentinosaurus (Argentina lizard)'),(197,'Baryonyx (heavy claws)'),(198,'Brachiosaurus (arm lizard)'),(199,'Ceratosaurus (horned lizard)'),(200,'Coelophysis (hollow form)'),(201,'Compsognathus (elegant jaw)'),(202,'Deinonychus (terrible claw)'),(203,'Diplodocus (double beam)'),(204,'Dromicelomimus (emu mimic)'),(205,'Gallimimus (fowl mimic)'),(206,'Mamenchisaurus (Mamenchi lizard)'),(207,'Megalosaurus (big lizard)'),(208,'Microvenator (small hunter)'),(209,'Ornithomimus (bird mimic)'),(210,'Oviraptor (egg robber)'),(211,'Plateosaurus (flat lizard)'),(212,'Sauronithoides (narrow-clawed lizard)'),(213,'Seismosaurus (tremor lizard)'),(214,'Spinosaurus (spiny lizard)'),(215,'Supersaurus (super lizard)'),(216,'Tyrannosaurus (tyrant lizard)'),(217,'Ultrasaurus (ultra lizard)'),(218,'Velociraptor (swift robber)'),(219,'2010-01-01'),(220,'2011-01-01'),(221,'2012-01-01'),(222,'2013-01-01');
/*!40000 ALTER TABLE `xData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yData`
--

DROP TABLE IF EXISTS `yData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yData` (
  `idYData` int(11) NOT NULL AUTO_INCREMENT,
  `value` float NOT NULL,
  `idDataContainer` int(11) NOT NULL,
  `idXData` int(11) NOT NULL,
  PRIMARY KEY (`idYData`),
  KEY `yData_FK_idDataContainer` (`idDataContainer`),
  KEY `yData_FK_idXData` (`idXData`),
  CONSTRAINT `yData_FK_idDataContainer` FOREIGN KEY (`idDataContainer`) REFERENCES `dataContainers` (`idDataContainer`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `yData_FK_idXData` FOREIGN KEY (`idXData`) REFERENCES `xData` (`idXData`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=393 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yData`
--

LOCK TABLES `yData` WRITE;
/*!40000 ALTER TABLE `yData` DISABLE KEYS */;
INSERT INTO `yData` VALUES (87,745,40,45),(88,752,41,45),(89,7247,42,45),(90,75,40,46),(91,72,41,46),(92,3387,42,46),(93,7522,40,47),(94,4834,41,47),(95,4834,42,47),(96,7576,40,48),(97,7486,41,48),(98,4838,42,48),(99,745,40,49),(100,752,41,49),(101,7247,42,49),(102,75,40,50),(103,72,41,50),(104,3387,42,50),(105,7522,40,51),(106,4834,41,51),(107,4834,42,51),(108,7576,40,52),(109,7486,41,52),(110,4838,42,52),(111,54,43,53),(112,616,44,53),(113,541,45,53),(114,56,46,53),(115,3,47,53),(116,15,43,54),(117,615,44,54),(118,351,45,54),(119,156,46,54),(120,0,47,54),(121,54,43,55),(122,616,44,55),(123,541,45,55),(124,56,46,55),(125,0,47,55),(126,15,43,56),(127,615,44,56),(128,351,45,56),(129,156,46,56),(130,0,47,56),(131,450,48,57),(132,800,49,57),(133,540,50,57),(134,450,48,58),(135,147,49,58),(136,530,50,58),(158,45345,57,72),(159,34534,58,72),(160,45345,59,72),(161,45340,60,72),(162,48634,61,72),(163,45345,62,72),(164,58453,63,72),(165,42342,64,72),(166,27434,65,72),(167,17152,66,72),(168,72272,67,72),(169,76842,57,73),(170,12764,58,73),(171,28764,59,73),(172,38431,60,73),(173,38738,61,73),(174,53423,62,73),(175,48312,63,73),(176,45304,64,73),(177,38438,65,73),(178,38431,66,73),(179,45831,67,73),(180,54344,57,74),(181,45388,58,74),(182,48318,59,74),(183,38434,60,74),(184,48348,61,74),(185,83434,62,74),(186,48344,63,74),(187,48313,64,74),(188,32483,65,74),(189,48343,66,74),(190,74863,67,74),(191,1444220000,68,75),(192,1393410000,68,76),(193,332915000,68,77),(194,225362000,68,78),(195,225200000,68,79),(196,213993000,68,80),(197,211401000,68,81),(198,166303000,68,82),(199,145912000,68,83),(200,130262000,68,84),(201,200,69,85),(202,400,69,86),(203,780,69,87),(204,250,69,88),(205,120,69,89),(206,12,69,90),(207,2536430,70,91),(208,2773020,70,92),(209,3034950,70,93),(210,3339580,70,94),(211,3700440,70,95),(212,4079480,70,96),(213,4458000,70,97),(214,4870920,70,98),(215,5327230,70,99),(216,5744210,70,100),(217,6143490,70,101),(218,6541910,70,102),(219,6956820,70,103),(220,7379800,70,104),(221,7794800,70,105),(237,98,72,121),(238,165,72,122),(239,-145,72,123),(240,87,72,124),(241,84,72,125),(242,-9,72,126),(243,546,72,127),(244,61,72,128),(245,615,72,129),(246,515,72,130),(247,12,72,131),(248,-481,72,132),(249,654,72,133),(250,61,72,134),(251,4,72,135),(252,0,72,136),(253,0,72,137),(254,6541,73,138),(255,1514,74,138),(256,641,75,138),(257,621,76,138),(258,651,77,138),(259,643,73,139),(260,464,74,139),(261,8479,75,139),(262,1264,76,139),(263,8619,77,139),(264,421,73,140),(265,651,74,140),(266,1789,75,140),(267,6134,76,140),(268,5190.99,77,140),(348,0.01,94,186),(349,0.2,94,187),(350,0.5,94,188),(351,0.09,94,189),(352,0.15,94,190),(353,12.2,95,191),(354,9.1,95,192),(355,12.2,95,193),(356,22.9,95,194),(357,0.9,95,195),(358,36.6,95,196),(359,9.1,95,197),(360,30.5,95,198),(361,6.1,95,199),(362,2.7,95,200),(363,0.9,95,201),(364,2.7,95,202),(365,27.1,95,203),(366,3.4,95,204),(367,5.5,95,205),(368,21,95,206),(369,7.9,95,207),(370,1.2,95,208),(371,4.6,95,209),(372,1.5,95,210),(373,7.9,95,211),(374,2,95,212),(375,45.7,95,213),(376,12.2,95,214),(377,30.5,95,215),(378,15.2,95,216),(379,30.5,95,217),(380,1.8,95,218),(381,450,96,219),(382,1450,97,219),(383,236,98,219),(384,478,96,220),(385,1500,97,220),(386,740,98,220),(387,280,96,221),(388,1600,97,221),(389,785,98,221),(390,489,96,222),(391,780,97,222),(392,1800,98,222);
/*!40000 ALTER TABLE `yData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'YourGraph'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-08 11:02:07
